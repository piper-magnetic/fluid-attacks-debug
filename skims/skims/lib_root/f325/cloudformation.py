from aws.iam.structure import (
    is_action_permissive,
    is_resource_permissive,
)
from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.f325.utils import (
    has_attribute_wildcard,
    has_write_actions,
)
from lib_root.utilities.cloudformation import (
    get_attribute,
    get_list_from_node,
    iter_iam_policies,
    iterate_iam_policy_documents,
    iterate_resource,
    yield_statements_from_policy,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
import re
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils.graph import (
    adj_ast,
)

WILDCARD_ACTION: re.Pattern = re.compile(r"^((\*)|(\w+:\*))$")


def _permissive_policy(graph: Graph, nid: NId) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    condition = get_attribute(graph, nid, "Condition")[0]
    principal = get_attribute(graph, nid, "Principal")[0]
    if (
        (effect_val == "Allow" or effect is None)
        and not condition
        and not principal
    ):
        _, _, action_id = get_attribute(graph, nid, "Action")
        _, _, resource_id = get_attribute(graph, nid, "Resource")
        action_list = get_list_from_node(graph, action_id)
        resources_list = get_list_from_node(graph, resource_id)
        if all(
            (
                any(map(is_action_permissive, action_list)),
                any(map(is_resource_permissive, resources_list)),
            )
        ):
            yield resource_id


def get_wildcard_nodes(
    act_res: list, pattern: re.Pattern, nid: NId
) -> Iterator[NId]:
    for act in act_res:
        if pattern.match(act):
            yield nid


def _iam_allow_wildcard_action_trust_policy(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    document = get_attribute(graph, val_id, "AssumeRolePolicyDocument")
    if document[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[document[2]]["value_id"]
        ):
            effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
            _, _, action_id = get_attribute(graph, stmt, "Action")
            action_list = get_list_from_node(graph, action_id)
            if effect and effect_val == "Allow":
                yield from get_wildcard_nodes(
                    action_list, WILDCARD_ACTION, action_id
                )


def _kms_key_has_master_keys_exposed_to_everyone(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    document = get_attribute(graph, val_id, "KeyPolicy")
    if document[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[document[2]]["value_id"]
        ):
            effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
            principal, _, p_id = get_attribute(graph, stmt, "Principal")
            if effect and effect_val == "Allow" and principal:
                p_attrs = graph.nodes[p_id].get("value_id")
                if (
                    p_attrs
                    and (aws := get_attribute(graph, p_attrs, "AWS"))
                    and aws[1] == "*"
                ):
                    yield aws[2]


def _aux_iam_allow_wildcard_actions_perms_policies(
    graph: Graph, stmt: NId
) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
    _, _, action_id = get_attribute(graph, stmt, "Action")
    action_list = get_list_from_node(graph, action_id)
    if effect and effect_val == "Allow":
        yield from get_wildcard_nodes(action_list, WILDCARD_ACTION, action_id)


def _iam_allow_wildcard_actions_perms_policies(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "Policies")
    if policies[0]:
        for policy in adj_ast(graph, graph.nodes[policies[2]]["value_id"]):
            _, _, pd_id = get_attribute(graph, policy, "PolicyDocument")
            for stmt in yield_statements_from_policy(
                graph, graph.nodes[pd_id]["value_id"]
            ):
                yield from _aux_iam_allow_wildcard_actions_perms_policies(
                    graph, stmt
                )


def _iam_is_policy_actions_wildcards(graph: Graph, nid: NId) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "PolicyDocument")
    if policies[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[policies[2]]["value_id"]
        ):
            effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
            _, _, action_id = get_attribute(graph, stmt, "Action")
            action_list = get_list_from_node(graph, action_id)
            if effect and effect_val == "Allow":
                yield from get_wildcard_nodes(
                    action_list, WILDCARD_ACTION, action_id
                )


def _iam_has_wildcard_resource_on_trust_policies(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    document = get_attribute(graph, val_id, "AssumeRolePolicyDocument")
    if document[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[document[2]]["value_id"]
        ):
            yield from _aux_iam_has_wildcard_resource_on_write_action(
                graph, stmt
            )


def _aux_iam_has_wildcard_resource_on_write_action(
    graph: Graph, stmt: NId
) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
    _, _, action_id = get_attribute(graph, stmt, "Action")
    _, _, res_id = get_attribute(graph, stmt, "Resource")
    res_list = get_list_from_node(graph, res_id)
    action_list = get_list_from_node(graph, action_id)
    if (
        effect
        and effect_val == "Allow"
        and has_attribute_wildcard(res_list)
        and has_write_actions(action_list)
    ):
        yield res_id


def _iam_has_wildcard_resource_on_write_action(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "Policies")
    if policies[0]:
        for policy in adj_ast(graph, graph.nodes[policies[2]]["value_id"]):
            _, _, pd_id = get_attribute(graph, policy, "PolicyDocument")
            for stmt in yield_statements_from_policy(
                graph, graph.nodes[pd_id]["value_id"]
            ):
                yield from _aux_iam_has_wildcard_resource_on_write_action(
                    graph, stmt
                )


def aux_wildcard_in_policy_document(graph: Graph, nid: NId) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "PolicyDocument")
    if policies[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[policies[2]]["value_id"]
        ):
            yield from _aux_iam_has_wildcard_resource_on_write_action(
                graph, stmt
            )


def cfn_iam_has_wildcard_resource_on_write_action(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_WILDCARD_RESOURCES

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iter_iam_policies(graph):
                for report in chain(
                    _iam_has_wildcard_resource_on_write_action(graph, nid),
                    aux_wildcard_in_policy_document(graph, nid),
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key=(
            "src.lib_path.f325.iam_has_wildcard_resource_on_write_action"
        ),
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_has_wildcard_resource_on_trust_policies(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_WILDCARD_WRITE

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iter_iam_policies(graph):
                for report in _iam_has_wildcard_resource_on_trust_policies(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key=(
            "src.lib_path.f325.iam_has_wildcard_resource_on_write_action"
        ),
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_is_policy_actions_wildcards(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_WILDCARD_ACTIONS

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iter_iam_policies(graph):
                for report in _iam_is_policy_actions_wildcards(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key=(
            "src.lib_path.f325.iam_allow_wilcard_actions_permissions_policy"
        ),
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_allow_wildcard_actions_perms_policies(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_WILDCARD_ACTIONS

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iter_iam_policies(graph):
                for report in _iam_allow_wildcard_actions_perms_policies(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key=(
            "src.lib_path.f325.iam_allow_wilcard_actions_permissions_policy"
        ),
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_kms_key_has_master_keys_exposed_to_everyone(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_KMS_MASTER_KEYS_EXPOSED

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::KMS::Key"):
                for report in _kms_key_has_master_keys_exposed_to_everyone(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key=(
            "src.lib_path.f325.kms_key_has_master_keys_exposed_to_everyone"
        ),
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_allow_wildcard_action_trust_policy(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_TRUST_POLICY_WILDCARD_ACTION

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::IAM::Role"):
                for report in _iam_allow_wildcard_action_trust_policy(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f325.iam_allow_wildcard_action_trust_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_permissive_policy(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_PERMISSIVE_POLICY

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_iam_policy_documents(graph):
                for report in _permissive_policy(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f325_aws.permissive_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
