# shellcheck shell=bash

function dynamodb_centralize {
  export AWS_DEFAULT_REGION="us-east-1"

  : \
    && aws_login "prod_observes" "3600" \
    && export_notifier_key \
    && redshift_env_vars \
    && echo '[INFO] Running centralizer' \
    && dynamo-etl centralize main \
      --schema 'dynamodb' \
      --tables 'integrates_vms' \
      --parts-schema-prefix 'dynamodb_integrates_vms_part_' \
      --parts-loading-schema 'dynamodb_integrates_vms_merged_parts_loading'
}

dynamodb_centralize
