from billing.types import (
    Customer,
)
import chargebee
from context import (
    FI_CHARGEBEE_API_KEY,
    FI_CHARGEBEE_SITE,
)
from organizations import (
    domain as orgs_domain,
)

chargebee.configure(FI_CHARGEBEE_API_KEY, FI_CHARGEBEE_SITE)


async def create_customer(
    org_id: str,
    org_name: str,
    user_email: str,
) -> Customer:
    """Create Chargebee customer"""
    result = chargebee.Customer.create(
        {
            "email": user_email,
            "first_name": org_name,
        }
    )

    chargebee_customer = result.customer
    customer: Customer = Customer(
        id=chargebee_customer.id,
        name=chargebee_customer.first_name,
        address=None,
        email=chargebee_customer.email,
        phone=None,
        default_payment_method=None,
    )

    # Assign subscription customer to org
    await orgs_domain.update_subscription_customer(
        organization_id=org_id,
        organization_name=org_name,
        subscription_customer=customer.id,
    )
    return customer
