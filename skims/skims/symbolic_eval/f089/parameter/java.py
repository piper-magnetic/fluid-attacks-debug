from symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from utils import (
    string,
)
from utils.logs import (
    log_blocking,
)


def java_trust_boundary_violation(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    log_blocking("info", "Processing Java trust boundary violations (%s)", args)
    lib = string.build_attr_paths(
        "javax", "servlet", "http", "HttpServletRequest"
    )
    log_blocking("info", "Attr paths built (%s)", lib)

    if args.graph.nodes[args.n_id]["variable_type"] in lib:
        args.triggers.add("userconnection")

    se = SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
    #log_blocking("info", "Returning symbolic eval (%s)", args)
    return se
