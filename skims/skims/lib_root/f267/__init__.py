from lib_root.f267.kubernetes import (
    k8s_allow_privilege_escalation_enabled,
    k8s_check_add_capability,
    k8s_check_drop_capability,
    k8s_check_privileged_used,
    k8s_check_run_as_user,
    k8s_check_seccomp_profile,
    k8s_container_without_securitycontext,
    k8s_root_container,
    k8s_root_filesystem_read_only,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F267
QUERIES: graph_model.Queries = (
    (FINDING, k8s_allow_privilege_escalation_enabled),
    (FINDING, k8s_check_add_capability),
    (FINDING, k8s_check_drop_capability),
    (FINDING, k8s_check_privileged_used),
    (FINDING, k8s_check_run_as_user),
    (FINDING, k8s_check_seccomp_profile),
    (FINDING, k8s_container_without_securitycontext),
    (FINDING, k8s_root_container),
    (FINDING, k8s_root_filesystem_read_only),
)
