from collections.abc import (
    Iterator,
)
from lib_root.utilities.common import (
    search_method_invocation_naive,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)


def info_leak_errors(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.CS_INFO_LEAK_ERRORS
    danger_set = {"WebHostDefaults.DetailedErrorsKey", '"true"'}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in search_method_invocation_naive(graph, {"UseSetting"}):
                if get_node_evaluation_results(
                    method, graph, n_id, danger_set
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_root.f239.csharp_info_leak_errors",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
