from lib_root.f044.cloudformation import (
    resource_has_danger_https_methods_enabled,
    resource_has_https_methods_enabled,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F044
QUERIES: graph_model.Queries = (
    (FINDING, resource_has_danger_https_methods_enabled),
    (FINDING, resource_has_https_methods_enabled),
)
