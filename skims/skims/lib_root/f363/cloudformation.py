from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.cloudformation import (
    get_attribute,
    iterate_resource,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def _aux_insecure_generate_secret_string(
    graph: Graph, s_attrs: NId
) -> Iterator[NId]:
    numerics: set = set("01234567890")
    lowercase: set = set("abcdefghijklmnopqrstuvwxyz")
    uppercase: set = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    punctuation: set = set("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~")
    min_length = 14
    exclude_chars = get_attribute(graph, s_attrs, "ExcludeCharacters")
    for charset in (
        numerics,
        lowercase,
        uppercase,
        punctuation,
    ):
        # Do not allow to entirely exclude one type of chars
        if exclude_chars and all(char in exclude_chars[1] for char in charset):
            yield exclude_chars[2]
    require_each_include_type = get_attribute(
        graph, s_attrs, "RequireEachIncludedType"
    )
    if (
        require_each_include_type[0]
        and require_each_include_type[1] == "false"
    ):
        yield require_each_include_type[2]
    password_length = get_attribute(graph, s_attrs, "PasswordLength")
    if password_length[0] and int(password_length[1]) < min_length:
        yield password_length[2]


def _insecure_generate_secret_string(graph: Graph, nid: NId) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    secret, _, s_id = get_attribute(graph, val_id, "GenerateSecretString")
    if secret:
        s_attrs = graph.nodes[s_id]["value_id"]
        exclude_lower = get_attribute(graph, s_attrs, "ExcludeLowercase")
        if exclude_lower[1] == "true":
            yield exclude_lower[2]
        exclude_upper = get_attribute(graph, s_attrs, "ExcludeUppercase")
        if exclude_upper[1] == "true":
            yield exclude_upper[2]
        exclude_numbers = get_attribute(graph, s_attrs, "ExcludeNumbers")
        if exclude_numbers[1] == "true":
            yield exclude_numbers[2]
        exclude_punctuation = get_attribute(
            graph, s_attrs, "ExcludePunctuation"
        )
        if exclude_punctuation[1] == "true":
            yield exclude_punctuation[2]
        yield from _aux_insecure_generate_secret_string(graph, s_attrs)


def cfn_insecure_generate_secret_string(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_INSEC_GEN_SECRET

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::SecretsManager::Secret"):
                for report in _insecure_generate_secret_string(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f363.insecure_generate_secret_string",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
