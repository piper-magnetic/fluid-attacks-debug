{
  fetchNixpkgs,
  projectPath,
  observesIndex,
}: let
  python_version = "python311";
  nixpkgs = fetchNixpkgs {
    rev = "421cfe4e7e1a5f91eddd1cf6a86574f8e9c8136e";
    sha256 = "MgcwKHS1DIw0zKu8Y6u2MVnkh9GpKWVY7I7ZcyXWodY=";
  };

  arch-lint = let
    src = builtins.fetchGit {
      url = "https://gitlab.com/dmurciaatfluid/arch_lint";
      rev = "258b92624e9e2de13f6cafb951f30416eb8e2441"; # post v2.3.0
      ref = "refs/heads/main";
    };
  in
    import "${src}/build" {
      inherit src nixpkgs;
    };

  fa-purity = let
    src = builtins.fetchGit {
      url = "https://gitlab.com/dmurciaatfluid/purity";
      rev = "a2fba10724f6062976d3bb3ae5a40f0addd959a5";
      ref = "refs/tags/v1.31.0";
    };
  in
    import "${src}/build" {
      inherit src nixpkgs;
    };

  fa-singer-io = let
    src = builtins.fetchGit {
      url = "https://gitlab.com/dmurciaatfluid/singer_io";
      rev = "75826706c6fc273dc18e13a2771a3cd922d3fea5";
      ref = "refs/tags/v1.6.2";
    };
  in
    import "${src}/build" {
      inherit src;
      nixpkgs =
        nixpkgs
        // {
          purity = fa-purity;
        };
    };

  utils-logger."${python_version}" = let
    src = projectPath observesIndex.common.utils_logger_2.root;
  in
    import src {
      inherit python_version src;
      nixpkgs =
        nixpkgs
        // {
          inherit fa-purity;
        };
    };

  out = import ./build {
    inherit python_version;
    nixpkgs =
      nixpkgs
      // {
        inherit arch-lint fa-purity fa-singer-io utils-logger;
      };
    src = ./.;
  };
in
  out
