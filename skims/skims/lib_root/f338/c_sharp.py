from collections.abc import (
    Iterator,
)
from lib_root.utilities.c_sharp import (
    yield_syntax_graph_object_creation,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)


def check_hashes_salt(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.CS_CHECK_HASHES_SALT

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for member in yield_syntax_graph_object_creation(
                graph, {"Rfc2898DeriveBytes"}
            ):
                if get_node_evaluation_results(method, graph, member, set()):
                    yield shard, member

    return get_vulnerabilities_from_n_ids(
        desc_key="criteria.vulns.338.description",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
