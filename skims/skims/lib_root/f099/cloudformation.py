from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_path.common import (
    FALSE_OPTIONS,
)
from lib_root.utilities.cloudformation import (
    get_attr_inside_attrs,
    get_attribute,
    iterate_resource,
    yield_statements_from_policy,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def _unencrypted_buckets(graph: Graph, nid: NId) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    bucket_encryption, _, _ = get_attribute(graph, val_id, "BucketEncryption")
    if not bucket_encryption:
        yield prop_id


def _bucket_policy_has_server_side_encryption_disabled(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "PolicyDocument")
    if policies[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[policies[2]]["value_id"]
        ):
            _, effect_val, _ = get_attribute(graph, stmt, "Effect")
            server_side_encryption, sse_val, sse_id = get_attr_inside_attrs(
                graph,
                stmt,
                ["Condition", "Null", "s3:x-amz-server-side-encryption"],
            )
            if (
                server_side_encryption
                and effect_val == "Allow"
                and sse_val in FALSE_OPTIONS
            ):
                yield sse_id


def cfn_bucket_policy_has_server_side_encryption_disabled(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_POLICY_SERVER_ENCRYP_DISABLED

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::S3::BucketPolicy"):
                for (
                    report
                ) in _bucket_policy_has_server_side_encryption_disabled(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f099.bckp_has_server_side_encryption_disabled",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_unencrypted_buckets(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_UNENCRYPTED_BUCKETS

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::S3::Bucket"):
                for report in _unencrypted_buckets(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f099.unencrypted_buckets",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
