lib: python_pkgs: let
  utils = import ./override_utils.nix;
  pkgs_overrides = override: pkgs: builtins.mapAttrs (_: override pkgs) pkgs;

  layer_1 = pkgs:
    pkgs
    // {
      types-python-dateutil = import ./types-python-dateutil.nix lib;
    };

  jsonschema_override = pkgs: utils.replace_pkg ["jsonschema"] (import ./jsonschema.nix lib pkgs);
  networkx_override = pkgs: utils.replace_pkg ["networkx"] (import ./networkx.nix lib pkgs);
  overrides = map pkgs_overrides [
    jsonschema_override
    networkx_override
    (_: utils.no_check_override)
  ];

  final_nixpkgs = utils.compose ([layer_1] ++ overrides) python_pkgs;
in {
  python_pkgs = final_nixpkgs;
}
