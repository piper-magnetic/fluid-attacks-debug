from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def get_suspicious_nodes(
    graph: Graph, dang_invocations: set[str]
) -> Iterator[NId]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            (node.get("label_type") == "MethodInvocation")
            and (node.get("expression") in dang_invocations)
        )

    for n_id in g.filter_nodes(graph, graph.nodes, predicate_matcher):
        yield n_id


def check_danger_arguments(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    if (
        (args_n_id := nodes[n_id].get("arguments_id"))
        and (args := g.adj_ast(graph, args_n_id))
        and (len(args) > 0 and len(args) % 2 == 0)
    ):
        for index in range(0, len(args), 2):
            if (nodes[args[index]].get("value")[1:-1] == "Accept") and (
                nodes[args[index + 1]].get("value")[1:-1] == "*/*"
            ):
                return True
    return False


def get_vuln_nodes_plain(
    graph: Graph, method: MethodsEnum, dang_invocations: set[str]
) -> Iterator[NId]:

    for n_id in get_suspicious_nodes(graph, dang_invocations):
        if get_node_evaluation_results(
            method, graph, n_id, {"all_myme_types_allowed"}
        ):
            yield n_id

    for n_id in g.matching_nodes(
        graph, label_type="ObjectCreation", name="Header"
    ):
        if check_danger_arguments(graph, n_id):
            yield n_id


def get_vuln_nodes_chain(
    graph: Graph, method: MethodsEnum, dang_invocations: set[str]
) -> Iterator[NId]:
    for n_id in get_suspicious_nodes(graph, dang_invocations):
        if check_danger_arguments(graph, n_id) and get_node_evaluation_results(
            method, graph, n_id, set()
        ):
            yield n_id


def java_http_accepts_any_mime_type(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JAVA_HTTP_REQ_ACCEPTS_ANY_MIMETYPE
    dang_invocations: set[str] = {
        "setRequestProperty",
        "header",
        "setHeader",
        "addHeader",
        "add",
    }

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_vuln_nodes_plain(graph, method, dang_invocations):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def java_accepts_any_mime_type_chain(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JAVA_ACCEPTS_ANY_MIMETYPE_CHAIN
    dang_invocations: set[str] = {"header", "setHeader", "headers"}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_vuln_nodes_chain(graph, method, dang_invocations):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
