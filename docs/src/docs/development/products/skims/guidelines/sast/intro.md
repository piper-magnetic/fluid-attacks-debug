---
id: intro
title: SAST Vulnerabilities
sidebar_label: Introduction
slug: /development/products/skims/guidelines/sast/intro
---

SAST refers to "Static Application Security Testing", and it is performed
by searching for deterministic vulnerabilities in code files.

There are two major libraries that group all the methods that search
vulnerabilities using this technique, lib_apk and lib_sast.

## LIB APK

As the name implies, this library checks vulnerabilities by reversing
Android APK files.

## Lib SAST

Methods included in this module are further
subdivided into two main libraries, lib_root and lib_path

### Lib root

The methods in this library use graph algorithms to search vulnerabilities in
code or configuration files written in the following languages:

- C#
- Java
- JavaScript
- TypeScript
- Dart
- Kotlin
- Go
- Python
- JSON
- HCL (Terraform files)
- YAML

### Lib path

The methods in this library perform vulnerability analysis by parsing a file
into iterable objects and performing search algorithms. This is specially
useful in configuration files that do not use complex control structures or
variable definitions.

Among the languages/extensions supported are:

- Bash scripts
- Dockerfiles
- Config files such as .xml, .jmx, .config
