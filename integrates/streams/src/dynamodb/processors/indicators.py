from boto3.dynamodb.conditions import (
    Attr,
)
from botocore.exceptions import (
    ClientError,
)
from dynamodb.resource import (
    TABLE_RESOURCE,
)
from dynamodb.types import (
    Item,
    Record,
    StreamEvent,
)
import logging

LOGGER = logging.getLogger(__name__)


def _format_indicators(indicators: Item) -> Item:
    return {
        f"unreliable_indicators.{key}": value
        for key, value in indicators.items()
        if value is not None
    }


def _format_nested_key(key: str) -> str:
    return key.replace(".", "_")


def _update_indicators(
    *,
    pk: str,
    sk: str,
    current: Item,
    new: Item,
) -> None:
    current_indicators = _format_indicators(current)
    new_indicators = _format_indicators(new)

    # Optimistic locking to prevent overwriting
    # if something else changed it first.
    condition_expression = Attr("pk").exists()
    for key in new_indicators.keys():
        if key in current_indicators:
            condition_expression &= Attr(key).eq(current_indicators[key])

    try:
        attrs_to_update = ",".join(
            f"{key} = :{_format_nested_key(key)}"
            for key in new_indicators.keys()
        )
        attr_values = {
            f":{_format_nested_key(key)}": value
            for key, value in new_indicators.items()
        }
        TABLE_RESOURCE.update_item(
            ConditionExpression=condition_expression,
            ExpressionAttributeValues=attr_values,
            Key={"pk": pk, "sk": sk},
            UpdateExpression=f"SET {attrs_to_update}",
        )
    except ClientError as ex:
        if ex.response["Error"]["Code"] == "ConditionalCheckFailedException":
            LOGGER.info(
                "Won't update indicators for %s due to optimistic locking", pk
            )
        else:
            raise


def process_roots(records: tuple[Record, ...]) -> None:
    for record in records:
        if record.new_image:
            if record.event_name == StreamEvent.INSERT:
                _update_indicators(
                    pk=record.pk,
                    sk=record.sk,
                    current={},
                    new={
                        "unreliable_last_status_update": record.new_image[
                            "created_date"
                        ]
                    },
                )

            elif record.event_name == StreamEvent.MODIFY and record.old_image:
                new_status: str = record.new_image["state"]["status"]
                old_status: str = record.old_image["state"]["status"]

                if new_status != old_status:
                    _update_indicators(
                        pk=record.pk,
                        sk=record.sk,
                        current=record.old_image["unreliable_indicators"],
                        new={
                            "unreliable_last_status_update": record.new_image[
                                "modified_date"
                            ],
                        },
                    )
