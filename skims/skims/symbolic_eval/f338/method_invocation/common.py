from symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

hashing_method = {"createHash", "crypto.createHash"}

random_methods = {"randomBytes", "crypto.randomBytes"}


def js_check_hashes_salt(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]["expression"]
    if ma_attr in "crypto.createHash":
        args.evaluation[args.n_id] = True
    if ma_attr in random_methods:
        args.triggers.add("usesMethod")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
