from aioextensions import (
    in_thread,
)
import aiohttp
import asyncio
from context import (
    FI_ENVIRONMENT,
)
from contextlib import (
    suppress,
)
from decimal import (
    Decimal,
    InvalidOperation,
)
import json
import logging
import logging.config
from mixpanel import (
    Mixpanel,
)
from settings import (
    LOGGING,
    MIXPANEL_API_SECRET,
    MIXPANEL_API_TOKEN,
    MIXPANEL_PROJECT_ID,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
MIXPANEL_QUERY_PR0FILE_URL = "https://mixpanel.com/api/2.0/engage?project_id="


def is_decimal(num: str) -> bool:
    try:
        Decimal(num)
        return True
    except InvalidOperation:
        with suppress(InvalidOperation):
            Decimal(num[:-1])
            return True
        return False


async def mixpanel_track(email: str, event: str, **extra: str) -> None:
    if FI_ENVIRONMENT == "production":
        await in_thread(
            Mixpanel(MIXPANEL_API_TOKEN).track,
            email,
            event,
            {"integrates_user_email": email, **extra},
        )


async def get_mixpanel_profile(
    email: str,
) -> dict[
    str, int | list[dict[str, dict[str, int | str] | str] | str] | str
] | None:
    payload: dict[str, str] = dict(
        distinct_id=email,
    )
    headers: dict[str, str] = {
        "accept": "application/json",
        "content-type": "application/x-www-form-urlencoded",
    }
    retries: int = 0
    retry: bool = True
    async with aiohttp.ClientSession(headers=headers) as session:
        while retry and retries < 10:
            async with session.post(
                f"{MIXPANEL_QUERY_PR0FILE_URL}{MIXPANEL_PROJECT_ID}",
                data=json.dumps(payload),
                auth=aiohttp.BasicAuth(MIXPANEL_API_SECRET),
            ) as response:
                try:
                    result = await response.json()
                except (
                    json.decoder.JSONDecodeError,
                    aiohttp.ClientError,
                ) as exc:
                    LOGGER.exception(exc, extra=dict(extra=locals()))
                    break
                if not response.ok:
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return result

    return None
