from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def is_plain_text_credentials(graph: Graph, n_id: NId) -> bool:
    args_ids = g.adj_ast(graph, n_id)
    if len(args_ids) < 2:
        return False

    for _id in args_ids[1:]:
        n_attrs = graph.nodes[_id]
        if n_attrs["label_type"] == "Literal":
            return True
    return False


def cs_plain_text_keys(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.C_SHARP_PLAIN_TEXT_KEYS

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in g.matching_nodes(graph, label_type="MethodInvocation"):
                n_attrs = graph.nodes[n_id]
                m_name = n_attrs["expression"].split(".")[-1]
                if (
                    m_name == "decrypt"
                    and (al_id := n_attrs.get("arguments_id"))
                    and is_plain_text_credentials(graph, al_id)
                    and get_node_evaluation_results(
                        method, graph, n_attrs["expression_id"], {"cryptlib"}
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f169.plain_text_keys",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
