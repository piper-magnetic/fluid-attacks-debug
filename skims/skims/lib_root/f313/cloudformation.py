from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.json import (
    is_parent,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils.graph import (
    matching_nodes,
)


def cfn_insecure_certificate(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.CFN_INSECURE_CERTIFICATE

    def n_ids() -> Iterator[GraphShardNode]:
        """
        Source:
        https://nodejs.org/api/cli.html#node_tls_reject_unauthorizedvalue
        """
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in matching_nodes(graph, label_type="Pair"):
                key_id = graph.nodes[nid]["key_id"]
                value_id = graph.nodes[nid]["value_id"]

                if (
                    graph.nodes[key_id]["value"]
                    == "NODE_TLS_REJECT_UNAUTHORIZED"
                    and graph.nodes[value_id].get("value") == "0"
                    and is_parent(
                        graph,
                        nid,
                        [
                            "environment",
                        ],
                    )
                ):
                    yield shard, nid

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f313.unsafe_certificate_validation",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
