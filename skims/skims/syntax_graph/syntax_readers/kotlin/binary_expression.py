from model.graph_model import (
    NId,
)
from syntax_graph.syntax_nodes.binary_operation import (
    build_binary_operation_node,
)
from syntax_graph.types import (
    SyntaxGraphArgs,
)
from utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    left_id = graph.nodes[args.n_id]["label_field_left"]
    right_id = graph.nodes[args.n_id]["label_field_right"]
    if graph.nodes[args.n_id]["label_type"] == "elvis_expression":
        operator_id = adj_ast(graph, args.n_id)[1]
    else:
        operator_id = graph.nodes[args.n_id]["label_field_operator"]
    operator = graph.nodes[operator_id]["label_text"]
    return build_binary_operation_node(args, operator, left_id, right_id)
