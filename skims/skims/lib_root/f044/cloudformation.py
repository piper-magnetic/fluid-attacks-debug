from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.json import (
    is_parent,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils import (
    graph as g,
)


def resource_has_https_methods_enabled(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CNF_HTTP_METHODS_ENABLED
    parents_path = ["MethodSettings", "Properties"]

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in g.matching_nodes(graph, label_type="Pair"):
                key_id = graph.nodes[nid]["key_id"]
                value_id = graph.nodes[nid]["value_id"]

                if (
                    graph.nodes[key_id]["value"] == "HttpMethod"
                    and is_parent(graph, nid, parents_path)
                    and graph.nodes[value_id]["label_type"] == "Literal"
                    and graph.nodes[value_id]["value"] in {"*", "'*'"}
                ):
                    yield shard, nid

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f044.resource_has_https_methods_enabled",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def list_has_danger_values(graph: Graph, nid: NId) -> bool:
    child_ids = g.adj_ast(graph, nid)
    for c_id in child_ids:
        curr_value = graph.nodes[c_id].get("value")
        if curr_value and curr_value in {"POST", "DELETE"}:
            return True
    return False


def resource_has_danger_https_methods_enabled(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CNF_HTTP_METHODS_ENABLED
    parents_path = ["CorsRules", "CorsConfiguration", "Properties"]

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in g.matching_nodes(graph, label_type="Pair"):
                key_id = graph.nodes[nid]["key_id"]
                value_id = graph.nodes[nid]["value_id"]

                if (
                    graph.nodes[key_id]["value"] == "AllowedMethods"
                    and is_parent(graph, nid, parents_path)
                    and graph.nodes[value_id]["label_type"]
                    == "ArrayInitializer"
                    and list_has_danger_values(graph, value_id)
                ):
                    yield shard, nid

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f044.resource_has_danger_https_methods_enabled",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
