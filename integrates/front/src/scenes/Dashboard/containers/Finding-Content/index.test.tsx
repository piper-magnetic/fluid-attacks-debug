import { MockedProvider } from "@apollo/client/testing";
import type { MockedResponse } from "@apollo/client/testing";
import { PureAbility } from "@casl/ability";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import React from "react";
import { MemoryRouter, Route } from "react-router-dom";

import { FindingContent } from "scenes/Dashboard/containers/Finding-Content";
import {
  GET_FINDING_HEADER,
  REMOVE_FINDING_MUTATION,
} from "scenes/Dashboard/containers/Finding-Content/queries";
import { authzGroupContext, authzPermissionsContext } from "utils/authz/config";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("../../../../utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("../../../../utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const mockHistoryReplace: jest.Mock = jest.fn();

jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual<Record<string, unknown>>("react-router-dom"),
    useHistory: (): { replace: (path: string) => void } => ({
      replace: mockHistoryReplace,
    }),
  })
);

describe("FindingContent", (): void => {
  const btnCancel = "components.modal.cancel";
  const btnConfirm = "components.modal.confirm";

  const findingMock: Readonly<MockedResponse> = {
    request: {
      query: GET_FINDING_HEADER,
      variables: {
        canRetrieveHacker: true,
        findingId: "438679960",
      },
    },
    result: {
      data: {
        finding: {
          closedVulns: 0,
          currentState: "APPROVED",
          hacker: "machine@fluidattacks.com",
          id: "438679960",
          minTimeToRemediate: 60,
          openVulns: 3,
          releaseDate: "2018-12-04 09:04:13",
          reportDate: "2017-12-04 09:04:13",
          severityScore: 2.6,
          status: "VULNERABLE",
          title: "050. Guessed weak credentials",
          tracking: [
            {
              closed: 0,
              cycle: 0,
              date: "2019-08-30",
              effectiveness: 0,
              open: 1,
            },
          ],
        },
      },
    },
  };

  const removeFindingMock: Readonly<MockedResponse> = {
    request: {
      query: GET_FINDING_HEADER,
      variables: {
        canRetrieveHacker: true,
        findingId: "438679960",
      },
    },
    result: {
      data: {
        finding: {
          closedVulns: 0,
          currentState: "CREATED",
          id: "438679960",
          minTimeToRemediate: 60,
          openVulns: 3,
          releaseDate: null,
          reportDate: "2017-12-04 09:04:13",
          severityScore: 2.6,
          status: "VULNERABLE",
          title: "050. Guessed weak credentials",
          tracking: [
            {
              closed: 0,
              cycle: 0,
              date: "2019-08-30",
              effectiveness: 0,
              open: 1,
            },
          ],
        },
      },
    },
  };

  it("should return a function", (): void => {
    expect.hasAssertions();

    expect(typeof FindingContent).toBe("function");
  });

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    // eslint-disable-next-line
    const mockedPermissions = new PureAbility<string>([
      { action: "api_mutations_submit_draft_mutate" },
      { action: "api_resolvers_finding_hacker_resolve" },
    ]);

    render(
      <MemoryRouter initialEntries={["/TEST/vulns/438679960/description"]}>
        <MockedProvider addTypename={false} mocks={[findingMock]}>
          <authzPermissionsContext.Provider value={mockedPermissions}>
            <authzGroupContext.Provider
              value={
                new PureAbility([
                  { action: "can_report_vulnerabilities" },
                  { action: "api_resolvers_finding_hacker_resolve" },
                ])
              }
            >
              <Route
                component={FindingContent}
                path={"/:groupName/vulns/:findingId/description"}
              />
            </authzGroupContext.Provider>
          </authzPermissionsContext.Provider>
        </MockedProvider>
      </MemoryRouter>
    );

    // Including heading inside `ContentTab`
    const numberOfHeading: number = 1;
    await waitFor((): void => {
      expect(screen.queryAllByRole("heading")).toHaveLength(numberOfHeading);
    });
  });

  it("should render header", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    const mockedPermissions = new PureAbility<string>([
      { action: "api_mutations_submit_draft_mutate" },
      { action: "api_resolvers_finding_hacker_resolve" },
    ]);
    render(
      <MemoryRouter initialEntries={["/TEST/vulns/438679960/description"]}>
        <MockedProvider addTypename={false} mocks={[findingMock]}>
          <authzPermissionsContext.Provider value={mockedPermissions}>
            <authzGroupContext.Provider
              value={
                new PureAbility([
                  { action: "can_report_vulnerabilities" },
                  { action: "api_resolvers_finding_hacker_resolve" },
                ])
              }
            >
              <Route
                component={FindingContent}
                path={"/:groupName/vulns/:findingId/description"}
              />
            </authzGroupContext.Provider>
          </authzPermissionsContext.Provider>
        </MockedProvider>
      </MemoryRouter>
    );
    await waitFor((): void => {
      expect(screen.queryByRole("heading", { level: 1 })).toBeInTheDocument();
    });

    expect(screen.getAllByRole("heading")[0].textContent).toContain(
      "050. Guessed weak credentials"
    );
  });

  it("should prompt delete justification", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "api_mutations_remove_finding_mutate" },
      { action: "api_resolvers_finding_hacker_resolve" },
    ]);
    render(
      <MemoryRouter initialEntries={["/TEST/vulns/438679960/description"]}>
        <MockedProvider addTypename={false} mocks={[removeFindingMock]}>
          <authzPermissionsContext.Provider value={mockedPermissions}>
            <authzGroupContext.Provider
              value={
                new PureAbility([
                  { action: "can_report_vulnerabilities" },
                  { action: "api_resolvers_finding_hacker_resolve" },
                ])
              }
            >
              <Route
                component={FindingContent}
                path={"/:groupName/vulns/:findingId/description"}
              />
            </authzGroupContext.Provider>
          </authzPermissionsContext.Provider>
        </MockedProvider>
      </MemoryRouter>
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.btn.text")
      ).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("button")).toHaveLength(3);

    await userEvent.click(screen.getByText("searchFindings.delete.btn.text"));
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.title")
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByText(btnCancel));
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.title")
      ).not.toBeInTheDocument();
    });
  });

  it("should delete finding", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const deleteMutationMock: Readonly<MockedResponse> = {
      request: {
        query: REMOVE_FINDING_MUTATION,
        variables: {
          findingId: "438679960",
          justification: "DUPLICATED",
        },
      },
      result: {
        data: {
          removeFinding: {
            success: true,
          },
        },
      },
    };

    const mockedPermissions = new PureAbility<string>([
      { action: "api_mutations_remove_finding_mutate" },
      { action: "api_resolvers_finding_hacker_resolve" },
    ]);
    render(
      <MemoryRouter initialEntries={["/TEST/vulns/438679960/description"]}>
        <MockedProvider
          addTypename={false}
          mocks={[removeFindingMock, deleteMutationMock]}
        >
          <authzPermissionsContext.Provider value={mockedPermissions}>
            <authzGroupContext.Provider
              value={
                new PureAbility([
                  { action: "can_report_vulnerabilities" },
                  { action: "api_resolvers_finding_hacker_resolve" },
                ])
              }
            >
              <Route
                component={FindingContent}
                path={"/:groupName/vulns/:findingId/description"}
              />
            </authzGroupContext.Provider>
          </authzPermissionsContext.Provider>
        </MockedProvider>
      </MemoryRouter>
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.btn.text")
      ).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText("searchFindings.delete.btn.text"));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.title")
      ).toBeInTheDocument();
    });
    await userEvent.selectOptions(
      screen.getByRole("combobox", { name: "justification" }),
      ["DUPLICATED"]
    );

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it("should handle deletion errors", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const deleteMutationMock: Readonly<MockedResponse> = {
      request: {
        query: REMOVE_FINDING_MUTATION,
        variables: {
          findingId: "438679960",
          justification: "DUPLICATED",
        },
      },
      result: {
        errors: [new GraphQLError("Unexpected error")],
      },
    };

    const mockedPermissions = new PureAbility<string>([
      { action: "api_mutations_remove_finding_mutate" },
      { action: "api_resolvers_finding_hacker_resolve" },
    ]);
    render(
      <MemoryRouter initialEntries={["/TEST/vulns/438679960/description"]}>
        <MockedProvider
          addTypename={false}
          mocks={[removeFindingMock, deleteMutationMock]}
        >
          <authzPermissionsContext.Provider value={mockedPermissions}>
            <authzGroupContext.Provider
              value={
                new PureAbility([
                  { action: "can_report_vulnerabilities" },
                  { action: "api_resolvers_finding_hacker_resolve" },
                ])
              }
            >
              <Route
                component={FindingContent}
                path={"/:groupName/vulns/:findingId/description"}
              />
            </authzGroupContext.Provider>
          </authzPermissionsContext.Provider>
        </MockedProvider>
      </MemoryRouter>
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.btn.text")
      ).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText("searchFindings.delete.btn.text"));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.delete.title")
      ).toBeInTheDocument();
    });
    await userEvent.selectOptions(
      screen.getByRole("combobox", { name: "justification" }),
      ["DUPLICATED"]
    );

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(1);
    });
  });
});
