from collections.abc import (
    Iterator,
)
from lib_root.utilities.docker import (
    iterate_images,
    validate_path,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
import re
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def _compose_image_has_digest(value: str, nid: NId) -> Iterator[NId]:
    env_var_re: re.Pattern = re.compile(r"\{.+\}")
    digest_re: re.Pattern = re.compile(".*@sha256:[a-fA-F0-9]{64}")
    if not (env_var_re.search(value) or digest_re.search(value)):
        yield nid


def docker_compose_image_has_digest(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.DOCKER_COMPOSE_IMAGE_HAS_DIGEST

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.YAML):
            if shard.syntax_graph is None or not validate_path(shard.path):
                continue
            graph = shard.syntax_graph

            for value, nid in iterate_images(graph):
                for report in _compose_image_has_digest(value, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f380.bash_image_has_digest",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
