from collections.abc import (
    Iterator,
)
from lib_root.utilities.python import (
    get_danger_imported_names,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)

DANG_LIBRARIES = {
    "urllib.request.Request",
    "urllib.request.Request.add_header",
    "urllib.request.Request.add_unredirected_header",
    "urllib.request.urlopen",
    "urllib2.Request",
    "urllib3.util.make_headers",
    "treq.delete",
    "treq.get",
    "treq.head",
    "treq.patch",
    "treq.post",
    "treq.put",
    "treq.request",
    "requests.delete",
    "requests.get",
    "requests.head",
    "requests.patch",
    "requests.post",
    "requests.put",
    "requests.request",
}

DANG_CLASSES = {
    "http.client.HTTPSConnection",
    "http.client.HTTPConnection",
    "httplib.HTTPSConnection",
    "httplib.HTTPConnection",
    "urllib.request.Request",
    "urllib3.PoolManager",
    "httplib2.Http",
}


DANG_METHODS = {
    "request",
    "add_header",
}


def get_danger_invocations(
    graph: Graph, dang_names: set[str]
) -> tuple[NId, ...]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            (node.get("label_type") == "MethodInvocation")
            and (node.get("expression") in dang_names)
        )

    return g.filter_nodes(graph, graph.nodes, predicate_matcher)


def get_vuln_nodes(graph: Graph, method: MethodsEnum) -> set[NId]:
    vuln_nodes: set[NId] = set()

    danger_libraries = get_danger_imported_names(graph, DANG_LIBRARIES)

    for n_id in get_danger_invocations(graph, danger_libraries):
        if get_node_evaluation_results(
            method, graph, n_id, {"accept", "*/*"}, False
        ):
            vuln_nodes.add(n_id)

    return vuln_nodes


def python_accepts_any_mime_type(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.PYTHON_ACCEPTS_ANY_MIME

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.PYTHON):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_vuln_nodes(graph, method):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def get_dang_instances(graph: Graph, dang_classes: set[str]) -> set[str]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            (node.get("label_type") == "MethodInvocation")
            and (node.get("expression") in dang_classes)
        )

    dang_var_names: set[str] = set()
    nodes = graph.nodes
    for n_id in g.filter_nodes(graph, nodes, predicate_matcher):
        if (parent_n_id := next(iter(g.pred(graph, n_id)), None)) and (
            nodes[parent_n_id].get("label_type") == "VariableDeclaration"
        ):
            dang_var_names.add(nodes[parent_n_id].get("variable"))
    return dang_var_names


def get_danger_instances_invocations(
    graph: Graph, dang_instances: set[str]
) -> tuple[str, ...]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            (node.get("label_type") == "MethodInvocation")
            and (exp := node.get("expression"))
            and (exp.split(".")[0] in dang_instances)
            and (index := exp.find("."))
            and (index != -1)
            and (exp[index + 1 :] in DANG_METHODS)
        )

    return g.filter_nodes(graph, graph.nodes, predicate_matcher)


def get_vuln_nodes_instance(graph: Graph, method: MethodsEnum) -> set[NId]:
    vuln_nodes: set[NId] = set()
    dang_classes = get_danger_imported_names(graph, DANG_CLASSES)
    danger_instances = get_dang_instances(graph, dang_classes)

    for n_id in get_danger_instances_invocations(graph, danger_instances):
        if (args_n_id := g.match_ast_d(graph, n_id, "ArgumentList")) and (
            get_node_evaluation_results(
                method, graph, args_n_id, {"accept", "*/*"}, False
            )
        ):
            vuln_nodes.add(n_id)

    return vuln_nodes


def python_accepts_any_mime_instance(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.PYTHON_ACCEPTS_ANY_MIME_INSTANCE

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.PYTHON):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_vuln_nodes_instance(graph, method):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
