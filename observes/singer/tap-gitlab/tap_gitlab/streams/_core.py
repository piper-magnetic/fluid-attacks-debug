from dataclasses import (
    dataclass,
)
from enum import (
    Enum,
)
from tap_gitlab.api.projects.ids import (
    ProjectId,
)
from tap_gitlab.api.projects.jobs.page import (
    Scope as JobScope,
)
from tap_gitlab.api.projects.merge_requests.data_page import (
    Scope as MrScope,
    State as MrState,
)
from typing import (
    Tuple,
)


class SupportedStreams(Enum):
    JOBS = "JOBS"
    MERGE_REQUESTS = "MERGE_REQUESTS"
    ALL = "ALL"


@dataclass(frozen=True)
class MrStream:
    project: ProjectId
    scope: MrScope
    mr_state: MrState


@dataclass(frozen=True)
class JobStream:
    project: ProjectId
    scopes: Tuple[JobScope, ...]


class DecodeError(Exception):
    pass
