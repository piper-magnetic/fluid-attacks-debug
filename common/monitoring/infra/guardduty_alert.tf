resource "aws_sns_topic" "guardduty_alert_us_east_2" {
  name     = "guardduty_alert"
  provider = aws.us-east-2

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "AWSAnomalyDetectionSNSPublishingPermissions",
          Effect = "Allow",
          Principal = {
            Service = ["costalerts.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"               = "guardduty_alert"
    "management:area"    = "cost"
    "management:product" = "common"
    "management:type"    = "product"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert_us_east_2" {
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert_us_east_2.arn
  provider  = aws.us-east-2
}

resource "aws_cloudwatch_event_rule" "guardduty_alert_us_east_2" {
  name     = "guardduty_alert"
  provider = aws.us-east-2

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = [
        1,
        1.0,
        1.1,
        1.2,
        1.3,
        1.4,
        1.5,
        1.6,
        1.7,
        1.8,
        1.9,
        2,
        2.0,
        2.1,
        2.2,
        2.3,
        2.4,
        2.5,
        2.6,
        2.7,
        2.8,
        2.9,
        3,
        3.0,
        3.1,
        3.2,
        3.3,
        3.4,
        3.5,
        3.6,
        3.7,
        3.8,
        3.9,
        4,
        4.0,
        4.1,
        4.2,
        4.3,
        4.4,
        4.5,
        4.6,
        4.7,
        4.8,
        4.9,
        5,
        5.0,
        5.1,
        5.2,
        5.3,
        5.4,
        5.5,
        5.6,
        5.7,
        5.8,
        5.9,
        6,
        6.0,
        6.1,
        6.2,
        6.3,
        6.4,
        6.5,
        6.6,
        6.7,
        6.8,
        6.9,
        7,
        7.0,
        7.1,
        7.2,
        7.3,
        7.4,
        7.5,
        7.6,
        7.7,
        7.8,
        7.9,
        8,
        8.0,
        8.1,
        8.2,
        8.3,
        8.4,
        8.5,
        8.6,
        8.7,
        8.8,
        8.9
      ],
    }
  })

  tags = {
    "Name"               = "guardduty_alert"
    "management:area"    = "cost"
    "management:product" = "common"
    "management:type"    = "product"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert_us_east_2" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert_us_east_2.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert_us_east_2.arn
  provider  = aws.us-east-2

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}

resource "aws_sns_topic" "guardduty_alert_us_west_1" {
  name     = "guardduty_alert"
  provider = aws.us-west-1

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "AWSAnomalyDetectionSNSPublishingPermissions",
          Effect = "Allow",
          Principal = {
            Service = ["costalerts.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"               = "guardduty_alert"
    "management:area"    = "cost"
    "management:product" = "common"
    "management:type"    = "product"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert_us_west_1" {
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert_us_west_1.arn
  provider  = aws.us-west-1
}

resource "aws_cloudwatch_event_rule" "guardduty_alert_us_west_1" {
  name     = "guardduty_alert"
  provider = aws.us-west-1

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = [
        1,
        1.0,
        1.1,
        1.2,
        1.3,
        1.4,
        1.5,
        1.6,
        1.7,
        1.8,
        1.9,
        2,
        2.0,
        2.1,
        2.2,
        2.3,
        2.4,
        2.5,
        2.6,
        2.7,
        2.8,
        2.9,
        3,
        3.0,
        3.1,
        3.2,
        3.3,
        3.4,
        3.5,
        3.6,
        3.7,
        3.8,
        3.9,
        4,
        4.0,
        4.1,
        4.2,
        4.3,
        4.4,
        4.5,
        4.6,
        4.7,
        4.8,
        4.9,
        5,
        5.0,
        5.1,
        5.2,
        5.3,
        5.4,
        5.5,
        5.6,
        5.7,
        5.8,
        5.9,
        6,
        6.0,
        6.1,
        6.2,
        6.3,
        6.4,
        6.5,
        6.6,
        6.7,
        6.8,
        6.9,
        7,
        7.0,
        7.1,
        7.2,
        7.3,
        7.4,
        7.5,
        7.6,
        7.7,
        7.8,
        7.9,
        8,
        8.0,
        8.1,
        8.2,
        8.3,
        8.4,
        8.5,
        8.6,
        8.7,
        8.8,
        8.9
      ],
    }
  })

  tags = {
    "Name"               = "guardduty_alert"
    "management:area"    = "cost"
    "management:product" = "common"
    "management:type"    = "product"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert_us_west_1" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert_us_west_1.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert_us_west_1.arn
  provider  = aws.us-west-1

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}

resource "aws_sns_topic" "guardduty_alert_us_west_2" {
  name     = "guardduty_alert"
  provider = aws.us-west-2

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "AWSAnomalyDetectionSNSPublishingPermissions",
          Effect = "Allow",
          Principal = {
            Service = ["costalerts.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"               = "guardduty_alert"
    "management:area"    = "cost"
    "management:product" = "common"
    "management:type"    = "product"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert_us_west_2" {
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert_us_west_2.arn
  provider  = aws.us-west-2
}

resource "aws_cloudwatch_event_rule" "guardduty_alert_us_west_2" {
  name     = "guardduty_alert"
  provider = aws.us-west-2

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = [
        1,
        1.0,
        1.1,
        1.2,
        1.3,
        1.4,
        1.5,
        1.6,
        1.7,
        1.8,
        1.9,
        2,
        2.0,
        2.1,
        2.2,
        2.3,
        2.4,
        2.5,
        2.6,
        2.7,
        2.8,
        2.9,
        3,
        3.0,
        3.1,
        3.2,
        3.3,
        3.4,
        3.5,
        3.6,
        3.7,
        3.8,
        3.9,
        4,
        4.0,
        4.1,
        4.2,
        4.3,
        4.4,
        4.5,
        4.6,
        4.7,
        4.8,
        4.9,
        5,
        5.0,
        5.1,
        5.2,
        5.3,
        5.4,
        5.5,
        5.6,
        5.7,
        5.8,
        5.9,
        6,
        6.0,
        6.1,
        6.2,
        6.3,
        6.4,
        6.5,
        6.6,
        6.7,
        6.8,
        6.9,
        7,
        7.0,
        7.1,
        7.2,
        7.3,
        7.4,
        7.5,
        7.6,
        7.7,
        7.8,
        7.9,
        8,
        8.0,
        8.1,
        8.2,
        8.3,
        8.4,
        8.5,
        8.6,
        8.7,
        8.8,
        8.9
      ],
    }
  })

  tags = {
    "Name"               = "guardduty_alert"
    "management:area"    = "cost"
    "management:product" = "common"
    "management:type"    = "product"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert_us_west_2" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert_us_west_2.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert_us_west_2.arn
  provider  = aws.us-west-2

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}
