from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from fa_purity import (
    Cmd,
)
from target_s3.core import (
    RecordGroup,
    TempReadOnlyFile,
)
from typing import (
    Callable,
    Generic,
    TypeVar,
)

_T = TypeVar("_T")


@dataclass(frozen=True)
class _Patch(Generic[_T]):
    inner: _T


@dataclass(frozen=True)
class CsvKeeper:
    _save: _Patch[Callable[[RecordGroup], Cmd[TempReadOnlyFile]]]

    def save(self, group: RecordGroup) -> Cmd[TempReadOnlyFile]:
        return self._save.inner(group)

    @staticmethod
    def new(save: Callable[[RecordGroup], Cmd[TempReadOnlyFile]]) -> CsvKeeper:
        return CsvKeeper(_Patch(save))
