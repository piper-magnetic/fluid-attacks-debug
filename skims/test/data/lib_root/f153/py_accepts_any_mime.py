# pylint: disable-all
# type: ignore

import requests
import urllib2
import urllib as urllib_alias
from urllib import (
    request as request_alias,
)

dang_headers = {"Content-Type": "application/json", "Accept": "*/*"}
safe_headers = {"Content-Type": "application/json"}

data = {"name": "John Doe", "age": 30}

# Must fail
response = requests.post(
    "https://example.com/api", headers=dang_headers, json=data
)

# Must pass
response_ii = requests.post(
    "https://example.com/api", headers=safe_headers, json=data
)

# Must Fail
req = urllib_alias.request.Request(
    "https://example.com/api", headers=dang_headers, data=data
)

response = request_alias.urlopen("https://example.com", headers=dang_headers)

request = urllib2.Request("https://thewebsite.com", headers=dang_headers)

# Must pass
req = urllib_alias.request.Request(
    "https://example.com/api", headers=safe_headers, data=data
)
response = request_alias.urlopen("https://example.com", headers=safe_headers)

request = urllib2.Request("https://thewebsite.com", headers=safe_headers)
