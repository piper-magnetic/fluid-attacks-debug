from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenList,
)
from more_itertools import (
    windowed,
)
from tap_gitlab.intervals.interval import (
    IntervalPoint,
    InvalidInterval,
    MAX,
    MIN,
    OpenLeftInterval,
)
from tap_gitlab.intervals.interval.factory import (
    IntervalFactory,
)
from typing import (
    Generic,
    Optional,
    TypeVar,
)

_Point = TypeVar("_Point")


@dataclass(frozen=True)
class _Private:
    pass


class InvalidEndpoints(Exception):
    pass


def _to_endpoints(
    intervals: FrozenList[OpenLeftInterval[_Point]],
) -> FrozenList[IntervalPoint[_Point]]:
    endpoints: FrozenList[IntervalPoint[_Point]] = tuple()
    if not intervals:
        raise InvalidInterval("Empty intervals")
    for interval in intervals:
        if not endpoints:
            endpoints = endpoints + (interval.lower, interval.upper)
        else:
            if endpoints[-1] == interval.lower:
                endpoints = endpoints + (interval.upper,)
            else:
                raise InvalidInterval(
                    f"discontinuous: {endpoints[-1]} + {interval}"
                )
    return endpoints


def _to_intervals(
    factory: IntervalFactory[_Point],
    endpoints: FrozenList[IntervalPoint[_Point]],
) -> FrozenList[OpenLeftInterval[_Point]]:
    def _new_interval(
        p_1: Optional[IntervalPoint[_Point]],
        p_2: Optional[IntervalPoint[_Point]],
    ) -> OpenLeftInterval[_Point]:
        if (
            p_1
            and p_2
            and not isinstance(p_1, MAX)
            and not isinstance(p_2, (MIN, MAX))
        ):
            return factory.new_lopen(p_1, p_2)
        raise InvalidEndpoints()

    return tuple(
        _new_interval(p_1, p_2) for p_1, p_2 in windowed(endpoints, 2)
    )


@dataclass(frozen=True)
class FragmentedInterval(Generic[_Point]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    endpoints: FrozenList[IntervalPoint[_Point]]
    intervals: FrozenList[OpenLeftInterval[_Point]]


@dataclass(frozen=True)
class FIntervalFactory(Generic[_Point]):
    factory: IntervalFactory[_Point]

    def from_endpoints(
        self, endpoints: FrozenList[IntervalPoint[_Point]]
    ) -> FragmentedInterval[_Point]:
        return FragmentedInterval(
            _Private(), endpoints, _to_intervals(self.factory, endpoints)
        )

    # pylint: disable=no-self-use
    def from_intervals(
        self, intervals: FrozenList[OpenLeftInterval[_Point]]
    ) -> FragmentedInterval[_Point]:
        endpoints = _to_endpoints(intervals)
        return FragmentedInterval(_Private(), endpoints, intervals)

    def append_to(
        self, f_interval: FragmentedInterval[_Point], point: _Point
    ) -> FragmentedInterval[_Point]:
        return self.from_endpoints(f_interval.endpoints + (point,))
