from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
import utils.graph as g


def kt_remote_command_exec(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.KT_REMOTE_COMMAND_EXECUTION
    danger_methods = {"loadLibrary", "exec"}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.KOTLIN):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in g.matching_nodes(
                graph,
                label_type="MemberAccess",
            ):
                n_attrs = graph.nodes[n_id]
                parent = g.pred_ast(graph, n_id)
                arg_list = graph.nodes[parent[0]].get("arguments_id")

                if (
                    n_attrs["member"] in danger_methods
                    and (child := g.adj_ast(graph, n_id)[0])
                    and get_node_evaluation_results(
                        method, graph, child, {"getRuntime"}
                    )
                    and arg_list
                    and get_node_evaluation_results(
                        method, graph, arg_list, {"UserConnection"}
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f004.remote_command_execution",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
