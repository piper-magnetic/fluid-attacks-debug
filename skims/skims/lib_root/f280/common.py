from model.core_model import (
    MethodsEnum,
)
from model.graph_model import (
    Graph,
    NId,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def _has_dangerous_literal(graph: Graph, args: dict) -> bool:
    sensitive_params = {'"Set-Cookie"', '"connect.sid"'}
    if (
        len(args) == 2
        and (first_param := graph.nodes[args["__0__"]])
        and (first_param.get("label_type") == "Literal")
        and (first_param.get("value") in sensitive_params)
    ):
        return True
    return False


def has_dangerous_param(method: MethodsEnum, graph: Graph) -> list[NId]:
    vuln_nodes: list[NId] = []
    sensitive_methods = {"res.setHeader", "res.cookie"}

    for member in g.matching_nodes(graph, label_type="MethodInvocation"):
        if (
            graph.nodes[member].get("expression") in sensitive_methods
            and (args_id := graph.nodes[member].get("arguments_id"))
            and (args := g.match_ast(graph, args_id))
            and (_has_dangerous_literal(graph, args))
            and get_node_evaluation_results(method, graph, member, set())
        ):
            vuln_nodes.append(member)

    return vuln_nodes
