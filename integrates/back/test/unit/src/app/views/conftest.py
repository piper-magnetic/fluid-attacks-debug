from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from db_model.stakeholders.types import (
    NotificationsParameters,
    NotificationsPreferences,
    Stakeholder,
    StakeholderState,
    StakeholderTours,
)
from decimal import (
    Decimal,
)
import pytest
from typing import (
    Any,
)

pytestmark = [
    pytest.mark.asyncio,
]


MOCKED_DATA: dict[str, dict[str, Any]] = {
    "app.views.auth.Dataloaders.stakeholder": {
        '["integratesuser2@fluidattacks.com"]': Stakeholder(
            email="integratesuser2@fluidattacks.com",
            access_token=None,
            enrolled=True,
            first_name="Integrates",
            is_concurrent_session=False,
            is_registered=True,
            last_login_date=datetime.fromisoformat(
                "2020-12-31T18:40:37+00:00"
            ),
            last_api_token_use_date=None,
            last_name="Internal Manager",
            legal_remember=True,
            phone=None,
            registration_date=datetime.fromisoformat(
                "2018-02-28T16:54:12+00:00"
            ),
            role="user",
            session_key=None,
            session_token=None,
            state=StakeholderState(
                modified_by=None,
                modified_date=None,
                notifications_preferences=NotificationsPreferences(
                    available=[],
                    email=[],
                    sms=[],
                    parameters=NotificationsParameters(
                        min_severity=Decimal("3.0")
                    ),
                ),
            ),
            tours=StakeholderTours(
                new_group=False,
                new_root=False,
                new_risk_exposure=False,
                welcome=False,
            ),
        )
    },
    "app.views.auth.stakeholders_domain.update_last_login": {
        '["integratesuser2@fluidattacks.com"]': None,
    },
    "app.views.auth.utils.send_autoenroll_mixpanel_event": {
        '["integratesuser2@fluidattacks.com"]': None,
    },
}


@pytest.fixture
def mocked_data_for_module(
    *,
    resolve_mocked_data: Any,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mocked_data(
            mocked_data=MOCKED_DATA,
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module
