from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
import utils.graph as g


def kt_default_http_client_deprecated(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.KT_DEFAULT_HTTP_CLIENT_DEPRECATED
    danger_methods = {"DefaultHttpClient"}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.KOTLIN):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in g.matching_nodes(
                graph,
                label_type="MethodInvocation",
            ):
                n_attrs = graph.nodes[n_id]

                if n_attrs["expression"] in danger_methods:
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f016.default_http_client_deprecated",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
