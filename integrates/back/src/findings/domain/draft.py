from dataloaders import (
    Dataloaders,
)
from db_model import (
    findings as findings_model,
)
from db_model.enums import (
    Source,
)
from db_model.findings.enums import (
    FindingStateStatus,
)
from db_model.findings.types import (
    Cvss31Severity,
    Finding,
    FindingState,
)
from decimal import (
    Decimal,
)
from findings.types import (
    FindingDraftToAdd,
)
from newutils import (
    cvss as cvss_utils,
    datetime as datetime_utils,
    findings as findings_utils,
)
from newutils.validations import (
    check_and_set_min_time_to_remediate,
    validate_all_fields_length_deco,
    validate_field_length,
    validate_fields_deco,
    validate_no_duplicate_drafts_deco,
)
from typing import (
    Any,
)
import uuid


def validate_draft_inputs(*, kwargs: list[str]) -> None:
    for value in kwargs:
        if isinstance(value, str):
            validate_field_length(value, 5000)


async def add_draft(
    loaders: Dataloaders,
    group_name: str,
    user_email: str,
    draft_info: FindingDraftToAdd,
    source: Source,
) -> Finding:
    await findings_utils.is_valid_finding_title(loaders, draft_info.title)

    group_name = group_name.lower()
    finding_id = str(uuid.uuid4())
    vulnerabilities_file = await loaders.vulnerabilities_file.load("")
    criteria_vulnerability_id = draft_info.title.split(".")[0].strip()
    criteria_vulnerability = vulnerabilities_file[criteria_vulnerability_id]
    requirements: list[str] = (
        criteria_vulnerability["requirements"]
        if criteria_vulnerability
        else []
    )
    updated_severity = cvss_utils.adjust_privileges_required(
        draft_info.severity
    )
    draft = Finding(
        hacker_email=user_email,
        attack_vector_description=draft_info.attack_vector_description,
        description=draft_info.description,
        group_name=group_name,
        id=finding_id,
        min_time_to_remediate=draft_info.min_time_to_remediate,
        state=FindingState(
            modified_by=user_email,
            modified_date=datetime_utils.get_utc_now(),
            source=source,
            status=FindingStateStatus.CREATED,
        ),
        recommendation=draft_info.recommendation,
        requirements=draft_info.requirements,
        severity=updated_severity,
        severity_score=cvss_utils.get_severity_score_summary(updated_severity),
        title=draft_info.title,
        threat=draft_info.threat,
        unfulfilled_requirements=requirements,
    )
    await findings_model.add(finding=draft)
    return draft


@validate_no_duplicate_drafts_deco("title", "drafts", "findings")
@validate_fields_deco(
    [
        "attack_vector_description",
        "description",
        "recommendation",
        "requirements",
        "threat",
    ]
)
@validate_all_fields_length_deco(limit=5000)
def get_draft_info(
    user_email: str, title: str, **kwargs: Any
) -> FindingDraftToAdd:
    min_time_to_remediate = check_and_set_min_time_to_remediate(
        kwargs.get("min_time_to_remediate", None)
    )
    severity_info = Cvss31Severity(
        attack_complexity=Decimal(kwargs.get("attack_complexity", "0.0")),
        attack_vector=Decimal(kwargs.get("attack_vector", "0.0")),
        availability_impact=Decimal(kwargs.get("availability_impact", "0.0")),
        confidentiality_impact=Decimal(
            kwargs.get("confidentiality_impact", "0.0")
        ),
        exploitability=Decimal(kwargs.get("exploitability", "0.0")),
        integrity_impact=Decimal(kwargs.get("integrity_impact", "0.0")),
        privileges_required=Decimal(kwargs.get("privileges_required", "0.0")),
        remediation_level=Decimal(kwargs.get("remediation_level", "0.0")),
        report_confidence=Decimal(kwargs.get("report_confidence", "0.0")),
        severity_scope=Decimal(kwargs.get("severity_scope", "0.0")),
        user_interaction=Decimal(kwargs.get("user_interaction", "0.0")),
    )
    draft_info = FindingDraftToAdd(
        attack_vector_description=kwargs.get("attack_vector_description", "")
        or kwargs.get("attack_vector_desc", ""),
        description=kwargs.get("description", ""),
        hacker_email=user_email,
        min_time_to_remediate=min_time_to_remediate,
        recommendation=kwargs.get("recommendation", ""),
        requirements=kwargs.get("requirements", ""),
        severity=severity_info,
        threat=kwargs.get("threat", ""),
        title=title,
    )
    return draft_info
