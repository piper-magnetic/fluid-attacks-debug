from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_path.common import (
    FALSE_OPTIONS,
    TRUE_OPTIONS,
)
from lib_root.utilities.cloudformation import (
    get_attr_inside_attrs,
    get_attribute,
    iterate_resource,
    yield_statements_from_policy,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def _bucket_policy_has_secure_transport(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "PolicyDocument")
    if policies[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[policies[2]]["value_id"]
        ):
            _, effect_val, _ = get_attribute(graph, stmt, "Effect")
            transport, t_val, t_id = get_attr_inside_attrs(
                graph, stmt, ["Condition", "Bool", "aws:SecureTransport"]
            )
            if transport and (
                (effect_val == "Deny" and t_val in TRUE_OPTIONS)
                or (effect_val == "Allow" and t_val in FALSE_OPTIONS)
            ):
                yield t_id


def cfn_bucket_policy_has_secure_transport(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_BUCKET_POLICY_SEC_TRANSPORT

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::S3::BucketPolicy"):
                for report in _bucket_policy_has_secure_transport(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f281.bucket_policy_has_secure_transport",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
