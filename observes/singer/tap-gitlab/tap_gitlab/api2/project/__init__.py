from tap_gitlab.api2.core.ids import (
    ProjectId,
)

__all__ = [
    "ProjectId",
]
