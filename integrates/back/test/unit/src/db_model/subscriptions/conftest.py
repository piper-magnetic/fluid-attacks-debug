from collections.abc import (
    Callable,
)
from dynamodb.types import (
    PageInfo,
    QueryResponse,
)
import pytest
from typing import (
    Any,
)

pytestmark = [
    pytest.mark.asyncio,
]


MOCKED_DATA: dict[str, dict[str, Any]] = {
    "db_model.subscriptions.add.operations.batch_put_item": {
        '[["test_user_email@test.com", "ORGANIZATION", "WEEKLY", '
        '"test_report_subject", ["2022-10-27 20:07:57+00:00"]]]': None,
        '[["test_user_email2@test.com", "GROUP", "MONTHLY", '
        '"test_report_subject2", ["2022-10-27 20:07:57+00:00"]]]': None,
    },
    "db_model.subscriptions.get.operations.query": {
        '["DAILY"]': QueryResponse(
            items=(
                {
                    "subject": "unittesting",
                    "sk": "SUBS#ENTITY#group#SUBJECT#unittesting",
                    "pk_2": "SUBS#all",
                    "pk": "USER#integratesmanager@fluidattacks.com",
                    "state": {"modified_date": "2022-05-22T20:07:57+00:00"},
                    "email": "integratesmanager@fluidattacks.com",
                    "entity": "GROUP",
                    "sk_2": "SUBS#daily",
                    "frequency": "DAILY",
                },
            ),
            page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        ),
        '["HOURLY"]': QueryResponse(
            items=(
                {
                    "subject": "unittesting",
                    "sk": "SUBS#ENTITY#group#SUBJECT#unittesting",
                    "pk_2": "SUBS#all",
                    "pk": "USER#integratesmanager@gmail.com",
                    "state": {"modified_date": "2022-02-22T20:07:57+00:00"},
                    "email": "integratesmanager@gmail.com",
                    "entity": "GROUP",
                    "sk_2": "SUBS#hourly",
                    "frequency": "HOURLY",
                },
            ),
            page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        ),
        '["MONTHLY"]': QueryResponse(
            items=(
                {
                    "subject": "oneshottest",
                    "sk": "SUBS#ENTITY#group#SUBJECT#unittesting",
                    "pk_2": "SUBS#all",
                    "pk": "USER#integratesmanager@fluidattacks.com",
                    "state": {"modified_date": "2022-03-22T20:07:57+00:00"},
                    "email": "integratesmanager@fluidattacks.com",
                    "entity": "GROUP",
                    "sk_2": "SUBS#monthly",
                    "frequency": "MONTHLY",
                },
            ),
            page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        ),
        '["WEEKLY"]': QueryResponse(
            items=(
                {
                    "subject": "test_report_subject",
                    "sk": "SUBS#ENTITY#organization#SUBJECT#"
                    "test_report_subject",
                    "pk_2": "SUBS#all",
                    "pk": "USER#test_user_email@test.com",
                    "state": {"modified_date": "2022-10-27T20:07:57+00:00"},
                    "email": "test_user_email@test.com",
                    "entity": "ORGANIZATION",
                    "sk_2": "SUBS#weekly",
                    "frequency": "WEEKLY",
                },
            ),
            page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        ),
        '["integratesmanager@gmail.com", "GROUP", "unittesting"]': QueryResponse(  # noqa: E501 pylint: disable=line-too-long
            items=(
                {
                    "subject": "unittesting",
                    "sk": "SUBS#2022-05-22T20:07:57+00:00",
                    "pk": "USER#integratesmanager@fluidattacks.com#ENTITY#"
                    "group#SUBJECT#unittesting",
                    "state": {"modified_date": "2022-05-22T20:07:57+00:00"},
                    "email": "integratesmanager@fluidattacks.com",
                    "entity": "GROUP",
                    "frequency": "DAILY",
                },
            ),
            page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        ),
    },
    "db_model.subscriptions.remove.operations.batch_delete_item": {
        '["GROUP", "unittesting", "integratesmanager@gmail.com"]': None,
    },
    "db_model.subscriptions.remove.operations.delete_item": {
        '["GROUP", "unittesting", "integratesmanager@gmail.com"]': None,
    },
    "db_model.subscriptions.remove.operations.query": {
        '["GROUP", "unittesting", "integratesmanager@gmail.com"]': QueryResponse(  # noqa: E501 pylint: disable=line-too-long
            items=(
                {
                    "subject": "unittesting",
                    "sk": "SUBS#2022-05-22T20:07:57+00:00",
                    "pk": "USER#integratesmanager@fluidattacks.com#ENTITY#"
                    "group#SUBJECT#unittesting",
                    "state": {"modified_date": "2022-05-22T20:07:57+00:00"},
                    "email": "integratesmanager@fluidattacks.com",
                    "entity": "GROUP",
                    "frequency": "DAILY",
                },
            ),
            page_info=PageInfo(has_next_page=False, end_cursor="bnVsbA=="),
        ),
    },
}


@pytest.fixture
def mocked_data_for_module(
    *,
    resolve_mocked_data: Any,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mocked_data(
            mocked_data=MOCKED_DATA,
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module
