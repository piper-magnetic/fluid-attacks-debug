from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_path.common import (
    TRUE_OPTIONS,
)
from lib_root.utilities.kubernetes import (
    get_attr_inside_attrs,
    get_attribute,
    get_containers_capabilities,
    get_pod_spec,
    iterate_containers,
    iterate_security_context,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils import (
    graph as g,
)


def _container_without_securitycontext(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    security_context = get_attribute(graph, nid, "securityContext")
    if not security_context[0]:
        yield nid


def _check_drop_capability(graph: Graph, nid: NId) -> Iterator[NId]:
    cap_drop = get_containers_capabilities(graph, nid, "drop")
    if cap_drop and "all" not in [cap.lower() for cap in cap_drop]:
        report = get_attr_inside_attrs(graph, nid, ["capabilities", "drop"])
        yield report[2]


def _check_privileged_used(graph: Graph, nid: NId) -> Iterator[NId]:
    privileged, p_val, p_id = get_attribute(graph, nid, "privileged")
    if privileged and p_val in TRUE_OPTIONS:
        yield p_id


def _check_run_as_user(graph: Graph, nid: NId) -> Iterator[NId]:
    run_as_user, rau_val, rau_id = get_attribute(graph, nid, "runAsUser")
    if run_as_user and rau_val == "0":
        yield rau_id


def _check_add_capability(graph: Graph, nid: NId) -> Iterator[NId]:
    cap_add = get_containers_capabilities(graph, nid, "add")
    if cap_add and cap_add[0].lower() not in {
        "net_bind_service",
        "null",
        "nil",
        "undefined",
    }:
        report = get_attr_inside_attrs(graph, nid, ["capabilities", "add"])
        yield report[2]


def get_seccomp_vuln_line(
    graph: Graph, container_ctx: NId, pod_has_safe_config: bool
) -> NId | None:
    sec_ctx = graph.nodes[container_ctx]["value_id"]
    container_seccomp_profile = get_attribute(graph, sec_ctx, "seccompProfile")
    if container_seccomp_profile[0]:
        cont_seccomp_attrs = graph.nodes[container_seccomp_profile[2]][
            "value_id"
        ]
        container_type = get_attribute(graph, cont_seccomp_attrs, "type")
        if container_type[0]:
            if container_type[1].lower() == "unconfined":
                return container_type[2]
        elif not pod_has_safe_config:
            return container_seccomp_profile[2]
    elif not pod_has_safe_config:
        return container_ctx
    return None


def _k8s_check_container_seccomp(
    graph: Graph, container_props: NId, pod_has_safe_config: bool
) -> NId | None:
    sec_ctx = get_attribute(graph, container_props, "securityContext")
    if sec_ctx[0]:
        if vuln := get_seccomp_vuln_line(
            graph, sec_ctx[2], pod_has_safe_config
        ):
            return vuln
    elif not pod_has_safe_config:
        return container_props
    return None


def _check_seccomp_profile(graph: Graph) -> Iterator[NId]:
    pod_has_safe_config: bool = False
    for nid in g.matching_nodes(graph, label_type="Object"):
        if (
            (spec := get_pod_spec(graph, nid))
            and (
                p_type := get_attr_inside_attrs(
                    graph, spec, ["securityContext", "seccompProfile", "type"]
                )
            )
            and p_type[0]
        ):
            if p_type[1].lower() == "unconfined":
                yield p_type[2]
            elif p_type[1].lower() in ["runtimedefault", "localhost"]:
                pod_has_safe_config = True
    for container in iterate_containers(graph):
        if vuln := _k8s_check_container_seccomp(
            graph, container, pod_has_safe_config
        ):
            yield vuln


def _root_filesystem_read_only(graph: Graph, nid: NId) -> NId | None:
    security_context = get_attribute(graph, nid, "securityContext")
    if security_context[0]:
        sec_ctx_attrs = graph.nodes[security_context[2]]["value_id"]
        read_perm = get_attribute(
            graph, sec_ctx_attrs, "readOnlyRootFilesystem"
        )
        if read_perm[0]:
            if read_perm[1].lower() != "true":
                return read_perm[2]
        else:
            return security_context[2]
    else:
        return nid
    return None


def _allow_privilege_escalation_enabled(graph: Graph, nid: NId) -> NId | None:
    security_context = get_attribute(graph, nid, "securityContext")
    if security_context[0]:
        sec_ctx_attrs = graph.nodes[security_context[2]]["value_id"]
        read_perm = get_attribute(
            graph, sec_ctx_attrs, "allowPrivilegeEscalation"
        )
        if read_perm[0]:
            if read_perm[1].lower() != "false":
                return read_perm[2]
        else:
            return security_context[2]
    else:
        return nid
    return None


def get_container_root_vuln_line(
    graph: Graph, container_ctx: NId, pod_has_safe_config: bool
) -> NId | None:
    sec_ctx = graph.nodes[container_ctx]["value_id"]
    root = get_attribute(graph, sec_ctx, "runAsNonRoot")
    if root[0]:
        if root[1].lower() != "true":
            return root[2]
    elif not pod_has_safe_config:
        return container_ctx
    return None


def _k8s_check_container_root(
    graph: Graph, container_props: NId, pod_has_safe_config: bool
) -> NId | None:
    sec_ctx = get_attribute(graph, container_props, "securityContext")
    if sec_ctx[0]:
        if vuln := get_container_root_vuln_line(
            graph, sec_ctx[2], pod_has_safe_config
        ):
            return vuln
    elif not pod_has_safe_config:
        return container_props
    return None


def _root_container(graph: Graph) -> Iterator[NId]:
    pod_has_safe_config: bool = False
    for nid in g.matching_nodes(graph, label_type="Object"):
        if (
            (spec := get_pod_spec(graph, nid))
            and (
                perm := get_attr_inside_attrs(
                    graph, spec, ["securityContext", "runAsNonRoot"]
                )
            )
            and perm[0]
        ):
            if perm[1].lower() != "true":
                yield perm[2]
            elif perm[1].lower() == "true":
                pod_has_safe_config = True

    for container in iterate_containers(graph):
        if vuln := _k8s_check_container_root(
            graph, container, pod_has_safe_config
        ):
            yield vuln


def k8s_root_container(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_ROOT_CONTAINER

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for report in _root_container(graph):
                yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_root_container",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_allow_privilege_escalation_enabled(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_PRIVILEGE_ESCALATION_ENABLED

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_containers(graph):
                if report := _allow_privilege_escalation_enabled(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_allow_privilege_escalation_enabled",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_root_filesystem_read_only(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_ROOT_FILESYSTEM_READ_ONLY

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_containers(graph):
                if report := _root_filesystem_read_only(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_root_filesystem_read_only",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_check_seccomp_profile(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_CHECK_SECCOMP_PROFILE

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for report in _check_seccomp_profile(graph):
                yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_check_seccomp_profile",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_check_add_capability(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_CHECK_ADD_CAPABILITY

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_security_context(graph, True):
                for report in _check_add_capability(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_check_add_capability",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_check_run_as_user(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_CHECK_RUN_AS_USER

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_security_context(graph, False):
                for report in _check_run_as_user(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_check_run_as_user",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_check_privileged_used(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_CHECK_PRIVILEGED_USED

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_security_context(graph, True):
                for report in _check_privileged_used(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_check_privileged_used",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_check_drop_capability(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_CHECK_DROP_CAPABILITY

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_security_context(graph, True):
                for report in _check_drop_capability(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_check_drop_capability",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def k8s_container_without_securitycontext(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.K8S_CONTAINER_WITHOUT_SECURITYCONTEXT

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_containers(graph):
                for report in _container_without_securitycontext(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f267.k8s_container_without_securitycontext",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
