from lib_root.f008.c_sharp import (
    cs_unsafe_addheader_write,
    cs_unsafe_status_write,
)
from lib_root.f008.java import (
    unsafe_xss_content as java_unsafe_xss_content,
)
from lib_root.f008.javascript import (
    unsafe_xss_content as js_unsafe_xss_content,
)
from lib_root.f008.typescript import (
    unsafe_xss_content as ts_unsafe_xss_content,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F008
QUERIES: graph_model.Queries = (
    (FINDING, cs_unsafe_addheader_write),
    (FINDING, cs_unsafe_status_write),
    (FINDING, java_unsafe_xss_content),
    (FINDING, js_unsafe_xss_content),
    (FINDING, ts_unsafe_xss_content),
)
