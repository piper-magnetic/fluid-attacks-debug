from __future__ import (
    annotations,
)

from ._utils import (
    GenericStream,
)
from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
    timedelta,
)
import dateutil.parser
from dateutil.parser import (
    isoparse,
)
from fa_purity import (
    Cmd,
    FrozenList,
    JsonObj,
    Maybe,
    Stream,
)
from fa_purity.json.value.transform import (
    Unfolder,
)
from fa_purity.pure_iter.factory import (
    from_flist,
)
from tap_gitlab.api2.core.ids import (
    ProjectId,
)
from tap_gitlab.api2.http_json_client import (
    HttpJsonClient,
    Page,
)
from tap_gitlab.api2.merge_requests import (
    MrFilter,
    MrsClient,
    OrderBy,
    Scope,
    Sort,
    State,
)
from tap_gitlab.intervals.interval import (
    MIN,
    OpenLeftInterval,
)
from tap_gitlab.intervals.progress import (
    FragmentedProgressInterval,
    ProgressInterval,
)
from tap_gitlab.streams._utils import (
    ItemOrState,
    SegmentsStream,
)
from typing import (
    FrozenSet,
)


class InvalidPage(Exception):
    pass


@dataclass(frozen=True)
class MrsPage:
    data: FrozenList[JsonObj]

    @property
    def min_updated_at(self) -> datetime:
        return isoparse(
            Unfolder(self.data[-1]["updated_at"]).to_primitive(str).unwrap()
        )

    @property
    def max_updated_at(self) -> datetime:
        return isoparse(
            Unfolder(self.data[0]["updated_at"]).to_primitive(str).unwrap()
        )


@dataclass(frozen=True)
class MrStreams:
    _client: HttpJsonClient
    proj: ProjectId
    _per_page: int
    _scope: Maybe[Scope]
    _state: Maybe[State]

    def updated_before(
        self,
        updated_before: datetime,
    ) -> Stream[JsonObj]:
        _filter = MrFilter(
            Maybe.empty(),
            Maybe.from_value(updated_before),
            Maybe.empty(),
            Maybe.empty(),
            Maybe.empty(),
            Maybe.empty(),
        )
        client = MrsClient(self._client, self.proj, Maybe.from_value(_filter))
        return GenericStream(0, self._per_page).generic_page_stream(
            lambda p: client.mrs_page(p), GenericStream.is_empty
        )

    def updated_between(
        self, after: datetime, before: datetime
    ) -> Stream[JsonObj]:
        _filter = MrFilter(
            Maybe.from_value(after),
            Maybe.from_value(before),
            Maybe.empty(),
            Maybe.empty(),
            Maybe.empty(),
            Maybe.empty(),
        )
        client = MrsClient(self._client, self.proj, Maybe.from_value(_filter))
        return GenericStream(0, self._per_page).generic_page_stream(
            lambda p: client.mrs_page(p), GenericStream.is_empty
        )

    def _updated_between_client(
        self, after: datetime | None, before: datetime
    ) -> MrsClient:
        _filter = MrFilter(
            Maybe.from_optional(after),
            Maybe.from_value(before),
            self._scope,
            self._state,
            Maybe.from_value(OrderBy.updated_at),
            Maybe.from_value(Sort.descendant),
        )
        return MrsClient(self._client, self.proj, Maybe.from_value(_filter))

    def stream_fragmented(
        self,
        fragments: FragmentedProgressInterval[datetime],
        page_limit: int,
        per_page: int,
    ) -> Cmd[
        Stream[
            ItemOrState[
                MrsPage,
                frozenset[ProgressInterval[OpenLeftInterval[datetime]]],
            ]
        ]
    ]:
        def get_segment(
            interval: ProgressInterval[OpenLeftInterval[datetime]], page: Page
        ) -> Cmd[MrsPage]:
            client = self._updated_between_client(
                None
                if isinstance(interval.interval.lower, MIN)
                else interval.interval.lower,
                interval.interval.upper,
            )
            return client.mrs_page(page).map(MrsPage)

        def _new_state(
            prev: FrozenSet[ProgressInterval[OpenLeftInterval[datetime]]],
            seg: ProgressInterval[OpenLeftInterval[datetime]],
        ) -> FrozenSet[ProgressInterval[OpenLeftInterval[datetime]]]:
            return frozenset((prev - {seg}).union({seg}))

        def _interrupted_state(
            prev: FrozenSet[ProgressInterval[OpenLeftInterval[datetime]]],
            seg: ProgressInterval[OpenLeftInterval[datetime]],
            item: MrsPage,
        ) -> FrozenSet[ProgressInterval[OpenLeftInterval[datetime]]]:
            removed_state = frozenset((prev - {seg}).union({seg}))
            lower = item.min_updated_at
            new_seg = ProgressInterval(
                OpenLeftInterval.new(
                    seg.interval.greater.inner, lower, seg.interval.upper
                ).unwrap(),
                True,
            )
            new_seg_2 = ProgressInterval(
                OpenLeftInterval.new(
                    seg.interval.greater.inner,
                    seg.interval.lower,
                    lower - timedelta(microseconds=1),
                ).unwrap(),
                False,
            )
            return removed_state.union({new_seg, new_seg_2})

        segments = from_flist(fragments.progress_intervals).filter(
            lambda i: not i.completed
        )

        new_stream: Cmd[
            SegmentsStream[
                ProgressInterval[OpenLeftInterval[datetime]],
                FrozenSet[ProgressInterval[OpenLeftInterval[datetime]]],
                MrsPage,
            ]
        ] = SegmentsStream.new(
            segments,
            frozenset(segments),
            page_limit,
            per_page,
            get_segment,
            lambda mp: GenericStream.is_empty(mp.data).map(lambda _: mp),
            _new_state,
            _interrupted_state,
        )
        return new_stream.map(lambda s: s.stream())
