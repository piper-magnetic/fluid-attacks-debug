from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.json import (
    is_parent,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils.graph import (
    matching_nodes,
)


def cfn_http_insecure_channel(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_HTTP_GET_INSECURE_CHANNELS
    parent_paths = [
        [
            "httpGet",
            "readinessProbe",
        ],
        [
            "httpGet",
            "livenessProbe",
        ],
        [
            "httpGet",
            "startupProbe",
        ],
    ]

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in matching_nodes(graph, label_type="Pair"):
                key_id = graph.nodes[nid]["key_id"]
                value_id = graph.nodes[nid]["value_id"]

                if (
                    graph.nodes[key_id]["value"] == "scheme"
                    and any(
                        is_parent(graph, nid, path) for path in parent_paths
                    )
                    and graph.nodes[value_id].get("value") in {"http", "HTTP"}
                ):
                    yield shard, nid

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f022.http_insecure_channel",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_server_port_insecure_channel(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_HTTP_GET_INSECURE_CHANNELS

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in matching_nodes(graph, label_type="Pair"):
                key_id = graph.nodes[nid]["key_id"]
                value_id = graph.nodes[nid]["value_id"]

                if (
                    graph.nodes[key_id]["value"] == "protocol"
                    and graph.nodes[value_id].get("value") in {"http", "HTTP"}
                    and is_parent(graph, nid, ["port", "servers"])
                ):
                    yield shard, nid

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f022.http_insecure_channel",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
