from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.cloudformation import (
    get_attribute,
    iterate_resource,
    yield_statements_from_policy,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils.graph import (
    adj_ast,
)


def _iam_is_policy_applying_to_users(graph: Graph, nid: NId) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    user, _, _ = get_attribute(graph, val_id, "Users")
    if user:
        yield prop_id


def _iam_allow_not_actions_trust_policy(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    document = get_attribute(graph, val_id, "AssumeRolePolicyDocument")
    if document[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[document[2]]["value_id"]
        ):
            effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
            not_action, _, na_id = get_attribute(graph, stmt, "NotAction")
            if effect and effect_val == "Allow" and not_action:
                yield na_id


def _iam_allow_not_principal_trust_policy(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    document = get_attribute(graph, val_id, "AssumeRolePolicyDocument")
    if document[0]:
        for stmt in yield_statements_from_policy(
            graph, graph.nodes[document[2]]["value_id"]
        ):
            effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
            not_principal, _, np_id = get_attribute(
                graph, stmt, "NotPrincipal"
            )
            if effect and effect_val == "Allow" and not_principal:
                yield np_id


def _iam_allow_not_action_perms_policies(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "Policies")
    if policies[0]:
        for policy in adj_ast(graph, graph.nodes[policies[2]]["value_id"]):
            _, _, pd_id = get_attribute(graph, policy, "PolicyDocument")
            for stmt in yield_statements_from_policy(
                graph, graph.nodes[pd_id]["value_id"]
            ):
                effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
                not_action, _, na_id = get_attribute(graph, stmt, "NotAction")
                if effect and effect_val == "Allow" and not_action:
                    yield na_id


def _iam_allow_not_resource_perms_policies(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies = get_attribute(graph, val_id, "Policies")
    if policies[0]:
        for policy in adj_ast(graph, graph.nodes[policies[2]]["value_id"]):
            _, _, pd_id = get_attribute(graph, policy, "PolicyDocument")
            for stmt in yield_statements_from_policy(
                graph, graph.nodes[pd_id]["value_id"]
            ):
                effect, effect_val, _ = get_attribute(graph, stmt, "Effect")
                not_resource, _, nr_id = get_attribute(
                    graph, stmt, "NotResource"
                )
                if effect and effect_val == "Allow" and not_resource:
                    yield nr_id


def cfn_iam_allow_not_resource_perms_policies(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_NOT_RESOURCE

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::IAM::Role"):
                for report in _iam_allow_not_resource_perms_policies(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f165.iam_allow_not_resourse_permissions_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_allow_not_action_perms_policies(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_NOT_ACTION

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::IAM::Role"):
                for report in _iam_allow_not_action_perms_policies(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f165.iam_allow_not_action_permissions_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_allow_not_principal_trust_policy(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_TRUST_POLICY_NOT_PRINCIPAL

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::IAM::Role"):
                for report in _iam_allow_not_principal_trust_policy(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f165.iam_allow_not_principal_trust_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_allow_not_actions_trust_policy(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_TRUST_POLICY_WILDCARD_ACTION

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::IAM::Role"):
                for report in _iam_allow_not_actions_trust_policy(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f165.iam_allow_not_action_trust_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_is_policy_applying_to_users(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_PERMISSIONS_POLICY_APLLY_USERS

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in chain(
                iterate_resource(graph, "AWS::IAM::ManagedPolicy"),
                iterate_resource(graph, "AWS::IAM::Policy"),
            ):
                for report in _iam_is_policy_applying_to_users(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f165.iam_policies_applying_to_users",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
