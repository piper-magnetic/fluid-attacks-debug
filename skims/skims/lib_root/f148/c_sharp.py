from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils import (
    graph as g,
)


def cs_insecure_channel(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.CS_INSECURE_CHANNEL

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in g.matching_nodes(graph, label_type="ObjectCreation"):
                if graph.nodes[n_id].get("name") == "FtpClient":
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f148.cs_insecure_channel",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
