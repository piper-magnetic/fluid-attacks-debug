from ._utils import (
    GenericStream,
)
from dataclasses import (
    dataclass,
)
from fa_purity import (
    Stream,
)
from tap_gitlab.api2.jobs import (
    JobId,
    JobObj,
    JobsClient,
)


@dataclass(frozen=True)
class JobStreams:
    _client: JobsClient

    def job_stream(self, start_page: int, per_page: int) -> Stream[JobObj]:
        return GenericStream(start_page, per_page).generic_page_stream(
            lambda p: self._client.jobs_page(p), GenericStream.is_empty
        )

    def job_id_stream(self, start_page: int, per_page: int) -> Stream[JobId]:
        return GenericStream(start_page, per_page).generic_page_stream(
            lambda p: self._client.jobs_ids_page(p), GenericStream.is_empty
        )
