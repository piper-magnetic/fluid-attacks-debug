from ._core import (
    JobStream,
    MrStream,
    SupportedStreams,
)
from ._decoder import (
    StreamDecoder,
)
from ._encoder import (
    StreamEncoder,
)
from ._jobs import (
    JobStreams,
)
from ._pipelines import (
    PipelineStreams,
)
from tap_gitlab.api.projects.ids import (
    ProjectId,
)
from tap_gitlab.api.projects.jobs.page import (
    JobsPage,
    Scope as JobScope,
)
from tap_gitlab.api.projects.merge_requests.data_page import (
    MrsPage,
    Scope as MrScope,
    State as MrState,
)
from typing import (
    Tuple,
    Union,
)

ApiPage = Union[MrsPage, JobsPage]


def default_mr_streams(proj: ProjectId) -> Tuple[MrStream, ...]:
    return (
        MrStream(proj, MrScope.all, MrState.closed),
        MrStream(proj, MrScope.all, MrState.merged),
    )


def default_job_stream(proj: ProjectId) -> JobStream:
    scopes = (
        JobScope.failed,
        JobScope.success,
        JobScope.canceled,
        JobScope.skipped,
        JobScope.manual,
    )
    return JobStream(proj, scopes)


__all__ = [
    "JobStream",
    "JobStreams",
    "MrStream",
    "PipelineStreams",
    "StreamDecoder",
    "StreamEncoder",
    "SupportedStreams",
]
