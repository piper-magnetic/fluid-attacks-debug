{
  nixpkgs,
  python_version,
  src,
}: let
  metadata = let
    _metadata = (builtins.fromTOML (builtins.readFile "${src}/pyproject.toml")).project;
    file_str = builtins.readFile "${src}/${_metadata.name}/__init__.py";
    match = builtins.match ".*__version__ *= *\"(.+?)\"\n.*" file_str;
    version = builtins.elemAt match 0;
  in
    _metadata // {inherit version;};

  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };

  deps = let
    python_pkgs =
      nixpkgs."${python_version}Packages"
      // {
        arch-lint = nixpkgs.arch-lint."${python_version}".pkg;
        fa-purity = nixpkgs.fa-purity."${python_version}".pkg;
        fa-singer-io = nixpkgs.fa-singer-io."${python_version}".pkg;
        utils-logger = nixpkgs.utils-logger."${python_version}".pkg;
      };
  in
    import ./deps lib python_pkgs;

  self_pkgs = import ./pkg {
    inherit lib src metadata;
    inherit (deps) python_pkgs;
  };
  check = import ./check.nix self_pkgs.pkg;
in
  self_pkgs // {inherit check;}
