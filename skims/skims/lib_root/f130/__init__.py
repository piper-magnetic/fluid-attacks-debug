from lib_root.f130.java import (
    java_cookie_set_secure,
)
from lib_root.f130.kotlin import (
    kt_cookie_set_secure,
)
from lib_root.f130.python import (
    python_secure_cookie,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F130
QUERIES: graph_model.Queries = (
    (FINDING, java_cookie_set_secure),
    (FINDING, kt_cookie_set_secure),
    (FINDING, python_secure_cookie),
)
