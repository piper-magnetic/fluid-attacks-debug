from model.graph_model import (
    NId,
)
from syntax_graph.syntax_nodes.infix_expression import (
    build_infix_expression_node,
)
from syntax_graph.types import (
    SyntaxGraphArgs,
)
from utils.graph import (
    adj_ast,
)


def reader(args: SyntaxGraphArgs) -> NId:
    graph = args.ast_graph
    n_attrs = graph.nodes[args.n_id]
    right_id = n_attrs["label_field_right"]
    left_id = n_attrs["label_field_left"]
    c_ids = adj_ast(graph, args.n_id)

    return build_infix_expression_node(
        args, left_id, right_id, (c_id for c_id in c_ids)
    )
