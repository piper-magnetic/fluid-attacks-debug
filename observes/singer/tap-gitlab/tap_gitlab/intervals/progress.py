from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    FrozenList,
    Result,
    ResultE,
)
from more_itertools import (
    split_when,
)
from tap_gitlab.intervals.fragmented import (
    FIntervalFactory,
    FragmentedInterval,
)
from tap_gitlab.intervals.interval import (
    ClosedInterval,
    OpenInterval,
    OpenLeftInterval,
    OpenRightInterval,
)
from typing import (
    Callable,
    Generic,
    Tuple,
    TypeVar,
)

_DataType = TypeVar("_DataType")
_I = TypeVar("_I")
_ProgressIntvlType = TypeVar(
    "_ProgressIntvlType",
    ClosedInterval,
    OpenInterval,
    OpenLeftInterval,
    OpenRightInterval,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class ProgressInterval(Generic[_I]):
    _interval: _I
    # _I should be restricted only to ClosedInterval, OpenInterval, OpenLeftInterval, OpenRightInterval,
    completed: bool

    @property
    def interval(self) -> _I:
        return self._interval


_State = TypeVar("_State")


@dataclass(frozen=True)
class ProcessStatus(
    Generic[_DataType, _State],
):
    p_intervals: FrozenList[ProgressInterval[OpenLeftInterval[_DataType]]]
    incomplete_is_present: bool
    function_state: _State


@dataclass(frozen=True)
class FragmentedProgressInterval(Generic[_DataType]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    f_interval: FragmentedInterval[_DataType]
    completeness: FrozenList[bool]

    @staticmethod
    def new(
        f_interval: FragmentedInterval[_DataType],
        completeness: FrozenList[bool],
    ) -> ResultE[FragmentedProgressInterval[_DataType]]:
        if len(f_interval.endpoints) - 1 == len(completeness):
            obj = FragmentedProgressInterval(
                _Private(), f_interval, completeness
            )
            return Result.success(obj)
        err = CannotBuild("FragmentedProgressInterval")
        return Result.failure(Exception(err))

    @property
    def progress_intervals(
        self,
    ) -> FrozenList[ProgressInterval[OpenLeftInterval[_DataType]]]:
        intervals = zip(self.f_interval.intervals, self.completeness)
        return tuple(
            ProgressInterval(item, completed) for item, completed in intervals
        )

    def process_until_incomplete(
        self,
        function: Callable[
            [_State, ProgressInterval[OpenLeftInterval[_DataType]]],
            Tuple[
                _State,
                FrozenList[ProgressInterval[OpenLeftInterval[_DataType]]],
            ],
        ],
        init_state: _State,
    ) -> FrozenList[ProgressInterval[OpenLeftInterval[_DataType]]]:
        def _process(
            prev: ProcessStatus[_DataType, _State],
            interval: ProgressInterval[OpenLeftInterval[_DataType]],
        ) -> ProcessStatus[_DataType, _State]:
            if prev.incomplete_is_present:
                return ProcessStatus(
                    (interval,) + prev.p_intervals,
                    prev.incomplete_is_present,
                    prev.function_state,
                )
            state, results = function(prev.function_state, interval)
            incomplete = tuple(
                filter(lambda p_invl: p_invl.completed is False, results)
            )
            return ProcessStatus(
                results + prev.p_intervals, bool(incomplete), state
            )

        status: ProcessStatus[_DataType, _State] = ProcessStatus(
            tuple(), False, init_state
        )
        for interval in reversed(self.progress_intervals):
            status = _process(status, interval)
        return status.p_intervals


class CannotBuild(Exception):
    pass


@dataclass(frozen=True)
class FProgressFactory(
    Generic[_DataType],
):
    factory: FIntervalFactory[_DataType]

    def from_n_progress(
        self,
        progress: FrozenList[ProgressInterval[OpenLeftInterval[_DataType]]],
    ) -> FragmentedProgressInterval[_DataType]:
        grouped = split_when(
            progress, lambda p1, p2: p1.completed != p2.completed
        )
        compressed = tuple(
            ProgressInterval(
                self.factory.factory.new_lopen(
                    group[0].interval.lower, group[-1].interval.upper
                ),
                group[0].completed,
            )
            for group in grouped
        )
        intervals: FrozenList[OpenLeftInterval[_DataType]] = tuple(
            item.interval for item in compressed
        )
        f_interval = self.factory.from_intervals(intervals)
        completeness = tuple(item.completed for item in compressed)
        return FragmentedProgressInterval.new(
            f_interval, completeness
        ).unwrap()

    def append(
        self,
        fp_interval: FragmentedProgressInterval[_DataType],
        point: _DataType,
    ) -> FragmentedProgressInterval[_DataType]:
        p_intervals = fp_interval.progress_intervals
        final: OpenLeftInterval[_DataType] = p_intervals[-1].interval
        new_section = ProgressInterval(
            self.factory.factory.new_lopen(final.upper, point), False
        )
        return self.from_n_progress(p_intervals + (new_section,))
