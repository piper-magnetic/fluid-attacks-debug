from collections.abc import (
    Iterator,
)
from lib_root.utilities.docker import (
    get_attribute,
    iterate_services,
    validate_path,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def _compose_read_only(graph: Graph, nid: NId) -> Iterator[NId]:
    props = graph.nodes[nid]["value_id"]
    read_only = get_attribute(graph, props, "read_only")
    if read_only[0]:
        if read_only[1].lower() != "true":
            yield read_only[2]
    else:
        yield nid


def docker_compose_read_only(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.DOCKER_COMPOSE_READ_ONLY

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.YAML):
            if shard.syntax_graph is None or not validate_path(shard.path):
                continue
            graph = shard.syntax_graph

            for nid in iterate_services(graph):
                for report in _compose_read_only(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f418.docker_compose_read_only",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
