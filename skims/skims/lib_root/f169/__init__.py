from lib_root.f169.c_sharp import (
    cs_plain_text_keys,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F169
QUERIES: graph_model.Queries = ((FINDING, cs_plain_text_keys),)
