{
  makesSrc = builtins.fetchGit {
    url = "https://github.com/fluidattacks/makes";
    ref = "refs/heads/main";
    rev = "4a2f1a9a54d860d01b2b79092cbb420249c7e9c6";
  };
}
