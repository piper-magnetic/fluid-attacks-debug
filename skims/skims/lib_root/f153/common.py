from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.javascript import (
    get_default_alias,
)
from model.core_model import (
    MethodsEnum,
)
from model.graph_model import (
    Graph,
    NId,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def get_dang_instances_names_method(
    graph: Graph, dang_classes: set[str]
) -> set[str]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        nodes = graph.nodes
        return bool(
            node.get("label_type") == "VariableDeclaration"
            and (val_n_id := node.get("value_id"))
            and (
                (
                    (nodes[val_n_id].get("label_type") == "ObjectCreation")
                    and (nodes[val_n_id].get("name") in dang_classes)
                )
                or (
                    (nodes[val_n_id].get("label_type") == "MethodInvocation")
                    and (expr := nodes[val_n_id].get("expression"))
                    and (expr.split(".")[0] in dang_classes)
                )
            )
        )

    dang_names: set[str] = set()
    for n_id in g.filter_nodes(graph, graph.nodes, predicate_matcher):
        dang_names.add(graph.nodes[n_id].get("variable"))
    return dang_names


def get_danger_invocations_method(graph: Graph) -> set[NId]:
    dang_methods: set[str] = {"setRequestHeader", "append", "set"}
    dang_nodes: set[NId] = set()
    dang_objects = get_dang_instances_names_method(
        graph, {"XMLHttpRequest", "Headers"}
    )
    dang_imported_classes = {"superagent"}

    for dang_class in dang_imported_classes:
        if imported_alias := get_default_alias(graph, dang_class):
            dang_objects.add(imported_alias)

    for dang_method in dang_methods:
        for dang_name in dang_objects:
            dang_nodes.update(
                g.matching_nodes(
                    graph,
                    label_type="MethodInvocation",
                    expression=f"{dang_name}.{dang_method}",
                )
            )
    return dang_nodes


def get_accept_header_vulns_method(graph: Graph) -> Iterator[NId]:
    for n_id in get_danger_invocations_method(graph):
        if args_n_id := graph.nodes[n_id].get("arguments_id"):
            yield args_n_id


def get_dang_instances_names_object(graph: Graph) -> set[str]:
    dang_libraries: set[str] = {"axios", "ky"}
    dang_aliases: set[str] = {"fetch", "$"}

    for lib in dang_libraries:
        if dang_alias := get_default_alias(graph, lib):
            dang_aliases.add(dang_alias)
    return dang_aliases


def get_danger_invocations_object(graph: Graph) -> tuple[NId, ...]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        return bool(
            node.get("label_type") == "MethodInvocation"
            and (exp := node.get("expression"))
            and (exp.split(".")[0] in dang_instances)
        )

    dang_instances = get_dang_instances_names_object(graph)
    return g.filter_nodes(graph, graph.nodes, predicate_matcher)


def get_accept_header_vulns_object(graph: Graph) -> Iterator[NId]:
    nodes = graph.nodes
    dang_types = {"Object", "SymbolLookup"}
    for n_id in get_danger_invocations_object(graph):
        if (args_n_id := nodes[n_id].get("arguments_id")) and (
            args := g.adj_ast(graph, args_n_id)
        ):
            for susp_node in filter(
                lambda x: nodes[x].get("label_type") in dang_types, args
            ):
                yield susp_node


def get_accept_header_vulns(
    graph: Graph, method: MethodsEnum
) -> Iterator[NId]:

    for n_id in chain(
        get_accept_header_vulns_object(graph),
        get_accept_header_vulns_method(graph),
    ):
        if get_node_evaluation_results(
            method, graph, n_id, {"accept", "*/*"}, False
        ):
            yield n_id


def get_suspicious_nodes(graph: Graph) -> tuple[NId, ...]:
    def predicate_matcher(node: dict[str, str]) -> bool:

        return bool(
            node.get("label_type") == "ElementAccess"
            and (exp_n_id := node.get("expression_id"))
            and (nodes[exp_n_id].get("label_type") == "MemberAccess")
            and (members := set(nodes[exp_n_id].get("member").split(".")))
            and members.issubset(dang_members)
            and (arg_n_id := node.get("arguments_id"))
            and (nodes[arg_n_id].get("value")[1:-1] == "Accept")
        )

    nodes = graph.nodes
    dang_members = {
        "defaults",
        "headers",
        "common",
        "get",
        "post",
        "put",
        "patch",
        "delete",
    }

    if library_alias := get_default_alias(graph, "axios"):
        dang_members.add(library_alias)
        if library_instances := get_dang_instances_names_method(
            graph, {library_alias}
        ):
            for dang_instance in library_instances:
                dang_members.add(dang_instance)
    return g.filter_nodes(graph, nodes, predicate_matcher)


def get_accept_header_vulns_default(
    graph: Graph, method: MethodsEnum
) -> Iterator[NId]:
    nodes = graph.nodes
    for n_id in get_suspicious_nodes(graph):
        for p_id in g.pred_ast(graph, n_id):
            if (nodes[p_id].get("label_type") == "Assignment") and (
                get_node_evaluation_results(
                    method, graph, p_id, {"*/*"}, False
                )
            ):
                yield n_id
