# pylint: disable=unused-argument

from click.testing import (
    CliRunner,
    Result,
)
from collections.abc import (
    Callable,
    Iterator,
)
import contextlib
import os
import pytest
from toolbox.cli import (
    melts as cli,
)
from toolbox.logger import (
    LOGGER,
)

# Constants
SUBS: str = "continuoustest"


@contextlib.contextmanager
def _relocate(path: str = "../") -> Iterator[str]:
    """Change temporarily the working directory."""
    # The toolbox need to be run in the root of the repository
    current_dir: str = os.getcwd()
    try:
        os.chdir(path)
        LOGGER.info("yield, %s", os.getcwd())
        yield os.getcwd()
    finally:
        # Return to where we were
        LOGGER.info("finally, %s", current_dir)
        os.chdir(current_dir)


@pytest.fixture(scope="function")
def relocate(request: object) -> Iterator[str]:
    """Change temporarily the working directory."""
    with _relocate() as new_path:
        yield new_path


@pytest.fixture(scope="function")
def relocate_to_cloned_repo(request: object) -> Iterator[str]:
    """Change temporarily the working directory."""
    with _relocate(path=f"../groups/{SUBS}/fusion/services") as new_path:
        yield new_path


@pytest.fixture(scope="session", autouse=True)
def cli_runner(  # pylint: disable=unused-argument
    request: object,
) -> Callable[..., Result]:
    def executor(command: list[str]) -> Result:
        runner = CliRunner()
        return runner.invoke(cli, command)

    return executor
