from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def kotlin_salting_is_harcoded(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.KOTLIN_SALT_IS_HARDCODED
    danger_methods = {"PBEParameterSpec", "PBEKeySpec"}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.KOTLIN):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in g.matching_nodes(graph, label_type="MethodInvocation"):
                expression = graph.nodes[n_id]["expression"]
                if (
                    expression in danger_methods
                    and (al_id := g.match_ast_d(graph, n_id, "ArgumentList"))
                    and (args_nids := g.adj_ast(graph, al_id))
                    and len(args_nids) >= 1
                    and get_node_evaluation_results(
                        method,
                        graph,
                        al_id,
                        {"usesMethod"},
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f338.salt_is_hardcoded",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
