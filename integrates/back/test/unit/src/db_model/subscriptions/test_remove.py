from back.test.unit.src.utils import (  # pylint: disable=import-error
    get_module_at_test,
)
from collections.abc import (
    Callable,
)
from db_model.subscriptions.enums import (
    SubscriptionEntity,
)
from db_model.subscriptions.remove import (
    remove,
)
import pytest
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = get_module_at_test(file_path=__file__)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["entity", "subject", "email"],
    [
        [
            SubscriptionEntity.GROUP,
            "unittesting",
            "integratesmanager@gmail.com",
        ],
    ],
)
@patch(MODULE_AT_TEST + "operations.delete_item", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "operations.batch_delete_item", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "operations.query", new_callable=AsyncMock)
async def test_remove(
    # pylint: disable=too-many-arguments
    mock_operations_query: AsyncMock,
    mock_operations_batch_delete_item: AsyncMock,
    mock_operations_delete_item: AsyncMock,
    mocked_data_for_module: Callable,
    entity: SubscriptionEntity,
    subject: str,
    email: str,
) -> None:

    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_operations_query,
            "operations.query",
            [entity, subject, email],
        ),
        (
            mock_operations_batch_delete_item,
            "operations.batch_delete_item",
            [entity, subject, email],
        ),
        (
            mock_operations_delete_item,
            "operations.delete_item",
            [entity, subject, email],
        ),
    ]
    # Set up mocks' results using mocked_data_for_module fixture
    for item in mocks_setup_list:
        mock, path, arguments = item
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )
    await remove(
        entity=entity,
        subject=subject,
        email=email,
    )
    mocks_list = [mock[0] for mock in mocks_setup_list]
    assert all(mock_object.called is True for mock_object in mocks_list)
