from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.kubernetes import (
    get_attribute,
    iterate_template_spec,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils.graph import (
    adj_ast,
)


def _insecure_port(graph: Graph, nid: NId) -> Iterator[NId]:
    ports = get_attribute(graph, nid, "ports")
    if ports[0]:
        ports_attrs = graph.nodes[ports[2]]["value_id"]
        for port_id in adj_ast(graph, ports_attrs):
            port = get_attribute(graph, port_id, "port")
            protocol = get_attribute(graph, port_id, "protocol")
            if (
                port[0]
                and protocol[0]
                and port[1] == "80"
                and protocol[1] == "TCP"
            ):
                yield port_id


def k8s_insecure_port(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.KUBERNETES_INSECURE_PORT

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_template_spec(graph):
                for report in _insecure_port(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f332.kubernetes_insecure_port",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
