from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def cs_ldap_connections_authenticated(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.CS_LDAP_CONN_AUTH

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in g.matching_nodes(graph, label_type="MemberAccess"):
                n_attrs = graph.nodes[n_id]
                parent_id = g.pred_ast(graph, n_id)[0]
                if (
                    n_attrs["member"] == "AuthenticationType"
                    and (val_id := graph.nodes[parent_id].get("value_id"))
                    and get_node_evaluation_results(
                        method, graph, n_attrs["expression_id"], set()
                    )
                    and get_node_evaluation_results(
                        method, graph, val_id, {"danger_auth"}
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f320.authenticated_ldap_connections",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cs_ldap_connections_directory(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.CS_LDAP_CONN_AUTH

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in g.matching_nodes(graph, label_type="ObjectCreation"):
                n_attrs = graph.nodes[n_id]
                if (
                    n_attrs["name"] == "DirectoryEntry"
                    and (al_id := n_attrs.get("arguments_id"))
                    and get_node_evaluation_results(
                        method, graph, al_id, {"danger_auth"}
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f320.authenticated_ldap_connections",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
