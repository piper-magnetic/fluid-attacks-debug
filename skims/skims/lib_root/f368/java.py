from collections.abc import (
    Iterator,
)
from lib_root.utilities.common import (
    search_method_invocation_naive,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    evaluate,
    get_node_evaluation_results,
)
from symbolic_eval.utils import (
    get_backward_paths,
)
from utils import (
    graph as g,
)


def is_config_vuln(
    graph: Graph,
    config_ids: tuple[NId, ...],
) -> bool:
    method = MethodsEnum.JAVA_HOST_KEY_CHECKING
    conditions: list[str] = []
    for n_id in config_ids:
        for path in get_backward_paths(graph, n_id):
            evaluation = evaluate(method, graph, path, n_id)
            if evaluation and evaluation.triggers in [{"hostkey"}, {"no"}]:
                conditions = conditions + list(evaluation.triggers)

    if conditions == ["hostkey", "no"]:
        return True
    return False


def is_session_vuln(
    method: MethodsEnum,
    graph: Graph,
    session_args: tuple[NId, ...],
) -> bool:
    if len(session_args) == 1 and get_node_evaluation_results(
        method, graph, session_args[0], set()
    ):
        return True
    if len(session_args) == 2 and is_config_vuln(graph, session_args):
        return True
    return False


def host_key_checking(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JAVA_HOST_KEY_CHECKING
    danger_methods = {"setConfig"}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in search_method_invocation_naive(graph, danger_methods):
                if (
                    (al_list := graph.nodes[n_id].get("arguments_id"))
                    and (args_nodes := g.adj_ast(graph, al_list))
                    and len(args_nodes) > 0
                    and is_session_vuln(method, graph, args_nodes)
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f368.insecure_host_key_checking",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
