from db_model.subscriptions.enums import (
    SubscriptionEntity,
)
from newutils.subscriptions import (
    translate_entity,
)
import pytest

pytestmark = [
    pytest.mark.asyncio,
]


def test_translate_entity() -> None:
    entity_org = SubscriptionEntity.ORGANIZATION
    assert translate_entity(entity=entity_org) == "org"
    entity_group = SubscriptionEntity.GROUP
    assert translate_entity(entity=entity_group) == "group"
    entity_portfolio = SubscriptionEntity.PORTFOLIO
    assert translate_entity(entity=entity_portfolio) == "portfolio"
