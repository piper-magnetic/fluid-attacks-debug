from aioextensions import (
    run,
)
from custom_exceptions import (
    InvalidPatchItem,
)
from db_model.advisories.types import (
    Advisory,
)
import pytest
from pytest_mock import (
    MockerFixture,
)
from sca_patch import (
    ADD,
    check_item,
    main as sca_patch_main,
    REMOVE,
    UPDATE,
)

SCA_PATCH: str = "sca_patch"
ADVISORIES_TEST_DICTS: tuple[dict, dict] = (
    {
        "associated_advisory": "CVE-ADVISORY-1",
        "package_name": "package_name_1",
        "package_manager": "gem",
        "vulnerable_version": ">=2.13.0 <2.15.0",
        "source": "example_source",
        "severity": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
    },
    {
        "associated_advisory": "CVE-ADVISORY-2",
        "package_name": "package_name_2",
        "package_manager": "npm",
        "vulnerable_version": "<2.15.0",
        "source": "MANUAL",
        "severity": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
    },
)
ADVISORIES_TEST_OBJECTS: tuple[Advisory, Advisory] = (
    Advisory(
        associated_advisory="CVE-ADVISORY-1",
        package_manager="gem",
        package_name="package_name_1",
        source="MANUAL",
        vulnerable_version=">=2.13.0 <2.15.0",
        severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
    ),
    Advisory(
        associated_advisory="CVE-ADVISORY-2",
        package_manager="npm",
        package_name="package_name_2",
        source="MANUAL",
        vulnerable_version="<2.15.0",
        severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
    ),
)


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "item,action,expected",
    [
        (
            ADVISORIES_TEST_DICTS[0],
            ADD,
            ADVISORIES_TEST_OBJECTS[0],
        ),
        (
            ADVISORIES_TEST_DICTS[1],
            REMOVE,
            ADVISORIES_TEST_OBJECTS[1],
        ),
    ],
)
def test_check_item(item: dict, action: str, expected: Advisory) -> None:
    result: Advisory = check_item(item, action)
    assert result == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "item,action",
    [
        (
            {
                "associated_advisory": "CVE-ADVISORY-1",
                "package_manager": "gem",
                "vulnerable_version": ">=2.13.0 <2.15.0",
                "source": "MANUAL",
                "severity": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            },
            ADD,
        ),
        (
            {
                "associated_advisory": "CVE-ADVISORY-1",
                "package_name": "package_name_1",
                "package_manager": "gem",
                "vulnerable_version": ">=2.13.0 <2.15.0",
                "severity": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            },
            REMOVE,
        ),
        (
            {
                "associated_advisory": "CVE-ADVISORY-1",
                "package_name": "package_name_1",
                "package_manager": "gem",
                "source": "MANUAL",
            },
            UPDATE,
        ),
    ],
)
def test_check_item_error(item: dict, action: str) -> None:
    with pytest.raises(InvalidPatchItem):
        check_item(item, action)


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "action",
    [ADD, UPDATE, REMOVE],
)
def test_sca_patch_main(mocker: MockerFixture, action: str) -> None:
    mocker.patch(f"{SCA_PATCH}.sys", argv=["test", action, "test_sca.json"])
    dyn_start_mock = mocker.patch(f"{SCA_PATCH}.dynamo_startup")
    dyn_down_mock = mocker.patch(f"{SCA_PATCH}.dynamo_shutdown")
    patch_sca_mock = mocker.patch(f"{SCA_PATCH}.patch_sca")
    mocker.patch(f"{SCA_PATCH}.os.path.exists", return_value=True)

    run(sca_patch_main())
    assert dyn_start_mock.await_count == 1
    assert patch_sca_mock.call_args.args == ("test_sca.json", action)
    assert dyn_down_mock.await_count == 1
