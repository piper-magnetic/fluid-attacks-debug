from custom_exceptions import (
    InvalidCvss3VectorString,
    InvalidSeverityCweIds,
)
from db_model.findings.types import (
    Cvss31Severity,
)
from decimal import (
    Decimal,
)
from newutils import (
    cvss as cvss_utils,
    utils,
)
import pytest


def format_severity(severity: dict[str, float]) -> dict[str, Decimal]:
    return {
        utils.camelcase_to_snakecase(key): Decimal(value)
        for key, value in severity.items()
    }


def test_calculate_cvss3_scope_changed_base_score() -> None:
    severity_dict = {
        "confidentialityImpact": 0.22,
        "integrityImpact": 0.22,
        "availabilityImpact": 0,
        "severityScope": 1,
        "attackVector": 0.85,
        "attackComplexity": 0.77,
        "privilegesRequired": 0.68,
        "userInteraction": 0.85,
    }
    cvss_base_score_test = Decimal(6.4).quantize(Decimal("0.1"))
    severity = Cvss31Severity(**format_severity(severity_dict))
    cvss_base_score = cvss_utils.get_cvss31_base_score(severity)
    assert cvss_base_score == cvss_base_score_test


def test_calculate_cvss3_scope_unchanged_base_score() -> None:
    severity_dict = {
        "confidentialityImpact": 0.22,
        "integrityImpact": 0.22,
        "availabilityImpact": 0,
        "severityScope": 0,
        "attackVector": 0.85,
        "attackComplexity": 0.77,
        "privilegesRequired": 0.62,
        "userInteraction": 0.85,
    }
    cvss_base_score_test = Decimal(5.4).quantize(Decimal("0.1"))
    severity = Cvss31Severity(**format_severity(severity_dict))
    cvss_base_score = cvss_utils.get_cvss31_base_score(severity)
    assert cvss_base_score == cvss_base_score_test


def test_calculate_cvss3_scope_changed_temporal() -> None:
    severity_dict = {
        "confidentialityImpact": 0.22,
        "integrityImpact": 0.22,
        "availabilityImpact": 0,
        "severityScope": 1,
        "attackVector": 0.85,
        "attackComplexity": 0.77,
        "privilegesRequired": 0.68,
        "userInteraction": 0.85,
        "exploitability": 0.97,
        "remediationLevel": 0.97,
        "reportConfidence": 1,
    }
    cvss_temporal_test = Decimal(6.1).quantize(Decimal("0.1"))
    severity = Cvss31Severity(**format_severity(severity_dict))
    cvss_base_score = cvss_utils.get_cvss31_base_score(severity)
    cvss_temporal = cvss_utils.get_cvss31_temporal(severity, cvss_base_score)
    assert cvss_temporal == cvss_temporal_test


def test_calculate_cvss3_scope_unchanged_temporal() -> None:
    severity_dict = {
        "confidentialityImpact": 0.22,
        "integrityImpact": 0.22,
        "availabilityImpact": 0,
        "severityScope": 0,
        "attackVector": 0.85,
        "attackComplexity": 0.77,
        "privilegesRequired": 0.62,
        "userInteraction": 0.85,
        "exploitability": 0.97,
        "remediationLevel": 0.97,
        "reportConfidence": 1,
    }
    cvss_temporal_test = Decimal(5.1).quantize(Decimal("0.1"))
    severity = Cvss31Severity(**format_severity(severity_dict))
    cvss_base_score = cvss_utils.get_cvss31_base_score(severity)
    cvss_temporal = cvss_utils.get_cvss31_temporal(severity, cvss_base_score)
    assert cvss_temporal == cvss_temporal_test


@pytest.mark.parametrize(
    ["vector_string", "cvss31_severity"],
    [
        [
            # Privilege escalation
            "CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:X/RL:X/RC:X",
            Cvss31Severity(
                attack_complexity=Decimal("0.44"),
                attack_vector=Decimal("0.85"),
                availability_impact=Decimal("0.56"),
                availability_requirement=Decimal("0.0"),
                confidentiality_impact=Decimal("0.56"),
                confidentiality_requirement=Decimal("0.0"),
                exploitability=Decimal("1.0"),
                integrity_impact=Decimal("0.56"),
                integrity_requirement=Decimal("0.0"),
                modified_attack_complexity=Decimal("0.0"),
                modified_attack_vector=Decimal("0.0"),
                modified_availability_impact=Decimal("0.0"),
                modified_confidentiality_impact=Decimal("0.0"),
                modified_integrity_impact=Decimal("0.0"),
                modified_privileges_required=Decimal("0.0"),
                modified_user_interaction=Decimal("0.0"),
                modified_severity_scope=Decimal("0.0"),
                privileges_required=Decimal("0.27"),
                remediation_level=Decimal("1.00"),
                report_confidence=Decimal("1.00"),
                severity_scope=Decimal("1.0"),
                user_interaction=Decimal("0.85"),
            ),
        ],
        [
            # Improper authentication for shared folders
            "CVSS:3.1/AV:A/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/E:H/RL:U/RC:C",
            Cvss31Severity(
                attack_complexity=Decimal("0.77"),
                attack_vector=Decimal("0.62"),
                availability_impact=Decimal("0.00"),
                availability_requirement=Decimal("0.0"),
                confidentiality_impact=Decimal("0.56"),
                confidentiality_requirement=Decimal("0.0"),
                exploitability=Decimal("1.0"),
                integrity_impact=Decimal("0.00"),
                integrity_requirement=Decimal("0.0"),
                modified_attack_complexity=Decimal("0.0"),
                modified_attack_vector=Decimal("0.0"),
                modified_availability_impact=Decimal("0.0"),
                modified_confidentiality_impact=Decimal("0.0"),
                modified_integrity_impact=Decimal("0.0"),
                modified_privileges_required=Decimal("0.0"),
                modified_user_interaction=Decimal("0.0"),
                modified_severity_scope=Decimal("0.0"),
                privileges_required=Decimal("0.62"),
                remediation_level=Decimal("1.00"),
                report_confidence=Decimal("1.00"),
                severity_scope=Decimal("0.0"),
                user_interaction=Decimal("0.85"),
            ),
        ],
    ],
)
def test_parse_cvss31_vector_string(
    vector_string: str, cvss31_severity: Cvss31Severity
) -> None:
    assert (
        cvss_utils.parse_cvss31_vector_string(vector_string) == cvss31_severity
    )


@pytest.mark.parametrize(
    ["vector_string"],
    [
        ["CVSS:3.1/AV:N"],
        ["CVSS:3.0/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:X/RL:X/RC:X"],
        ["CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:X/RL:X/RC:T"],
        ["CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H/E:X/RL:X/RC:X/"],
        [
            "AV:L/AC:L/Au:M/C:N/I:P/A:C/E:U/RL:W/RC:ND/CDP:L/TD:H/CR:ND/"
            "IR:ND/AR:M"  # CVSS 2.0
        ],
    ],
)
def test_parse_cvss31_vector_string_fail(vector_string: str) -> None:
    with pytest.raises(InvalidCvss3VectorString):
        cvss_utils.parse_cvss31_vector_string(vector_string)


@pytest.mark.parametrize(
    ["cwe_ids", "expected_result"],
    [
        [None, None],
        [[], None],
        [
            ["CWE-1035", "CWE-770", "CWE-937"],
            ["CWE-1035", "CWE-770", "CWE-937"],
        ],
    ],
)
def test_parse_cwe_ids(
    cwe_ids: list[str] | None, expected_result: list[str] | None
) -> None:
    assert cvss_utils.parse_cwe_ids(cwe_ids) == expected_result


@pytest.mark.parametrize(
    ["cwe_ids"],
    [
        [["CWE-1035", "CWE-770", "CWE-93700"]],
        [["CWE-1035", "CWE-770", "CWE-937x"]],
        [["CWE-1035", "CWE-770", "cwe-937"]],
    ],
)
def test_parse_cwe_ids_fail(cwe_ids: list[str] | None) -> None:
    with pytest.raises(InvalidSeverityCweIds):
        cvss_utils.parse_cwe_ids(cwe_ids)
