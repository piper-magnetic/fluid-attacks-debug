from lib_root.f016.c_sharp import (
    httpclient_no_revocation_list as cs_httpclient_no_revocation_list,
    insecure_shared_access_protocol as cs_insecure_shared_access_protocol,
    service_point_manager_disabled as cs_service_point_manager_disabled,
    weak_protocol as cs_weak_protocol,
)
from lib_root.f016.cloudformation import (
    cfn_elb_without_sslpolicy,
    cfn_serves_content_over_insecure_protocols,
)
from lib_root.f016.kotlin import (
    kt_default_http_client_deprecated,
)
from lib_root.f016.terraform import (
    tfm_aws_elb_without_sslpolicy,
    tfm_aws_serves_content_over_insecure_protocols,
    tfm_azure_api_insecure_protocols,
    tfm_azure_serves_content_over_insecure_protocols,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F016
QUERIES: graph_model.Queries = (
    (FINDING, cs_weak_protocol),
    (FINDING, cs_service_point_manager_disabled),
    (FINDING, cs_insecure_shared_access_protocol),
    (FINDING, cs_httpclient_no_revocation_list),
    (FINDING, cfn_elb_without_sslpolicy),
    (FINDING, cfn_serves_content_over_insecure_protocols),
    (FINDING, tfm_aws_elb_without_sslpolicy),
    (FINDING, tfm_aws_serves_content_over_insecure_protocols),
    (FINDING, tfm_azure_serves_content_over_insecure_protocols),
    (FINDING, tfm_azure_api_insecure_protocols),
    (FINDING, kt_default_http_client_deprecated),
)
