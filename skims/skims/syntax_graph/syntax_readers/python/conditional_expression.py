from model.graph_model import (
    NId,
)
from syntax_graph.syntax_nodes.conditional_access_expression import (
    build_conditional_access_expression_node,
)
from syntax_graph.types import (
    SyntaxGraphArgs,
)
from utils.graph import (
    adj_ast,
)
from utils.graph.text_nodes import (
    node_to_str,
)


def reader(args: SyntaxGraphArgs) -> NId:
    childs = adj_ast(args.ast_graph, args.n_id)
    if len(childs) == 5:
        condition = node_to_str(args.ast_graph, childs[2])
        return build_conditional_access_expression_node(
            args, condition, childs[0]
        )
    return args.n_id
