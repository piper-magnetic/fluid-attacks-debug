from .schema import (
    GROUP,
)
from authz.model import (
    FLUID_IDENTIFIER,
)
from dataloaders import (
    Dataloaders,
)
from db_model.findings.types import (
    Finding,
)
from db_model.groups.types import (
    Group,
)
from decorators import (
    require_asm,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from newutils import (
    utils,
)
from newutils.findings import (
    is_finding_released,
)
from sessions import (
    domain as sessions_domain,
)
from typing import (
    Any,
)


@GROUP.field("findings")
@require_asm
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> list[Finding]:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(
        info.context
    )
    user_email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    group_name: str = parent.name
    filters: dict[str, Any] | None = kwargs.get("filters")
    findings = await loaders.group_drafts_and_findings.load(group_name)
    if not user_email.endswith(FLUID_IDENTIFIER):
        findings = [
            finding for finding in findings if is_finding_released(finding)
        ]
    if filters:
        return utils.filter_findings(findings, filters)

    return findings
