from . import (
    _csv_1,
)
from ._core import (
    CsvKeeper,
)
from dataclasses import (
    dataclass,
)


@dataclass(frozen=True)
class CsvKeeperFactory:
    @staticmethod
    def keeper_1(str_limit: int) -> CsvKeeper:
        return _csv_1.new(str_limit)


__all__ = [
    "CsvKeeper",
]
