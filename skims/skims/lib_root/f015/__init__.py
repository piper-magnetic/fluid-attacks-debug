from lib_root.f015.c_sharp import (
    cs_insecure_authentication,
)
from lib_root.f015.java import (
    java_basic_authentication,
    java_insecure_authentication,
)
from lib_root.f015.terraform import (
    tfm_azure_linux_vm_insecure_authentication,
    tfm_azure_virtual_machine_insecure_authentication,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F015
QUERIES: graph_model.Queries = (
    (FINDING, cs_insecure_authentication),
    (FINDING, java_insecure_authentication),
    (FINDING, java_basic_authentication),
    (FINDING, tfm_azure_linux_vm_insecure_authentication),
    (FINDING, tfm_azure_virtual_machine_insecure_authentication),
)
