from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, Collection]:
    return {
        "Vpcs": [
            {
                "State": "pending",
                "VpcId": "fluidvpc1",
                "OwnerId": "fluidattacks",
                "InstanceTenancy": "default",
            },
        ],
    }
