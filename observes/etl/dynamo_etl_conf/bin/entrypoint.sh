# shellcheck shell=bash

export DYNAMO_PARALLEL=__argSendParallelTableETL__
export DYNAMO_PREPARE_LOADING=__argPrepareLoading__
export DYNAMO_DETERMINE_SCHEMA=__argDynamoSchema__

dynamo-etl "${@}"
