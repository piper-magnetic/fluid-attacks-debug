from dynamodb.types import (
    Item,
    PageInfo,
)
from search.client import (
    get_client,
)
from search.enums import (
    Sort,
)
from search.types import (
    SearchResponse,
)
import simplejson as json
from typing import (
    Any,
)


def _get_end_cursor(*, hits: list[Item], sort_by: list[Item] | None) -> Any:
    if hits:
        if sort_by and "sort" in hits[-1] and hits[-1]["sort"]:
            return json.dumps([str(attr) for attr in hits[-1]["sort"]])
        return hits[-1]["_id"]

    if sort_by:
        return json.dumps([])
    return ""


async def search(  # pylint: disable=too-many-locals
    *,
    exact_filters: Item | None = None,
    index: str,
    limit: int,
    after: str | list[str] | None = None,
    query: str | None = None,
    should_filters: list[Item] | None = None,
    must_filters: list[Item] | None = None,
    must_match_prefix_filters: list[Item] | None = None,
    range_filters: list[Item] | None = None,
    must_not_filters: list[Item] | None = None,
    sort_by: list[Item] | None = None,
) -> SearchResponse:
    """
    Searches for items matching both the user input (full-text)
    and the provided filters (exact matches)

    https://opensearch.org/docs/1.2/opensearch/query-dsl/index/
    https://opensearch-project.github.io/opensearch-py/api-ref/client.html#opensearchpy.OpenSearch.search
    """
    client = await get_client()
    full_and_filters = []
    full_match_prefix_filters = []
    full_must_not_filters = []
    full_or_filters = []
    query_range = []

    if must_filters:
        full_and_filters = [
            {"match": {key: {"query": value, "operator": "and"}}}
            for attrs in must_filters
            for key, value in attrs.items()
        ]

    if must_not_filters:
        full_must_not_filters = [
            {"match": {key: {"query": value, "operator": "and"}}}
            for attrs in must_not_filters
            for key, value in attrs.items()
        ]

    if should_filters:
        full_or_filters = [
            {"match": {key: value}}
            for attrs in should_filters
            for key, value in attrs.items()
        ]

    if range_filters:
        query_range = [{"range": range} for range in range_filters]

    if must_match_prefix_filters:
        full_match_prefix_filters = [
            {"match_phrase_prefix": {key: value}}
            for attrs in must_match_prefix_filters
            for key, value in attrs.items()
        ]

    full_text_queries = [{"multi_match": {"query": query}}] if query else []
    term_queries = (
        [
            {"term": {key: value}}
            for key, value in exact_filters.items()
            if not isinstance(value, list)
        ]
        if exact_filters
        else {}
    )
    terms_queries = (
        [
            {"terms": {key: value}}
            for key, value in exact_filters.items()
            if isinstance(value, list)
        ]
        if exact_filters
        else {}
    )

    body: dict = {
        "query": {
            "bool": {
                "must": [
                    *full_and_filters,
                    *full_text_queries,
                    *query_range,
                    *term_queries,
                    *terms_queries,
                    *full_match_prefix_filters,
                ],
                "should": [
                    *full_or_filters,
                ],
                "minimum_should_match": 1 if full_or_filters else 0,
                "must_not": [*full_must_not_filters],
            }
        },
        "sort": sort_by
        if sort_by
        else [{"_id": {"order": Sort.DESCENDING.value}}],
    }

    if after:
        body["search_after"] = [after] if isinstance(after, str) else after

    response = await client.search(
        body=body,
        index=index,
        size=limit,
    )
    hits: list[Item] = response["hits"]["hits"]

    return SearchResponse(
        items=tuple(hit["_source"] for hit in hits),
        page_info=PageInfo(
            end_cursor=_get_end_cursor(hits=hits, sort_by=sort_by),
            has_next_page=len(hits) > 0,
        ),
        total=response["hits"]["total"]["value"],
    )
