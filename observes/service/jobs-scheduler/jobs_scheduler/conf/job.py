from __future__ import (
    annotations,
)

from enum import (
    Enum,
)
from fa_purity import (
    Result,
)


class InvalidJob(Exception):
    pass


class Job(Enum):
    DYNAMO_INTEGRATES_MAIN = "DYNAMO_INTEGRATES_MAIN"
    DYNAMO_INTEGRATES_MAIN_NO_CACHE = "DYNAMO_INTEGRATES_MAIN_NO_CACHE"

    @staticmethod
    def new_job(raw: str) -> Result[Job, InvalidJob]:
        try:
            return Result.success(Job[raw.upper()])
        except KeyError:
            return Result.failure(InvalidJob())
