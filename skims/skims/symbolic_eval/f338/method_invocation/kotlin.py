from symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def kotlin_check_hashes_salt(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]["expression"].split(".")
    if ma_attr[-1] == "toByteArray":
        args.triggers.add("usesMethod")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
