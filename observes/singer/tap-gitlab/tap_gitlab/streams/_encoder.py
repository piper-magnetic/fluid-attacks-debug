from ._core import (
    JobStream,
    MrStream,
)
from dataclasses import (
    dataclass,
)
from singer_io import (
    JSON,
)


@dataclass(frozen=True)
class StreamEncoder:
    # pylint: disable=no-self-use

    def encode_mr_stream(self, obj: MrStream) -> JSON:
        return {
            "type": "MrStream",
            "obj": {
                "project": obj.project.raw,
                "scope": obj.scope.value,
                "mr_state": obj.mr_state.value,
            },
        }

    def encode_job_stream(self, obj: JobStream) -> JSON:
        return {
            "type": "JobStream",
            "obj": {
                "project": obj.project.raw,
                "scopes": tuple(scope.value for scope in obj.scopes),
            },
        }
