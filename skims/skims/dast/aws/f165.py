from collections.abc import (
    Callable,
    Coroutine,
)
from contextlib import (
    suppress,
)
import csv
from dast.aws.types import (
    Location,
)
from dast.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from io import (
    StringIO,
)
from model import (
    core_model,
)
from model.core_model import (
    AwsCredentials,
    Vulnerability,
)
from typing import (
    Any,
)


async def users_with_multiple_access_keys(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    method = core_model.MethodsEnum.AWS_USER_WITH_MULTIPLE_ACCESS_KEYS
    response: dict[str, Any] = await run_boto3_fun(
        credentials,
        service="iam",
        function="list_users",
    )
    vulns: core_model.Vulnerabilities = ()

    users = response.get("Users", [])

    if users:
        for user in users:
            access_keys: dict[str, Any] = await run_boto3_fun(
                credentials,
                service="iam",
                function="list_access_keys",
                parameters={
                    "UserName": user["UserName"],
                },
            )
            access_key_metadata = access_keys["AccessKeyMetadata"]
            locations: list[Location] = []
            access_keys_activated = list(
                filter(
                    lambda y: y == "Active",
                    list(
                        map(lambda attr: attr["Status"], access_key_metadata)
                    ),
                )
            )
            if len(access_keys_activated) > 1:
                locations = [
                    Location(
                        access_patterns=(),
                        arn=(f"arn:aws:iam:::{user['UserName']}"),
                        values=(),
                        description=(
                            "src.lib_path.f165.users_with_multiple_access_keys"
                        ),
                    ),
                ]
            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=(method),
                    aws_response=access_key_metadata,
                ),
            )

    return vulns


async def root_has_access_keys(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    await run_boto3_fun(
        credentials, service="iam", function="generate_credential_report"
    )
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="iam", function="get_credential_report"
    )
    locations: list[Location] = []
    vulns: core_model.Vulnerabilities = ()
    users_csv = StringIO(response.get("Content", b"").decode())
    credentials_report = tuple(csv.DictReader(users_csv, delimiter=","))
    root_user = credentials_report[0]
    root_arn = root_user["arn"]

    key_names = ("access_key_1_active", "access_key_2_active")
    with suppress(KeyError):
        for index, name in enumerate(key_names):
            if root_user.get(name) == "true":
                locations = [
                    *locations,
                    Location(
                        access_patterns=(f"/{key_names[index]}",),
                        arn=(f"{root_arn}"),
                        values=(root_user[key_names[index]],),
                        description=("src.lib_path.f165.root_has_access_keys"),
                    ),
                ]
    vulns = (
        *vulns,
        *build_vulnerabilities(
            locations=locations,
            method=(core_model.MethodsEnum.AWS_IAM_ROOT_HAS_ACCESS_KEYS),
            aws_response=root_user,
        ),
    )

    return vulns


async def has_not_support_role(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    method = core_model.MethodsEnum.AWS_IAM_HAS_NOT_SUPPORT_ROLE
    vulns: core_model.Vulnerabilities = ()
    attached_times: int = 0
    policy_arn = "arn:aws:iam::aws:policy/AWSSupportAccess"
    entities: dict[str, Any] = await run_boto3_fun(
        credentials,
        service="iam",
        function="list_entities_for_policy",
        parameters={
            "PolicyArn": policy_arn,
        },
    )

    locations: list[Location] = []
    attached_times = (
        len(list(filter(None, entities["PolicyUsers"])))
        + len(list(filter(None, entities["PolicyGroups"])))
        + len(list(filter(None, entities["PolicyRoles"])))
    )
    if attached_times == 0:
        locations = [
            Location(
                access_patterns=(),
                arn=(f"{policy_arn}"),
                values=(),
                description=("src.lib_path.f165.has_not_support_role"),
            ),
        ]
        vulns = (
            *vulns,
            *build_vulnerabilities(
                locations=locations,
                method=(method),
                aws_response=entities,
            ),
        )

    return vulns


async def has_root_active_signing_certificates(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    method = (
        core_model.MethodsEnum.AWS_IAM_HAS_ROOT_ACTIVE_SIGNING_CERTIFICATES
    )
    await run_boto3_fun(
        credentials, service="iam", function="generate_credential_report"
    )
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="iam", function="get_credential_report"
    )
    vulns: core_model.Vulnerabilities = ()
    users_csv = StringIO(response.get("Content", b"").decode())
    credentials_report = tuple(csv.DictReader(users_csv, delimiter=","))
    root_user = credentials_report[0]
    root_arn = root_user["arn"]
    root_has_active_signing_certs: bool = any(
        (
            root_user.get("cert_1_active") == "true",
            root_user.get("cert_2_active") == "true",
        )
    )
    locations: list[Location] = []
    if root_has_active_signing_certs:
        key_names = ("cert_1_active", "cert_2_active")
        for index, name in enumerate(key_names):
            if root_user.get(name) == "true":
                locations = [
                    *locations,
                    Location(
                        access_patterns=(f"/{key_names[index]}",),
                        arn=(f"{root_arn}"),
                        values=(root_user[key_names[index]],),
                        description=(
                            "src.lib_path.f165."
                            "has_root_active_signing_certificates"
                        ),
                    ),
                ]
    vulns = (
        *vulns,
        *build_vulnerabilities(
            locations=locations,
            method=(method),
            aws_response=root_user,
        ),
    )

    return vulns


async def dynamob_encrypted_with_aws_master_keys(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="dynamodb", function="list_tables"
    )
    table_names = response.get("TableNames", []) if response else []
    method = core_model.MethodsEnum.AWS_DYNAMODB_ENCRYPTED_WITH_AWS_MASTER_KEYS
    vulns: core_model.Vulnerabilities = ()
    if table_names:
        for table_name in table_names:
            locations: list[Location] = []

            describe_table: dict[str, Any] = await run_boto3_fun(
                credentials,
                service="dynamodb",
                function="describe_table",
                parameters={
                    "TableName": table_name,
                },
            )
            table_arn = describe_table["Table"]["TableArn"]
            table = describe_table["Table"]
            try:
                table_ssetype = table["SSEDescription"]["SSEType"]
                if table_ssetype == "AES256":
                    locations = [
                        Location(
                            access_patterns=("/SSEDescription/SSEType",),
                            arn=(table_arn),
                            values=(table_ssetype,),
                            description=(
                                "src.lib_path.f165."
                                "dynamob_encrypted_with_aws_master_keys"
                            ),
                        ),
                    ]
                    vulns = (
                        *vulns,
                        *build_vulnerabilities(
                            locations=locations,
                            method=(method),
                            aws_response=table,
                        ),
                    )

            except KeyError:
                locations = [
                    Location(
                        access_patterns=(),
                        arn=(table_arn),
                        values=(),
                        description=(
                            "src.lib_path.f165."
                            "dynamob_encrypted_with_aws_master_keys"
                        ),
                    ),
                ]
                vulns = (
                    *vulns,
                    *build_vulnerabilities(
                        locations=locations,
                        method=(method),
                        aws_response=table,
                    ),
                )
    return vulns


async def eks_has_endpoints_publicly_accessible(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="eks", function="list_clusters"
    )
    method = core_model.MethodsEnum.AWS_EKS_HAS_ENDPOINTS_PUBLICLY_ACCESSIBLE
    cluster_names = response.get("clusters", []) if response else []
    vulns: core_model.Vulnerabilities = ()
    locations: list[Location] = []
    for cluster in cluster_names:
        cluster_desc = await run_boto3_fun(
            credentials,
            service="eks",
            function="describe_cluster",
            parameters={"name": str(cluster)},
        )
        cluster_description = cluster_desc["cluster"]
        vulnerable = (
            cluster_description["resourcesVpcConfig"]["endpointPublicAccess"]
            and not cluster_description["resourcesVpcConfig"][
                "endpointPrivateAccess"
            ]
        )
        if vulnerable:
            locations = [
                Location(
                    access_patterns=(
                        "/resourcesVpcConfig/endpointPrivateAccess",
                        "/resourcesVpcConfig/endpointPublicAccess",
                    ),
                    arn=(cluster_description["arn"]),
                    values=(
                        cluster_description["resourcesVpcConfig"][
                            "endpointPrivateAccess"
                        ],
                        cluster_description["resourcesVpcConfig"][
                            "endpointPublicAccess"
                        ],
                    ),
                    description=(
                        "src.lib_path.f165."
                        "dynamob_encrypted_with_aws_master_keys"
                    ),
                ),
            ]
            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=(method),
                    aws_response=cluster_description,
                ),
            )

    return vulns


async def rds_has_public_snapshots(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="rds", function="describe_db_snapshots"
    )
    snapshots = response.get("DBSnapshots", []) if response else []
    method = core_model.MethodsEnum.AWS_RDS_HAS_PUBLIC_SNAPSHOTS
    vulns: core_model.Vulnerabilities = ()
    for snapshot in snapshots:
        locations: list[Location] = []
        snapshot_attributes: dict[str, Any] = await run_boto3_fun(
            credentials,
            service="rds",
            function="describe_db_snapshot_attributes",
            parameters={
                "DBSnapshotIdentifier": snapshot["DBSnapshotIdentifier"],
            },
        )
        attr_results = snapshot_attributes.get(
            "DBSnapshotAttributesResult", {}
        )
        for index, attr in enumerate(attr_results["DBSnapshotAttributes"]):
            if "all" in attr["AttributeValues"]:
                locations = [
                    *locations,
                    Location(
                        access_patterns=(f"/{index}/AttributeValues",),
                        arn=(
                            "arn:aws:rds:::DBSnapshotIdentifier:"
                            f"{attr_results['DBSnapshotIdentifier']}"
                        ),
                        values=(attr["AttributeValues"],),
                        description=(
                            "src.lib_path.f165.rds_has_public_snapshots"
                        ),
                    ),
                ]
        vulns = (
            *vulns,
            *build_vulnerabilities(
                locations=locations,
                method=(method),
                aws_response=attr_results["DBSnapshotAttributes"],
            ),
        )

    return vulns


async def not_uses_iam_authentication(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="rds", function="describe_db_instances"
    )
    instances = response.get("DBInstances", []) if response else []
    method = core_model.MethodsEnum.AWS_RDS_NOT_USES_IAM_AUTHENTICATION
    vulns: core_model.Vulnerabilities = ()
    for instance in instances:
        locations: list[Location] = []
        if not instance["IAMDatabaseAuthenticationEnabled"]:
            locations = [
                *locations,
                Location(
                    access_patterns=("/IAMDatabaseAuthenticationEnabled",),
                    arn=(instance["DBInstanceArn"]),
                    values=(instance["IAMDatabaseAuthenticationEnabled"],),
                    description=(
                        "src.lib_path.f165.not_uses_iam_authentication"
                    ),
                ),
            ]
            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=(method),
                    aws_response=instance,
                ),
            )

    return vulns


async def redshift_get_paginated_items(
    credentials: AwsCredentials,
) -> list:
    """Get all items in paginated API calls."""
    pools = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "service": "redshift",
        "function": "describe_clusters",
        "parameters": {"MaxRecords": 50},
    }
    data = await run_boto3_fun(**args)
    object_name = "Clusters"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


async def redshift_has_public_clusters(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    clusters = await redshift_get_paginated_items(credentials)
    method = core_model.MethodsEnum.AWS_REDSHIFT_HAS_PUBLIC_CLUSTERS
    vulns: core_model.Vulnerabilities = ()
    if clusters:
        for cluster in clusters:
            if cluster["PubliclyAccessible"]:
                locations = [
                    Location(
                        access_patterns=("/PubliclyAccessible",),
                        arn=(cluster["PubliclyAccessible"]),
                        values=(cluster["ClusterNamespaceArn"],),
                        description=(
                            "src.lib_path.f165.not_uses_iam_authentication"
                        ),
                    ),
                ]
                vulns = (
                    *vulns,
                    *build_vulnerabilities(
                        locations=locations,
                        method=(method),
                        aws_response=cluster,
                    ),
                )

    return vulns


async def redshift_not_requires_ssl(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    method = core_model.MethodsEnum.AWS_REDSHIFT_NOT_REQUIRES_SSL
    vulns: core_model.Vulnerabilities = ()
    locations: list[Location] = []
    clusters = await redshift_get_paginated_items(credentials)
    for cluster in clusters:
        param_groups = cluster.get("ClusterParameterGroups", [])

        for group in param_groups:
            describe_cluster_parameters: dict[str, Any] = await run_boto3_fun(
                credentials,
                service="redshift",
                function="describe_cluster_parameters",
                parameters={
                    "ParameterGroupName": group["ParameterGroupName"],
                },
            )
            params = describe_cluster_parameters.get("Parameters", [])

            for param in params:
                if (
                    param["ParameterName"] == "require_ssl"
                    and param["ParameterValue"] == "false"
                ):
                    locations = [
                        Location(
                            arn=(
                                "arn:aws:redshift::cluster:"
                                f"{cluster['ClusterIdentifier']}"
                            ),
                            description=(
                                "src.lib_path.f165.redshift_not_requires_ssl"
                            ),
                            values=(
                                param["ParameterName"],
                                param["ParameterValue"],
                            ),
                            access_patterns=(
                                "/ParameterName",
                                "/ParameterValue",
                            ),
                        ),
                    ]

                    vulns = (
                        *vulns,
                        *build_vulnerabilities(
                            locations=locations,
                            method=(method),
                            aws_response=param,
                        ),
                    )

    return vulns


async def elasticache_uses_default_port(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="elasticache", function="describe_cache_clusters"
    )
    caches = response.get("CacheClusters", []) if response else []
    method = core_model.MethodsEnum.AWS_ELASTICACHE_USES_DEFAULT_PORT
    vulns: core_model.Vulnerabilities = ()
    for cluster in caches:
        locations: list[Location] = []
        if cluster.get("Engine") == "memcached" and cluster.get(
            "ConfigurationEndpoint"
        )["Port"] in (11211, 6379):
            locations = [
                *locations,
                Location(
                    access_patterns=(
                        "/Engine",
                        "/ConfigurationEndpoint/Port",
                    ),
                    arn=(cluster["ARN"]),
                    values=(
                        cluster["Engine"],
                        cluster["ConfigurationEndpoint"]["Port"],
                    ),
                    description=(
                        "src.lib_path.f165.not_uses_iam_authentication"
                    ),
                ),
            ]
            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=(method),
                    aws_response=cluster,
                ),
            )

    return vulns


async def elasticache_is_transit_encryption_disabled(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="elasticache", function="describe_cache_clusters"
    )
    caches = response.get("CacheClusters", []) if response else []
    method = core_model.MethodsEnum.AWS_ELASTICACHE_TRANSIT_ENCRYPTION_DISABLED
    vulns: core_model.Vulnerabilities = ()
    for cluster in caches:
        locations: list[Location] = []
        if (
            cluster.get("Engine") == "redis"
            and cluster.get("TransitEncryptionEnabled", "") != "True"
        ):
            locations = [
                *locations,
                Location(
                    access_patterns=(
                        "/Engine",
                        "/TransitEncryptionEnabled",
                    ),
                    arn=(cluster["ARN"]),
                    values=(
                        cluster["Engine"],
                        cluster["TransitEncryptionEnabled"],
                    ),
                    description=(
                        "src.lib_path.f165."
                        "elasticache_is_transit_encryption_disabled"
                    ),
                ),
            ]
            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=(method),
                    aws_response=cluster,
                ),
            )

    return vulns


async def elasticache_is_at_rest_encryption_disabled(
    credentials: AwsCredentials,
) -> core_model.Vulnerabilities:
    response: dict[str, Any] = await run_boto3_fun(
        credentials, service="elasticache", function="describe_cache_clusters"
    )
    caches = response.get("CacheClusters", []) if response else []
    method = core_model.MethodsEnum.AWS_ELASTICACHE_REST_ENCRYPTION_DISABLED
    vulns: core_model.Vulnerabilities = ()
    for cluster in caches:
        locations: list[Location] = []
        if (
            cluster.get("Engine") == "redis"
            and cluster.get("AtRestEncryptionEnabled", "") != "True"
        ):
            locations = [
                *locations,
                Location(
                    access_patterns=(
                        "/Engine",
                        "/AtRestEncryptionEnabled",
                    ),
                    arn=(cluster["ARN"]),
                    values=(
                        cluster["Engine"],
                        cluster["AtRestEncryptionEnabled"],
                    ),
                    description=(
                        "src.lib_path.f165."
                        "elasticache_is_at_rest_encryption_disabled"
                    ),
                ),
            ]
            vulns = (
                *vulns,
                *build_vulnerabilities(
                    locations=locations,
                    method=(method),
                    aws_response=cluster,
                ),
            )

    return vulns


CHECKS: tuple[
    Callable[[AwsCredentials], Coroutine[Any, Any, tuple[Vulnerability, ...]]],
    ...,
] = (
    users_with_multiple_access_keys,
    root_has_access_keys,
    has_not_support_role,
    has_root_active_signing_certificates,
    dynamob_encrypted_with_aws_master_keys,
    eks_has_endpoints_publicly_accessible,
    rds_has_public_snapshots,
    not_uses_iam_authentication,
    redshift_has_public_clusters,
    redshift_not_requires_ssl,
    elasticache_uses_default_port,
    elasticache_is_transit_encryption_disabled,
    elasticache_is_at_rest_encryption_disabled,
)
