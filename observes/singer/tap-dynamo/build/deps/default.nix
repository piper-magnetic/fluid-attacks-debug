{
  nixpkgs,
  python_version,
}: let
  lib = {
    buildEnv = nixpkgs."${python_version}".buildEnv.override;
    inherit (nixpkgs."${python_version}".pkgs) buildPythonPackage;
    inherit (nixpkgs.python3Packages) fetchPypi;
  };

  utils = import ./override_utils.nix;
  pkgs_overrides = override: python_pkgs: builtins.mapAttrs (_: override python_pkgs) python_pkgs;

  layer_1 = python_pkgs:
    python_pkgs
    // {
      mypy-boto3-dynamodb = import ./boto3/dynamodb-stubs.nix {inherit lib python_pkgs;};
      types-boto3 = import ./boto3/stubs.nix {inherit lib python_pkgs;};
      types-click = import ./click/stubs.nix {inherit lib;};
      arch-lint = nixpkgs.arch-lint."${python_version}".pkg;
      fa-purity = nixpkgs.fa-purity."${python_version}".pkg;
      fa-singer-io = nixpkgs.fa-singer-io."${python_version}".pkg;
      utils-logger = nixpkgs.utils-logger."${python_version}".pkg;
    };

  jsonschema_override = python_pkgs: utils.replace_pkg ["jsonschema"] (import ./jsonschema.nix lib python_pkgs);
  networkx_override = python_pkgs: utils.replace_pkg ["networkx"] (import ./networkx.nix lib python_pkgs);
  pyrsistent_override = python_pkgs:
    utils.replace_pkg ["pyrsistent"] (
      python_pkgs.pyrsistent.overridePythonAttrs (
        _: {doCheck = false;}
      )
    );
  pytz_override = python_pkgs: utils.replace_pkg ["pytz"] (import ./pytz.nix lib python_pkgs);
  overrides = map pkgs_overrides [
    jsonschema_override
    pyrsistent_override
    pytz_override
    networkx_override
    (_: utils.no_check_override)
  ];

  final_nixpkgs = utils.compose ([layer_1] ++ overrides) nixpkgs."${python_version}Packages";
in {
  inherit lib;
  python_pkgs = final_nixpkgs;
}
