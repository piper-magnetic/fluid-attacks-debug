from ._core import (
    DecodeError,
    JobStream,
    MrStream,
)
from dataclasses import (
    dataclass,
)
from singer_io import (
    JSON,
)
from tap_gitlab.api.projects.ids import (
    ProjectId,
)
from tap_gitlab.api.projects.jobs.page import (
    Scope as JobScope,
)
from tap_gitlab.api.projects.merge_requests.data_page import (
    Scope as MrScope,
    State as MrState,
)


@dataclass(frozen=True)
class StreamDecoder:
    # pylint: disable=no-self-use

    def decode_mr_stream(self, raw: JSON) -> MrStream:
        if raw.get("type") == "MrStream":
            proj = ProjectId.from_name(raw["obj"]["project"])
            scope = MrScope(raw["obj"]["scope"])
            mr_state = MrState(raw["obj"]["mr_state"])
            return MrStream(proj, scope, mr_state)
        raise DecodeError("MrStream")

    def decode_job_stream(self, raw: JSON) -> JobStream:
        if raw.get("type") == "JobStream":
            proj = ProjectId.from_name(raw["obj"]["project"])
            scopes = tuple(JobScope(item) for item in raw["obj"]["scopes"])
            return JobStream(proj, scopes)
        raise DecodeError("MrStream")
