from async_sqs_consumer.queue import (
    Queue,
)
from async_sqs_consumer.worker import (
    Worker,
)
from batch_dispatch.clone_roots import (
    clone_git_root_simple_args,
)
from batch_dispatch.rebase import (
    rebase_root_simple_args,
)
from batch_dispatch.refresh_toe_lines import (
    refresh_toe_lines_simple_args,
)
from server.report_machine import (
    process_execution,
)

worker = Worker(
    queues={
        "default": Queue(
            url=(
                "https://sqs.us-east-1.amazonaws.com/"
                "205810638802/integrates_tasks"
            ),
            max_queue_parallel_messages=10,
            visibility_timeout=240,
        )
    }
)


@worker.task("clone", queue_name="default")
async def clone(
    group_name: str, git_root_id: str, refresh_next_next: bool = False
) -> None:
    await clone_git_root_simple_args(group_name, git_root_id)
    if refresh_next_next:
        await worker.queue_message_to_worker(
            task_id=f"{group_name}_{git_root_id}",
            task_name="refresh_toe_lines",
            queue_alias="default",
            retries=1,
            args=(group_name, git_root_id, refresh_next_next),
        )


@worker.task("refresh_toe_lines", queue_name="default")
async def refresh_toe_lines(
    group_name: str, git_root_id: str, rebase_next: bool = False
) -> None:
    await refresh_toe_lines_simple_args(group_name, git_root_id)
    if rebase_next:
        await worker.queue_message_to_worker(
            task_id=f"{group_name}_{git_root_id}",
            task_name="rebase",
            queue_alias="default",
            retries=1,
            args=(group_name, git_root_id),
        )


@worker.task("rebase", queue_name="default")
async def rebase(group_name: str, git_root_id: str) -> None:
    await rebase_root_simple_args(group_name, git_root_id)


@worker.task("report", queue_name="default")
async def report(execution_id: str) -> None:
    await process_execution(execution_id)


if __name__ == "__main__":
    worker.start()
