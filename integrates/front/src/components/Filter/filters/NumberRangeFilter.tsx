import React, { useState } from "react";

import type { IFilter } from "./types";

import { FormikNumber } from "components/Input/Formik";
import { Col, Row } from "components/Layout";

const NumberRangeFilter = ({
  id,
  label,
  onChange,
  numberRangeValues,
}: IFilter): JSX.Element => {
  const [rangeValues, setRangeValues] = useState(numberRangeValues);

  const handleChange = (
    position: 0 | 1
  ): ((event: React.ChangeEvent<HTMLInputElement>) => void) => {
    return (event: React.ChangeEvent<HTMLInputElement>): void => {
      const value: [string, string] =
        position === 0
          ? [event.target.value, rangeValues?.[1] ?? ""]
          : [rangeValues?.[0] ?? "", event.target.value];
      setRangeValues(value);
      onChange({ id, rangeValues: value });
    };
  };

  return (
    <Row key={id}>
      <Col lg={50} md={50}>
        <FormikNumber
          field={{
            name: id,
            onBlur: (): void => undefined,
            onChange: handleChange(0),
            value: rangeValues?.[0] ?? "",
          }}
          form={{ errors: {}, touched: {} }}
          label={label}
          name={id}
          placeholder={"Min"}
        />
      </Col>
      <Col lg={50} md={50}>
        <FormikNumber
          field={{
            name: id,
            onBlur: (): void => undefined,
            onChange: handleChange(1),
            value: rangeValues?.[1] ?? "",
          }}
          form={{ errors: {}, touched: {} }}
          label={"\n"}
          name={id}
          placeholder={"Max"}
        />
      </Col>
    </Row>
  );
};

export { NumberRangeFilter };
