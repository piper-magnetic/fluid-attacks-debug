from ._client import (
    IssueClient,
    IssueFilter,
)
from tap_gitlab.api2.core.ids import (
    IssueId,
)
from tap_gitlab.api2.core.issue import (
    Issue,
    IssueObj,
    IssueType,
)

__all__ = [
    "IssueId",
    "IssueType",
    "Issue",
    "IssueObj",
    "IssueFilter",
    "IssueClient",
]
