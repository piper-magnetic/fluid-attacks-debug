from collections.abc import (
    Iterator,
)
from lib_root.f153.common import (
    get_accept_header_vulns,
    get_accept_header_vulns_default,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def javascript_accepts_any_mime_method(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JAVASCRIPT_ACCEPTS_ANY_MIME_METHOD

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVASCRIPT):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_accept_header_vulns(graph, method):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def javascript_accepts_any_mime_default(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JAVASCRIPT_ACCEPTS_ANY_MIME_DEFAULT

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVASCRIPT):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_accept_header_vulns_default(graph, method):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
