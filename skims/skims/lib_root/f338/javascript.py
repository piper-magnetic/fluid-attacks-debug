from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def js_salting_is_harcoded(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JS_SALT_IS_HARDCODED

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVASCRIPT):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in g.matching_nodes(graph, label_type="MemberAccess"):
                expression = graph.nodes[n_id]["expression"]
                parent = g.pred_ast(graph, n_id)
                args_id = graph.nodes[parent[0]]["arguments_id"]
                args_salted = g.adj_ast(graph, args_id)
                hash_method = g.adj_ast(graph, n_id)
                if (
                    expression == "update"
                    and get_node_evaluation_results(
                        method, graph, hash_method[0], set()
                    )
                    and get_node_evaluation_results(
                        method, graph, args_salted[0], set()
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f338.salt_is_hardcoded",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
