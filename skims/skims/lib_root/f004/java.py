from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)
from utils.logs import (
    log_blocking,
)


def remote_command_execution(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.JAVA_REMOTE_COMMAND_EXECUTION
    danger_methods = {"exec", "start"}
    danger_set = {"UserParams", "UserConnection"}

    def n_ids() -> Iterator[GraphShardNode]:
        s = 0
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            s += 1
            log_blocking("info", "[%s] Processing shard %s (log_parents=%s)", s, shard, log_parents)
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            log_blocking("info", "[%s] Processing graph %s", s, graph)

            m_n = g.matching_nodes(graph, label_type="MethodInvocation")
            i = 0
            log_blocking("info", "[%s] Processing %s matchable nodes", s, len(m_n))
            for n_id in m_n:
                i += 1
                log_blocking("info", "[%s]%s/%s Matched node %s", s, i, len(m_n), n_id)
                n_attrs = graph.nodes[n_id]
                log_blocking("info", "[%s]%s/%s Got attrs %s", s, i, len(m_n), n_id)
                expr = n_attrs["expression"].split(".")
                log_blocking("info", "[%s]%s/%s Got expr %s (%s)", s, i, len(m_n), n_id, expr[-1] in danger_methods)
                if expr[-1] in danger_methods and get_node_evaluation_results(
                    method, graph, n_id, danger_set, False
                ):
                    log_blocking("info", "[%s]%s/%s Yielding %s %s", s, i, len(m_n), shard, n_id)
                    yield shard, n_id
                else: 
                    log_blocking("info", "[%s]%s/%s Nothing to yield", s, i, len(m_n))

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_path.f004.remote_command_execution",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
