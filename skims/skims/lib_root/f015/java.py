from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils.graph import (
    adj_ast,
    matching_nodes,
)


def java_insecure_authentication(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.JAVA_INSECURE_AUTHENTICATION
    insecure_methods = {"setBasicAuth"}

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in matching_nodes(graph, label_type="MethodInvocation"):
                expr = graph.nodes[n_id].get("expression")
                if expr in insecure_methods and get_node_evaluation_results(
                    method, graph, n_id, set()
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f015.insecure_authentication",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def _is_basic_auth(
    graph: Graph, args_ids: tuple[NId, ...], method: MethodsEnum
) -> bool:
    if get_node_evaluation_results(
        method, graph, args_ids[0], {"authconfig"}
    ) and get_node_evaluation_results(
        method, graph, args_ids[1], {"basicauth"}
    ):
        return True
    return False


def java_basic_authentication(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.JAVA_BASIC_AUTHENTICATION

    def n_ids() -> Iterator[GraphShardNode]:
        """
        Source:
        https://cheatsheetseries.owasp.org/cheatsheets/
        Web_Service_Security_Cheat_Sheet.html
        """
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in matching_nodes(graph, label_type="MethodInvocation"):
                n_attrs = graph.nodes[n_id]
                if (
                    n_attrs.get("expression") == "setHeader"
                    and (al_id := n_attrs.get("arguments_id"))
                    and (childs := adj_ast(graph, al_id))
                    and len(childs) > 1
                    and _is_basic_auth(graph, childs, method)
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f015.basic_authentication",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
