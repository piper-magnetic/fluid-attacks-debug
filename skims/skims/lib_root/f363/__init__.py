from lib_root.f363.cloudformation import (
    cfn_insecure_generate_secret_string,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F363
QUERIES: graph_model.Queries = (
    (FINDING, cfn_insecure_generate_secret_string),
)
