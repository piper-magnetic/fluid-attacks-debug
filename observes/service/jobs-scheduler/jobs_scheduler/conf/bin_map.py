from jobs_scheduler.conf.job import (
    Job,
)
import os
from typing import (
    NoReturn,
)


def job_to_bin_cmd(job: Job) -> str | NoReturn:
    if job is Job.DYNAMO_INTEGRATES_MAIN:
        return f'{os.environ["dynamoDbEtls"]} CORE'
    if job is Job.DYNAMO_INTEGRATES_MAIN_NO_CACHE:
        return f'{os.environ["dynamoDbEtls"]} DETERMINE_SCHEMA'
