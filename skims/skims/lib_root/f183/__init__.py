from lib_root.f183.javascript import (
    js_debugging_enabled,
)
from lib_root.f183.typescript import (
    ts_debugging_enabled,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F183
QUERIES: graph_model.Queries = (
    (FINDING, js_debugging_enabled),
    (FINDING, ts_debugging_enabled),
)
