resource "aws_sqs_queue" "celery_queue" {
  name                      = "celery"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10

  tags = {
    "Name"               = "celery"
    "management:area"    = "cost"
    "management:product" = "machine"
    "management:type"    = "product"
  }
}

resource "aws_sqs_queue" "integrates_tasks" {
  name                      = "integrates_tasks"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false

  tags = {
    "Name"               = "integrates_tasks"
    "management:area"    = "cost"
    "management:product" = "integrates"
    "management:type"    = "product"
  }
}
resource "aws_sqs_queue" "integrates_tasks_dev" {
  name                      = "integrates_tasks_dev"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10
  fifo_queue                = false

  tags = {
    "Name"               = "integrates_tasks_dev"
    "management:area"    = "cost"
    "management:product" = "integrates"
    "management:type"    = "product"
  }
}

resource "aws_sqs_queue" "integrates_streams_dlq" {
  name                      = "integrates_streams_dlq"
  delay_seconds             = 5
  max_message_size          = 2048
  message_retention_seconds = 259200
  receive_wait_time_seconds = 10

  tags = {
    "Name"               = "integrates_streams_dlq"
    "management:area"    = "cost"
    "management:product" = "integrates"
    "management:type"    = "product"
  }
}
