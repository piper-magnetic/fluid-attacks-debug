---
id: config
title: Configuration
sidebar_label: Configuration
slug: /development/skims/guidelines/configuration
---

Skims uses a configuration file in [YAML](https://yaml.org/) syntax.

The general schema is shown and described below:

```yaml
# Description: Assigned a name to the analysis, normally the name of the repository.
# Example:
namespace: repository

# Description: Output path to CSV or SARIF document with results
# Set to a path if you want CSV results.
output:
  file_path: /path/to/results.csv
  format: CSV

# Description:
#   Working directory, normally used as the path to the repository.
# Example:
working_dir: /path/to/your/repository

# Description: SAST for source code.
path:
  # Description: Target files used in the analysis.
  include:
    # Absolute path
    - /path/to/file/or/dir
    # Relative path to `working_dir`
    - src/main/java/org/test/Test.java
    # Unix-style globs
    - glob(*)
    - glob(**.java)
    - glob(src/**/test*.py)
    - .

# Description: Reversing checks for Android APKs.
apk:
  # Description: Target files used in the analysis.
  # Example:
  include:
    # Absolute path
    - /path/to/build/awesome-app-v1.0.apk
    # Relative path to `working_dir`
    - build/awesome-app-v1.0.apk

# Description: DAST checks for endpoints and cloud environments
dast:
  # Description: Endpoints to perform dast (ssl and httṕ) checks
  urls:
    - https://localhost.com
    - https://localhost.com:443
  # Description: Activate SSL checks on the listed urls
  ssl_checks: true
  # Description: Activate http checks on the listed urls
  http_checks: true
  # Description: Development credentials used to access the AWS console
  aws_credentials:
    - access_key_id: "000f"
      secret_access_key: "000f"


# Description: Findings to analyze.
# The complete list of findings can be found here:
# https://gitlab.com/fluidattacks/universe/-/blob/trunk/skims/manifests/findings.lst
# If not present, all security findings will be analyzed.
# Example:
checks:
  - F052

# Description: Language to use, valid values are: EN, ES.
# If not present, defaults to EN (English).
language: EN
```
