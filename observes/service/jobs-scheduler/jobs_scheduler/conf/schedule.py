from fa_purity.frozen import (
    FrozenDict,
    FrozenList,
)
from jobs_scheduler.conf.job import (
    Job,
)
from jobs_scheduler.cron.core import (
    Cron,
    CronItem,
    Days,
    DaysRange,
)
from jobs_scheduler.cron.factory import (
    behind_work_days,
    weekly,
    work_days,
)

ANY = CronItem.any()
SCHEDULE: FrozenDict[Cron, FrozenList[Job]] = FrozenDict(
    {
        weekly(ANY, 0, frozenset([Days.SAT])).unwrap(): (
            Job.DYNAMO_INTEGRATES_MAIN_NO_CACHE,
        ),
        work_days(ANY, 0).unwrap(): (Job.DYNAMO_INTEGRATES_MAIN,),
    }
)
