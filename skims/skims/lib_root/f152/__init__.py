from lib_root.f152.javascript import (
    js_insecure_header_xframe_options,
)
from lib_root.f152.typescript import (
    ts_insecure_header_xframe_options,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F152
QUERIES: graph_model.Queries = (
    (FINDING, ts_insecure_header_xframe_options),
    (FINDING, js_insecure_header_xframe_options),
)
