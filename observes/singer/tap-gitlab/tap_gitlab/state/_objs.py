from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from paginator.pages import (
    PageId,
)
from tap_gitlab.intervals.progress import (
    FragmentedProgressInterval,
)
from tap_gitlab.streams import (
    JobStream,
    MrStream,
)
from typing import (
    Dict,
    Optional,
)


@dataclass(frozen=True)
class JobStatePoint:
    item_id: int
    last_seen: PageId[int]


@dataclass(frozen=True)
class MrStreamState:
    state: FragmentedProgressInterval[datetime]


@dataclass(frozen=True)
class JobStreamState:
    state: FragmentedProgressInterval[JobStatePoint]


@dataclass(frozen=True)
class MrStateMap:
    items: Dict[MrStream, MrStreamState]


@dataclass(frozen=True)
class JobStateMap:
    items: Dict[JobStream, JobStreamState]


@dataclass(frozen=True)
class EtlState:
    jobs: Optional[JobStateMap]
    mrs: MrStateMap
