from symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def java_check_hashes_salt(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]["expression"]
    if ma_attr == "getBytes":
        args.triggers.add("usesMethod")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
