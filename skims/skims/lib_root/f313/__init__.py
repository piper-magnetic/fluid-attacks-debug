from lib_root.f313.cloudformation import (
    cfn_insecure_certificate,
)
from lib_root.f313.python import (
    python_unsafe_certificate_validation,
    python_unsafe_ssl_context_certificate,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F313
QUERIES: graph_model.Queries = (
    (FINDING, cfn_insecure_certificate),
    (FINDING, python_unsafe_certificate_validation),
    (FINDING, python_unsafe_ssl_context_certificate),
)
