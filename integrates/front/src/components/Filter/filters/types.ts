/* eslint @typescript-eslint/no-explicit-any:0 */
import type {
  IPermanentData,
  ISelectedOptions,
  ISwitchOptions,
} from "../types";

interface IFilter {
  id: string;
  label: string;
  onChange: (permanentData: IPermanentData) => void;
  switchValues?: ISwitchOptions[];
  checkValues?: string[];
  mappedOptions?: ISelectedOptions[];
  type?:
    | "checkBoxes"
    | "dateRange"
    | "number"
    | "numberRange"
    | "select"
    | "switch"
    | "text";
  value?: string;
  numberRangeValues?: [string, string];
}

export type { IFilter };
