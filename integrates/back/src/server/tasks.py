from aioextensions import (
    run,
)
from batch_dispatch.clone_roots import (
    clone_git_root_simple_args,
)
from batch_dispatch.rebase import (
    rebase_root_simple_args,
)
from batch_dispatch.refresh_toe_lines import (
    refresh_toe_lines_simple_args,
)
from collections.abc import (
    AsyncIterator,
)
from contextlib import (
    asynccontextmanager,
)
from dynamodb.resource import (
    dynamo_shutdown,
    dynamo_startup,
)
from s3.resource import (
    s3_shutdown,
    s3_startup,
)
from server import (
    SERVER,
)
from server.report_machine import (
    process_execution,
)


@asynccontextmanager
async def setup_connections() -> AsyncIterator:
    await dynamo_startup()
    await s3_startup()
    try:
        yield
    finally:
        await dynamo_shutdown()
        await s3_shutdown()


async def wrap(execution_id: str) -> None:
    async with setup_connections():
        await process_execution(execution_id)


async def wrap_clone(group_name: str, git_root_id: str) -> None:
    async with setup_connections():
        await clone_git_root_simple_args(group_name, git_root_id)


async def wrap_rebase(group_name: str, git_root_id: str) -> None:
    async with setup_connections():
        await rebase_root_simple_args(group_name, git_root_id)


async def wrap_refresh_toe_lines(group_name: str, git_root_id: str) -> None:
    async with setup_connections():
        await refresh_toe_lines_simple_args(group_name, git_root_id)


@SERVER.task(serializer="json", name="process-machine-result", queue="celery")
def report(execution_id: str) -> None:
    run(wrap(execution_id))


@SERVER.task(serializer="json", name="clone", queue="integrates_tasks")
def clone(
    group_name: str,
    git_root_id: str,
    refresh_toe_lines_next: bool = False,
) -> None:
    run(wrap_clone(group_name, git_root_id))
    if refresh_toe_lines_next:
        refresh_toe_lines.apply_async(
            (group_name, git_root_id, refresh_toe_lines_next)
        )


@SERVER.task(serializer="json", name="refresh_toe_lines", queue="celery")
def refresh_toe_lines(
    group_name: str, git_root_id: str, rebase_next: bool = False
) -> None:
    run(wrap_refresh_toe_lines(group_name, git_root_id))
    if rebase_next:
        rebase.apply_async((group_name, git_root_id))


@SERVER.task(serializer="json", name="rebase", queue="integrates_tasks")
def rebase(group_name: str, git_root_id: str) -> None:
    run(wrap_rebase(group_name, git_root_id))
