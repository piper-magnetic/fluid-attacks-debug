from . import (
    get_finding_severity,
    get_result,
)
import pytest
from typing import (
    Any,
)


@pytest.mark.asyncio
@pytest.mark.resolver_test_group("update_severity")
@pytest.mark.parametrize(
    ["email"],
    [
        ["admin@gmail.com"],
        ["hacker@gmail.com"],
        ["hacker@fluidattacks.com"],
    ],
)
async def test_update_severity(populate: bool, email: str) -> None:
    assert populate
    draft_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    result: dict[str, Any] = await get_result(user=email, draft=draft_id)
    assert "errors" not in result
    assert "success" in result["data"]["updateSeverity"]
    assert result["data"]["updateSeverity"]["success"]

    expected_severity = {
        "attackComplexity": 0.77,
        "attackVector": 0.62,
        "availabilityImpact": 0,
        "availabilityRequirement": 1,
        "confidentialityImpact": 0,
        "confidentialityRequirement": 1,
        "exploitability": 0.91,
        "integrityImpact": 0.22,
        "integrityRequirement": 1,
        "modifiedAttackComplexity": 0.77,
        "modifiedAttackVector": 0.62,
        "modifiedAvailabilityImpact": 0,
        "modifiedConfidentialityImpact": 0,
        "modifiedIntegrityImpact": 0.22,
        "modifiedPrivilegesRequired": 0.62,
        "modifiedSeverityScope": 0,
        "modifiedUserInteraction": 0.85,
        "privilegesRequired": 0.62,
        "remediationLevel": 0.97,
        "reportConfidence": 0.92,
        "severityScope": 0,
        "userInteraction": 0.85,
    }
    result = await get_finding_severity(user=email, finding_id=draft_id)
    assert result["data"]["finding"]["severity"] == expected_severity


@pytest.mark.asyncio
@pytest.mark.resolver_test_group("update_severity")
@pytest.mark.parametrize(
    ["email"],
    [
        ["reattacker@gmail.com"],
    ],
)
async def test_update_severity_fail(populate: bool, email: str) -> None:
    assert populate
    draft_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    result: dict[str, Any] = await get_result(user=email, draft=draft_id)
    assert "errors" in result
    assert result["errors"][0]["message"] == "Access denied"
