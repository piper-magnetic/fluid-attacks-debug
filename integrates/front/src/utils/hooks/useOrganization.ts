import { useQuery } from "@apollo/client";
import { useParams } from "react-router-dom";

import { GET_ORGANIZATION } from "./queries";

import { Logger } from "utils/logger";

interface IOrgData {
  country: string;
  name: string;
}

interface IOrgQuery {
  organizationId: IOrgData;
}

const useOrganization = (): IOrgData | undefined => {
  const { organizationName } = useParams<{ organizationName: string }>();

  const { data } = useQuery<IOrgQuery>(GET_ORGANIZATION, {
    fetchPolicy: "cache-first",
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        Logger.error("An error occurred loading organization", message);
      });
    },
    variables: { organizationName },
  });

  if (data === undefined) {
    return undefined;
  }

  const organization = data.organizationId;

  return organization;
};

export { useOrganization };
