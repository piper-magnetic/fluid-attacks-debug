from lib_root.utilities.c_sharp import (
    yield_syntax_graph_object_creation,
)
from model.core_model import (
    MethodsEnum,
)
from model.graph_model import (
    Graph,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)


def insecure_http_headers(graph: Graph, method: MethodsEnum) -> list[str]:
    vuln_nodes: list[str] = []
    for n_id in yield_syntax_graph_object_creation(graph, {"HttpHeaders"}):
        if get_node_evaluation_results(method, graph, n_id, set()):
            vuln_nodes.append(n_id)
    return vuln_nodes
