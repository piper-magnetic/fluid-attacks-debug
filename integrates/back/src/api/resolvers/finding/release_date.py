from .schema import (
    FINDING,
)
from db_model.findings.types import (
    Finding,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from newutils.datetime import (
    get_as_str,
)


@FINDING.field("releaseDate")
def resolve(
    parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None
) -> str | None:
    indicators = parent.unreliable_indicators
    if indicators.unreliable_oldest_vulnerability_report_date:
        return get_as_str(
            indicators.unreliable_oldest_vulnerability_report_date
        )
    return None
