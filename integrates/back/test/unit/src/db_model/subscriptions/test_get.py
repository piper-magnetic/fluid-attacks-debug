from back.test.unit.src.utils import (  # pylint: disable=import-error
    get_module_at_test,
)
from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from db_model.subscriptions.enums import (
    SubscriptionEntity,
    SubscriptionFrequency,
)
from db_model.subscriptions.get import (
    _get_historic_subscription,
    get_all_subscriptions,
)
from db_model.subscriptions.types import (
    Subscription,
    SubscriptionState,
)
import pytest
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = get_module_at_test(file_path=__file__)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["frequency", "expected_result"],
    [
        [
            SubscriptionFrequency.DAILY,
            [
                Subscription(
                    email="integratesmanager@fluidattacks.com",
                    entity=SubscriptionEntity.GROUP,
                    frequency=SubscriptionFrequency.DAILY,
                    subject="unittesting",
                    state=SubscriptionState(
                        modified_date=datetime.fromisoformat(
                            "2022-05-22T20:07:57+00:00"
                        )
                    ),
                ),
            ],
        ],
        [
            SubscriptionFrequency.HOURLY,
            [
                Subscription(
                    email="integratesmanager@gmail.com",
                    entity=SubscriptionEntity.GROUP,
                    frequency=SubscriptionFrequency.HOURLY,
                    subject="unittesting",
                    state=SubscriptionState(
                        modified_date=datetime.fromisoformat(
                            "2022-02-22T20:07:57+00:00"
                        )
                    ),
                ),
            ],
        ],
        [
            SubscriptionFrequency.MONTHLY,
            [
                Subscription(
                    email="integratesmanager@fluidattacks.com",
                    entity=SubscriptionEntity.GROUP,
                    frequency=SubscriptionFrequency.MONTHLY,
                    subject="oneshottest",
                    state=SubscriptionState(
                        modified_date=datetime.fromisoformat(
                            "2022-03-22T20:07:57+00:00"
                        )
                    ),
                ),
            ],
        ],
        [
            SubscriptionFrequency.WEEKLY,
            [
                Subscription(
                    email="test_user_email@test.com",
                    entity=SubscriptionEntity.ORGANIZATION,
                    frequency=SubscriptionFrequency.WEEKLY,
                    subject="test_report_subject",
                    state=SubscriptionState(
                        modified_date=datetime.fromisoformat(
                            "2022-10-27T20:07:57+00:00"
                        )
                    ),
                ),
            ],
        ],
    ],
)
@patch(MODULE_AT_TEST + "operations.query", new_callable=AsyncMock)
async def test_get_all_subscriptions(
    mock_operations_query: AsyncMock,
    mocked_data_for_module: Callable,
    frequency: SubscriptionFrequency,
    expected_result: list[Subscription],
) -> None:

    # Set up mock's result using mocked_data_for_module fixture
    mock_operations_query.return_value = mocked_data_for_module(
        mock_path="operations.query",
        mock_args=[frequency],
        module_at_test=MODULE_AT_TEST,
    )
    result = await get_all_subscriptions(frequency=frequency)
    assert result == expected_result
    assert mock_operations_query.called is True


@pytest.mark.parametrize(
    ["email", "entity", "subject", "expected_result"],
    [
        [
            "integratesmanager@gmail.com",
            SubscriptionEntity.GROUP,
            "unittesting",
            [
                Subscription(
                    email="integratesmanager@fluidattacks.com",
                    entity=SubscriptionEntity.GROUP,
                    frequency=SubscriptionFrequency.DAILY,
                    subject="unittesting",
                    state=SubscriptionState(
                        modified_date=datetime.fromisoformat(
                            "2022-05-22T20:07:57+00:00"
                        ),
                    ),
                )
            ],
        ],
    ],
)
@patch(MODULE_AT_TEST + "operations.query", new_callable=AsyncMock)
async def test__get_historic_subscription(
    # pylint: disable=too-many-arguments
    mock_operations_query: AsyncMock,
    mocked_data_for_module: Callable,
    email: str,
    entity: SubscriptionEntity,
    subject: str,
    expected_result: list[Subscription],
) -> None:

    # Set up mock's result using mocked_data_for_module fixture
    mock_operations_query.return_value = mocked_data_for_module(
        mock_path="operations.query",
        mock_args=[email, entity, subject],
        module_at_test=MODULE_AT_TEST,
    )
    result = await _get_historic_subscription(
        email=email,
        entity=entity,
        subject=subject,
    )
    assert result == expected_result
    assert mock_operations_query.called is True
