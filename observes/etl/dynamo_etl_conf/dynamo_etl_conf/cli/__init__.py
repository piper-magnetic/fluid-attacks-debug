from ._centralize import (
    centralize,
)
import click
from dynamo_etl_conf.executor import (
    default_executor,
    Job,
)
import logging
from typing import (
    NoReturn,
)

LOG = logging.getLogger(__name__)


@click.command()  # type: ignore[misc]
@click.argument(  # type: ignore[misc]
    "job", type=click.Choice([i.name for i in Job], case_sensitive=False)
)
def run(job: str) -> NoReturn:
    exe = default_executor()
    exe.run_job(Job(job)).compute()


@click.group()  # type: ignore[misc]
def main() -> None:
    # cli group entrypoint
    pass


main.add_command(run)
main.add_command(centralize)
