# shellcheck shell=bash

function _get_files_changed {
  git diff-tree --no-commit-id --name-only -r HEAD
}

function main {
  local files
  local name

  : \
    && files="$(_get_files_changed)" \
    && name="$(git show --pretty=format:%s -s HEAD)" \
    && IFS="$(printf \\)" read -r commit_title _ <<< "$name" \
    && if [ "${commit_title[0]}" == "all" ]; then
      return 0
    fi \
    && for file in ${files}; do
      if ! [[ 
        ${file} == "${commit_title[0]}"* ||
        ${file} == "."* ||
        ${file} == "makes"* ]] \
        ; then
        error "The commit tittle needs to begin equal to changed directory"
      else
        continue
      fi
    done
}

main "${@}"
