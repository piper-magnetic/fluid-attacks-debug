from celery import (
    Celery,
)

BROKER_TRANSPORT_OPTIONS = {
    "region": "us-east-1",
    "polling_interval": 0.3,
    "visibility_timeout": 300,
    "predefined_queues": {
        "celery": {
            "url": "https://sqs.us-east-1.amazonaws.com/205810638802/celery",
        },
        "integrates_tasks": {
            "url": (
                "https://sqs.us-east-1.amazonaws.com/"
                "205810638802/integrates_tasks"
            )
        },
    },
}
SERVER = Celery(
    "report",
    broker=("sqs://"),
    include=["server.tasks"],
    broker_transport_options=BROKER_TRANSPORT_OPTIONS,
)

if __name__ == "__main__":
    SERVER.start()
