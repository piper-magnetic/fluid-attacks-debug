from .schema import (
    GROUP,
)
from dataloaders import (
    Dataloaders,
)
from db_model.groups.types import (
    Group,
)
from db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    VulnerabilitiesConnection,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from newutils.vulnerabilities import (
    get_inverted_state_converted,
)


@GROUP.field("forcesVulnerabilities")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    after: str | None = None,
    first: int | None = None,
    state: str | None = None,
    **_kwargs: None,
) -> VulnerabilitiesConnection:
    loaders: Dataloaders = info.context.loaders

    return await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(
            group_name=parent.name,
            state_status=VulnerabilityStateStatus[
                get_inverted_state_converted(state)
            ]
            if state is not None
            else None,
            after=after,
            first=first,
            paginate=True,
        )
    )
