import { gql } from "@apollo/client";
import type { DocumentNode } from "graphql";

const GET_GROUP_SERVICES: DocumentNode = gql`
  query GetGroupServices($groupName: String!) {
    group(groupName: $groupName) {
      name
      serviceAttributes
    }
  }
`;

const GET_STAKEHOLDER_TRIAL: DocumentNode = gql`
  query GetStakeholderTrial {
    me {
      userEmail
      trial {
        extensionDate
        extensionDays
        completed
        startDate
        state
      }
    }
  }
`;

const GET_ORGANIZATION: DocumentNode = gql`
  query GetOrganization($organizationName: String!) {
    organizationId(organizationName: $organizationName) {
      country
      name
    }
  }
`;

export { GET_GROUP_SERVICES, GET_ORGANIZATION, GET_STAKEHOLDER_TRIAL };
