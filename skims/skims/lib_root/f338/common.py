from model.core_model import (
    MethodsEnum,
)
from model.graph_model import (
    Graph,
    NId,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def has_dangerous_param(graph: Graph, method: MethodsEnum) -> list[NId]:
    sensitive_methods = {"salt", "SALT"}

    return [
        n_id
        for n_id in g.matching_nodes(graph, label_type="SymbolLookup")
        if graph.nodes[n_id].get("symbol") in sensitive_methods
        and get_node_evaluation_results(method, graph, n_id, {"danger_salt"})
    ]
