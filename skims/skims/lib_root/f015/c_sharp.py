from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from utils import (
    graph as g,
)


def cs_insecure_authentication(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CS_INSECURE_AUTHENTICATION

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.CSHARP):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in g.matching_nodes(graph, label_type="MethodInvocation"):
                n_attrs = graph.nodes[n_id]
                if (
                    n_attrs["expression"].endswith("Headers.Add")
                    and (al_id := n_attrs.get("arguments_id"))
                    and get_node_evaluation_results(
                        method, graph, n_attrs["expression_id"], {"webreq"}
                    )
                    and (arg_id := g.match_ast(graph, al_id).get("__1__"))
                    and get_node_evaluation_results(
                        method, graph, arg_id, {"basicauth"}
                    )
                ):
                    yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f015.insecure_authentication",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
