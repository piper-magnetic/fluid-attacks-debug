import React from "react";

import { BigScreenLogin } from "./bigScreen";
import { SmallScreenLogin } from "./smallScreen";

import { useWindowSize } from "utils/hooks";

export const Login: React.FC = (): JSX.Element => {
  const { width } = useWindowSize();

  if (width > 1200) {
    return <BigScreenLogin />;
  }

  return <SmallScreenLogin />;
};
