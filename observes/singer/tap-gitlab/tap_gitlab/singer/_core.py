from enum import (
    Enum,
)


class SingerStreams(Enum):
    issue_assignees = "issue_assignees"
    issue_labels = "issue_labels"
    issue = "issue"
    members = "members"
    merge_requests = "merge_requests"
