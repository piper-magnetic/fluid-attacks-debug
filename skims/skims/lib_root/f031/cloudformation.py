from aws.iam.structure import (
    is_action_permissive,
    is_public_principal,
    is_resource_permissive,
)
from collections.abc import (
    Iterator,
)
from itertools import (
    chain,
)
from lib_root.utilities.cloudformation import (
    get_attribute,
    get_list_from_node,
    iterate_group_resources,
    iterate_iam_policy_documents,
    iterate_resource,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils.graph import (
    adj_ast,
)


def _is_s3_action_writeable(actions_list: list) -> bool:
    action_start_with = [
        "Copy",
        "Create",
        "Delete",
        "Put",
        "Restore",
        "Update",
        "Upload",
        "Write",
    ]
    for action in actions_list:
        if any(action.startswith(f"s3:{atw}") for atw in action_start_with):
            return True
    return False


def _iam_user_missing_role_based_security(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    _, _, prop_id = get_attribute(graph, nid, "Properties")
    val_id = graph.nodes[prop_id]["value_id"]
    policies, _, policies_id = get_attribute(graph, val_id, "Policies")
    if policies:
        policies_attrs = graph.nodes[policies_id]["value_id"]
        for pol in adj_ast(graph, policies_attrs):
            policy_name, _, pn_id = get_attribute(graph, pol, "PolicyName")
            if policy_name:
                yield pn_id


def _admin_policy_attached(graph: Graph, nid: NId) -> Iterator[NId]:
    elevated_policies = {
        "PowerUserAccess",
        "IAMFullAccess",
        "AdministratorAccess",
    }
    value = graph.nodes[nid]["value"]
    if value.split("/")[-1] in elevated_policies:
        yield nid


def _iam_has_full_access_to_ssm(graph: Graph, nid: NId) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    action, action_val, action_id = get_attribute(graph, nid, "Action")
    if effect and action and effect_val == "Allow":
        action_attrs = graph.nodes[action_id]["value_id"]
        if graph.nodes[action_attrs]["label_type"] == "ArrayInitializer":
            for act in adj_ast(graph, action_attrs):
                if graph.nodes[act]["value"] == "ssm:*":
                    yield act
        elif action_val == "ssm:*":
            yield action_id


def _negative_statement(graph: Graph, nid: NId) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    action, _, action_id = get_attribute(graph, nid, "NotAction")
    resource, _, res_id = get_attribute(graph, nid, "NotResource")
    action_list = get_list_from_node(graph, action_id)
    res_list = get_list_from_node(graph, res_id)
    if effect and effect_val == "Allow":
        if action:
            yield from (
                action_id
                for act in action_list
                if not is_action_permissive(act)
            )

        if resource:
            yield from (
                res_id for res in res_list if not is_resource_permissive(res)
            )


def _bucket_policy_allows_public_access(
    graph: Graph, nid: NId
) -> Iterator[NId]:
    effect, effect_val, _ = get_attribute(graph, nid, "Effect")
    _, _, action_id = get_attribute(graph, nid, "Action")
    _, _, principal_id = get_attribute(graph, nid, "Principal")
    action_list = get_list_from_node(graph, action_id)
    principal_list = get_list_from_node(graph, principal_id)
    if (
        effect
        and effect_val == "Allow"
        and _is_s3_action_writeable(action_list)
        and is_public_principal(principal_list)
    ):
        yield principal_id


def cfn_bucket_policy_allows_public_access(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_BUCKET_ALLOWS_PUBLIC

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_iam_policy_documents(graph):
                for report in _bucket_policy_allows_public_access(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f031.bucket_policy_allows_public_access",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_negative_statement(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_NEGATIVE_STATEMENT

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_iam_policy_documents(graph):
                for report in _negative_statement(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f031_aws.negative_statement",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_has_full_access_to_ssm(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_FULL_ACCESS_SSM

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_iam_policy_documents(graph):
                for report in _iam_has_full_access_to_ssm(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f031.iam_has_full_access_to_ssm",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_admin_policy_attached(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_ADMIN_POLICY_ATTACHED

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_group_resources(graph, "AWS::IAM"):
                for report in _admin_policy_attached(graph, nid):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f031_aws.permissive_policy",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )


def cfn_iam_user_missing_role_based_security(
    graph_db: GraphDB,
) -> Vulnerabilities:
    method = MethodsEnum.CFN_IAM_MISSING_SECURITY

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in chain(
            graph_db.shards_by_language(GraphLanguage.YAML),
            graph_db.shards_by_language(GraphLanguage.JSON),
        ):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for nid in iterate_resource(graph, "AWS::IAM::User"):
                for report in _iam_user_missing_role_based_security(
                    graph, nid
                ):
                    yield shard, report

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_path.f031.iam_user_missing_role_based_security",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
