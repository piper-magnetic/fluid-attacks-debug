from collections.abc import (
    Iterator,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    Graph,
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
    NId,
)
import re
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from utils import (
    graph as g,
)


def get_vuln_nodes(graph: Graph) -> set[NId]:
    def predicate_matcher(node: dict[str, str]) -> bool:
        method_re = re.compile(r"^\w+\.Header\.Add$")
        return bool(
            (node.get("label_type") == "MethodInvocation")
            and (exp := node.get("expression"))
            and (method_re.match(exp))
        )

    nodes = graph.nodes
    vuln_nodes: set[NId] = set()
    for n_id in g.filter_nodes(graph, nodes, predicate_matcher):
        if args_n_id := g.match_ast_d(graph, n_id, "ArgumentList"):
            args_values = set()
            for arg_n_id in g.adj(graph, args_n_id):
                if (nodes[arg_n_id].get("label_type") == "Literal") and (
                    val := nodes[arg_n_id].get("value")
                ):
                    args_values.add(val.lower()[1:-1])
            if args_values == {"accept", "*/*"}:
                vuln_nodes.add(n_id)

    return vuln_nodes


def go_accepts_any_mime_type(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.GO_ACCEPTS_ANY_MIME_TYPE

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.GO):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph
            for n_id in get_vuln_nodes(graph):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_http.analyze_headers.accept.insecure",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
