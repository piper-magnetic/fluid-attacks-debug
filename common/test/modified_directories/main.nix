{
  inputs,
  makeScript,
  ...
}:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "common-test-modified-directories";
  searchPaths.bin = [
    inputs.nixpkgs.git
  ];
}
