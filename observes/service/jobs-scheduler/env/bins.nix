{outputs}: let
  bins = {
    dynamoDbEtls = outputs."/observes/etl/dynamo/conf";
  };
  export = builtins.attrValues (
    builtins.mapAttrs (name: output: [name output "/bin/${output.name}"]) bins
  );
in
  export
