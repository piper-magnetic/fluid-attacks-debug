# pylint: disable-all
# type: ignore

import certifi
import http as h
import http.client
import httplib
import httplib2
import json
import urllib3

dang_headers = {"Content-Type": "application/json", "Accept": "*/*"}
safe_headers = {"Content-Type": "application/json"}

data = {"name": "John Doe", "age": 30}

# Must Fail pointing to line 19
conn = http.client.HTTPSConnection("www.example.com")
conn.request("GET", "/", headers=dang_headers)

# Must Fail pointing to line 23
conn_vuln = h.client.HTTPConnection("www.example.com")
conn_vuln.request("GET", "/", headers=dang_headers)

# Must Fail pointing to line 29
http = urllib3.PoolManager(ca_certs=certifi.where())
encoded_data = json.dumps(data).encode("utf-8")

resp = http.request(
    "POST",
    "https://httpbin.org/post",
    body=encoded_data,
    headers={"Accept": "*/*"},
)

# Must Fail pointing to line 40
data = json.loads(resp.data.decode("utf-8"))["json"]
url = "https://api.twitter.com/oauth2/token/?grant_type=client_credentials"
http = httplib2.Http()
response, content = http.request(url, method="POST", headers=dang_headers)

# Must Fail pointing to line 44
conn_3 = httplib.HTTPSConnection("www.googleapis.com")
conn_3.request("POST", "/oauth2/v4/token", data, dang_headers)

# Must Pass
conn2 = http.client.HTTPSConnection("www.example.com")
conn2.request("GET", "/", headers=safe_headers)

resp_safe = http.request(
    "POST",
    "https://httpbin.org/post",
    body=encoded_data,
    headers=safe_headers,
)
