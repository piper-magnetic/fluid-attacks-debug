from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)
from datetime import (
    datetime,
)
from tap_gitlab.intervals.interval._objs import (
    ClosedInterval,
    Comparison,
    IntervalPoint,
    MAX,
    MIN,
    OpenInterval,
    OpenLeftInterval,
    OpenRightInterval,
)
from tap_gitlab.intervals.patch import (
    Patch,
)
from typing import (
    cast,
    Generic,
    Type,
    TypeVar,
    Union,
)

_P = TypeVar("_P")


def _default_greater(
    _type: Type[_P],
) -> Comparison[_P]:
    if issubclass(_type, int):

        def greater_int(_x: _P, _y: _P) -> bool:
            return cast(int, _x) > cast(int, _y)

        return greater_int
    if issubclass(_type, datetime):

        def greater_dt(_x: _P, _y: _P) -> bool:
            return cast(datetime, _x) > cast(datetime, _y)

        return greater_dt
    raise NotImplementedError(f"No default greater for type {_type}")


def _build_greater(
    greater: Comparison[_P],
) -> Comparison[IntervalPoint[_P]]:
    def _greater(_x: IntervalPoint[_P], _y: IntervalPoint[_P]) -> bool:
        if _x == _y:
            return False
        if isinstance(_x, MIN) or isinstance(_y, MAX):
            return False
        if isinstance(_x, MAX) or isinstance(_y, MIN):
            return True
        return greater(_x, _y)

    return _greater


@dataclass(frozen=True)
class IntervalFactory(Generic[_P]):
    greater: Patch[Comparison[IntervalPoint[_P]]]

    def __init__(
        self,
        greater_than: Comparison[_P],
    ) -> None:
        object.__setattr__(
            self, "greater", Patch(_build_greater(greater_than))
        )

    @classmethod
    def from_default(cls, data_type: Type[_P]) -> IntervalFactory[_P]:
        return IntervalFactory(_default_greater(data_type))

    def new_closed(self, lower: _P, upper: _P) -> ClosedInterval[_P]:
        return ClosedInterval.new(self.greater.unwrap, lower, upper).unwrap()

    def new_open(
        self, lower: Union[_P, MIN], upper: Union[_P, MAX]
    ) -> OpenInterval[_P]:
        return OpenInterval.new(self.greater.unwrap, lower, upper).unwrap()

    def new_ropen(
        self, lower: _P, upper: Union[_P, MAX]
    ) -> OpenRightInterval[_P]:
        return OpenRightInterval.new(
            self.greater.unwrap, lower, upper
        ).unwrap()

    def new_lopen(
        self, lower: Union[_P, MIN], upper: _P
    ) -> OpenLeftInterval[_P]:
        return OpenLeftInterval.new(self.greater.unwrap, lower, upper).unwrap()
