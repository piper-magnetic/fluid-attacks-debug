export interface IHandleAcceptanceButtonProps {
  areVulnsPendingOfAcceptance: boolean;
  areRequestedZeroRiskVulns: boolean;
  areSubmittedVulns: boolean;
  isEditing: boolean;
  isRequestingReattack: boolean;
  isVerifying: boolean;
  openHandleAcceptance: () => void;
}
