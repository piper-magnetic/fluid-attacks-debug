---
slug: platform/
title: Platform Overview
description: Security should not be an obstacle in the time-to-market of your application.
keywords: Fluid Attacks, Información, Company, Security, Experts, Pentesting, Ethical Hacking
template: productOverview
---
