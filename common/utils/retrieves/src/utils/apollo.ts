// eslint-disable-next-line import/no-unresolved
import { workspace } from "vscode";

import { getClient } from "./api";

const API_CLIENT = getClient(
  workspace.getConfiguration("fluidattacks").get("api_token") ??
    workspace.getConfiguration("fluidattacks").get("apiToken") ??
    process.env.INTEGRATES_API_TOKEN ??
    ""
);

export { API_CLIENT };
