/* eslint-disable complexity, react/forbid-component-props */
import { Buffer } from "buffer";

import { useMutation, useQuery } from "@apollo/client";
import type { ApolloError } from "@apollo/client";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import type { FormikProps } from "formik";
import { Form, Formik } from "formik";
import type { GraphQLError } from "graphql";
import _ from "lodash";
import type { ChangeEvent, FC } from "react";
import React, {
  Fragment,
  useCallback,
  useContext,
  useRef,
  useState,
} from "react";
import { useTranslation } from "react-i18next";

import { CredentialsType } from "./credentialsType";
import { HealthCheck } from "./HealthCheck";
import { RepositoryTour } from "./RepositoryTour";

import {
  GET_ORGANIZATION_CREDENTIALS,
  VALIDATE_GIT_ACCESS,
} from "../../queries";
import type { ICredentialsAttr, IFormValues } from "../../types";
import { formatTypeCredentials } from "../../utils";
import { GitIgnoreAlert, gitModalSchema } from "../helpers";
import { QuestionButton } from "../styles";
import { Alert } from "components/Alert";
import type { IAlertProps } from "components/Alert";
import { Button } from "components/Button";
import { Checkbox, Input, InputArray, Label, Select } from "components/Input";
import { Col, Hr, Row } from "components/Layout";
import { ModalConfirm } from "components/Modal";
import { Text } from "components/Text";
import { groupContext } from "scenes/Dashboard/group/context";
import type { IGroupContext } from "scenes/Dashboard/group/types";
import { Can } from "utils/authz/Can";
import { Logger } from "utils/logger";
import { openUrl } from "utils/resourceHelpers";
import {
  composeValidators,
  hasSshFormat,
  validTextField,
} from "utils/validations";

interface IRepositoryProps {
  initialValues: IFormValues;
  isEditing: boolean;
  manyRows: boolean | undefined;
  modalMessages: { message: string; type: string };
  nicknames: string[];
  onClose: () => void;
  onSubmit: (values: IFormValues) => Promise<void>;
  runTour: boolean;
  finishTour: () => void;
}

const Repository: FC<IRepositoryProps> = ({
  initialValues,
  isEditing,
  nicknames,
  manyRows = false,
  modalMessages,
  onClose,
  onSubmit,
  runTour,
  finishTour,
}: IRepositoryProps): JSX.Element => {
  const { t } = useTranslation();
  const { organizationId }: IGroupContext = useContext(groupContext);
  const formRef = useRef<FormikProps<IFormValues>>(null);

  // GraphQL operations
  const { data: organizationCredentialsData } = useQuery<{
    organization: { credentials: ICredentialsAttr[] };
  }>(GET_ORGANIZATION_CREDENTIALS, {
    onError: ({ graphQLErrors }: ApolloError): void => {
      graphQLErrors.forEach((error: GraphQLError): void => {
        Logger.error("Couldn't load organization credentials", error);
      });
    },
    variables: {
      organizationId,
    },
  });

  const isDuplicated = (field: string): boolean => {
    const repoName: string = field
      ? field.split("/").slice(-1)[0].replace(".git", "")
      : "";
    const { nickname: initialNickname } = initialValues;

    return nicknames.includes(repoName) && initialNickname !== repoName;
  };

  const [isGitAccessible, setIsGitAccessible] = useState(true);
  const [repoUrl, setRepoUrl] = useState(initialValues.url);
  const [credExists, setCredExists] = useState(
    (!_.isNull(initialValues.credentials) &&
      initialValues.credentials.id !== "") ||
      manyRows
  );
  const [disabledCredsEdit, setDisabledCredsEdit] = useState(
    (!_.isNull(initialValues.credentials) &&
      initialValues.credentials.id !== "") ||
      manyRows
  );
  const [hasSquad, setHasSquad] = useState(false);
  const [isCheckedHealthCheck, setIsCheckedHealthCheck] = useState(isEditing);

  const goToDocumentation = useCallback((): void => {
    openUrl(
      "https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitignore.html#_pattern_format"
    );
  }, []);

  const [showGitAlert, setShowGitAlert] = useState(false);
  const [showSubmitAlert, setShowSubmitAlert] = useState(false);

  const [validateGitMsg, setValidateGitMsg] = useState({
    message: "",
    type: "success",
  });
  const [validateGitAccess] = useMutation(VALIDATE_GIT_ACCESS, {
    onCompleted: (): void => {
      setShowGitAlert(false);
      setIsGitAccessible(true);
      setValidateGitMsg({
        message: t("group.scope.git.repo.credentials.checkAccess.success"),
        type: "success",
      });
      formRef.current?.validateField("credentials.token");
    },
    onError: ({ graphQLErrors }: ApolloError): void => {
      setShowGitAlert(false);
      graphQLErrors.forEach((error: GraphQLError): void => {
        switch (error.message) {
          case "Exception - Git repository was not accessible with given credentials":
            setValidateGitMsg({
              message: t("group.scope.git.errors.invalidGitCredentials"),
              type: "error",
            });
            break;
          case "Exception - Branch not found":
            setValidateGitMsg({
              message: t("group.scope.git.errors.invalidBranch"),
              type: "error",
            });
            break;
          default:
            setValidateGitMsg({
              message: t("groupAlerts.errorTextsad"),
              type: "error",
            });
            Logger.error("Couldn't activate root", error);
        }
      });
      setIsGitAccessible(false);
    },
  });

  const organizationCredentials = _.isUndefined(organizationCredentialsData)
    ? []
    : organizationCredentialsData.organization.credentials;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const groupedExistingCreds =
    organizationCredentials.length > 0
      ? Object.fromEntries(
          organizationCredentials.map((cred): [string, ICredentialsAttr] => [
            cred.id,
            cred,
          ])
        )
      : {};

  const handleCheckAccessClick = useCallback((): void => {
    if (formRef.current !== null) {
      void validateGitAccess({
        variables: {
          branch: formRef.current.values.branch,
          credentials: {
            key: formRef.current.values.credentials.key
              ? Buffer.from(formRef.current.values.credentials.key).toString(
                  "base64"
                )
              : undefined,
            name: formRef.current.values.credentials.name,
            password: formRef.current.values.credentials.password,
            token: formRef.current.values.credentials.token,
            type: formRef.current.values.credentials.type,
            user: formRef.current.values.credentials.user,
          },
          url: formRef.current.values.url,
        },
      });
    }
  }, [validateGitAccess]);

  const submittableCredentials = (values: IFormValues): boolean => {
    if (values.useVpn || credExists) {
      return true;
    }
    if (values.credentials.typeCredential === "") {
      return true;
    }
    if (
      values.credentials.typeCredential === "SSH" &&
      (!values.credentials.name ||
        !values.credentials.key ||
        hasSshFormat(values.credentials.key) !== undefined)
    ) {
      return true;
    }
    if (
      values.credentials.typeCredential === "USER" &&
      (!values.credentials.name ||
        !values.credentials.user ||
        !values.credentials.password)
    ) {
      return true;
    }
    if (
      values.credentials.typeCredential === "TOKEN" &&
      (!values.credentials.name ||
        !values.credentials.token ||
        !values.credentials.azureOrganization)
    ) {
      return true;
    }

    return false;
  };

  const cleanValues = (): void => {
    setCredExists(false);
    formRef.current?.setFieldValue("credentials.name", "", false);
    formRef.current?.setFieldValue("credentials.id", "", false);
    formRef.current?.setFieldValue("credentials.password", "", false);
    formRef.current?.setFieldValue("credentials.token", "", false);
    formRef.current?.setFieldValue("credentials.user", "", false);
    formRef.current?.setFieldValue("credentials.key", "", false);
    formRef.current?.setFieldValue("credentials.azureOrganization", "", false);
    formRef.current?.setFieldValue("credentials.type", "", false);
    formRef.current?.setFieldValue("credentials.typeCredential", "", false);
  };

  const chooseCredentialType = useCallback(
    (url: string, isCredExisting: boolean): void => {
      const httpsRegex = /^https:\/\/(?:(?!\b.*dev\.azure\.com\b)).*/u;
      const azureRegex = /.*(?:dev\.azure\.com).*/u;
      const sshRegex = /^(?:ssh|git@).*/u;
      if (!isCredExisting) {
        if (azureRegex.test(url)) {
          cleanValues();
          formRef.current?.setFieldValue(
            "credentials.typeCredential",
            "TOKEN",
            false
          );
          formRef.current?.setFieldValue("credentials.type", "HTTPS", false);
          formRef.current?.setFieldValue("credentials.auth", "TOKEN", false);
          formRef.current?.setFieldValue("credentials.isPat", true, false);
        }

        if (httpsRegex.test(url)) {
          cleanValues();
          formRef.current?.setFieldValue(
            "credentials.typeCredential",
            "USER",
            false
          );
          formRef.current?.setFieldValue("credentials.type", "HTTPS", false);
          formRef.current?.setFieldValue("credentials.auth", "USER", false);
          formRef.current?.setFieldValue("credentials.isPat", false, false);
        }

        if (sshRegex.test(url)) {
          cleanValues();
          formRef.current?.setFieldValue(
            "credentials.typeCredential",
            "SSH",
            false
          );
          formRef.current?.setFieldValue("credentials.type", "SSH", false);
          formRef.current?.setFieldValue("credentials.auth", "", false);
          formRef.current?.setFieldValue("credentials.isPat", false, false);
        }
        if (
          !azureRegex.test(url) &&
          !httpsRegex.test(url) &&
          !sshRegex.test(url)
        ) {
          cleanValues();
        }
      }

      if (
        isCredExisting &&
        ((!azureRegex.test(url) &&
          !httpsRegex.test(url) &&
          !sshRegex.test(url)) ||
          url === "")
      ) {
        cleanValues();
      }
    },
    []
  );

  const onChangeExits = useCallback(
    (event: ChangeEvent<HTMLSelectElement>): void => {
      if (event.target.value === "") {
        setCredExists(manyRows || false);
        setDisabledCredsEdit(manyRows || false);
        chooseCredentialType(repoUrl, false);
      } else {
        const currentCred = groupedExistingCreds[event.target.value];
        formRef.current?.setFieldValue(
          "credentials.typeCredential",
          formatTypeCredentials(currentCred)
        );
        formRef.current?.setFieldValue("credentials.type", currentCred.type);
        formRef.current?.setFieldValue("credentials.name", currentCred.name);
        formRef.current?.setFieldValue("credentials.id", currentCred.id);
        setCredExists(true);
        setDisabledCredsEdit(true);
      }
    },
    [chooseCredentialType, groupedExistingCreds, manyRows, repoUrl]
  );

  const onChangeUrl = useCallback(
    (event: ChangeEvent<HTMLInputElement>): void => {
      const urlValue = event.target.value;
      setRepoUrl(urlValue);
      chooseCredentialType(urlValue, credExists);
    },
    [credExists, chooseCredentialType]
  );

  return (
    <Formik
      initialValues={initialValues}
      innerRef={formRef}
      name={"gitRoot"}
      onSubmit={onSubmit}
      validationSchema={gitModalSchema(
        isEditing,
        credExists,
        hasSquad,
        initialValues,
        isCheckedHealthCheck,
        isDuplicated,
        isGitAccessible,
        nicknames
      )}
    >
      {({
        dirty,
        errors,
        isSubmitting,
        setFieldValue,
        values,
      }): // eslint-disable-next-line
        JSX.Element => {  // NOSONAR
        if (isSubmitting) {
          setShowSubmitAlert(false);
        }

        function onTypeChange(
          event: React.ChangeEvent<HTMLSelectElement>
        ): void {
          event.preventDefault();
          if (event.target.value === "SSH") {
            setFieldValue("credentials.type", "SSH");
            setFieldValue("credentials.auth", "");
            setFieldValue("credentials.isPat", false);
          }
          if (event.target.value === "") {
            setFieldValue("type", "");
            setFieldValue("auth", "");
            setFieldValue("isPat", false);
          }
          if (event.target.value === "USER") {
            setFieldValue("credentials.type", "HTTPS");
            setFieldValue("credentials.auth", "USER");
            setFieldValue("credentials.isPat", false);
          }
          if (event.target.value === "TOKEN") {
            setFieldValue("credentials.type", "HTTPS");
            setFieldValue("credentials.auth", "TOKEN");
            setFieldValue("credentials.isPat", true);
          }
        }

        return (
          <Fragment>
            <Form>
              <fieldset className={"bn"}>
                <Row>
                  {manyRows ? undefined : (
                    <Col lg={50} md={50} sm={50}>
                      <Input
                        fw={"bold"}
                        id={"git-root-add-repo-url"}
                        label={t("group.scope.git.repo.url.text")}
                        name={"url"}
                        onChange={onChangeUrl}
                        placeholder={t("group.scope.git.repo.url.placeHolder")}
                        required={true}
                        tooltip={t("group.scope.git.repo.url.toolTip")}
                        type={"text"}
                        validate={composeValidators([validTextField])}
                      />
                    </Col>
                  )}
                  <Col id={"git-root-add-repo-branch"} lg={50} md={50} sm={50}>
                    <Input
                      fw={"bold"}
                      label={t("group.scope.git.repo.branch.text")}
                      name={"branch"}
                      required={true}
                      tooltip={t("group.scope.git.repo.branch.toolTip")}
                    />
                  </Col>
                  <Col id={"git-root-add-use-vpn"} lg={100} md={100} sm={100}>
                    <Checkbox
                      label={t("group.scope.git.repo.useVpn")}
                      name={"useVpn"}
                    />
                  </Col>
                  {isEditing && values.branch !== initialValues.branch ? (
                    <Alert>{t("group.scope.common.changeWarning")}</Alert>
                  ) : undefined}
                  {isDuplicated(values.url) || isEditing ? (
                    <Input
                      fw={"bold"}
                      id={"git-root-add-nickname"}
                      label={t("group.scope.git.repo.nickname")}
                      name={"nickname"}
                      placeholder={t("group.scope.git.repo.nicknameHint")}
                      required={true}
                    />
                  ) : undefined}
                </Row>
                <Hr mv={16} />
                <Row id={"git-root-add-credentials"}>
                  <Col lg={100} md={100} sm={100}>
                    <Text fw={8} mb={2} size={"medium"}>
                      {t("group.scope.git.repo.credentials.title")}
                    </Text>
                  </Col>
                  {_.isEmpty(groupedExistingCreds) ? null : (
                    <Col lg={100} md={100} sm={100}>
                      <Select
                        label={t("group.scope.git.repo.credentials.existing")}
                        name={"credentials.id"}
                        onChange={onChangeExits}
                      >
                        <option value={""}>{""}</option>
                        {Object.values(groupedExistingCreds).map(
                          (cred): JSX.Element => (
                            <option key={cred.id} value={cred.id}>
                              {cred.name}
                            </option>
                          )
                        )}
                      </Select>
                    </Col>
                  )}
                  <Col lg={50} md={50} sm={50}>
                    <Select
                      disabled={true}
                      fw={"bold"}
                      label={t("group.scope.git.repo.credentials.type.text")}
                      name={"credentials.typeCredential"}
                      // eslint-disable-next-line
                      onChange={onTypeChange} // NOSONAR
                      required={!isEditing}
                      tooltip={t(
                        "group.scope.git.repo.credentials.type.toolTip"
                      )}
                    >
                      <option value={""}>{""}</option>
                      <option value={"SSH"}>
                        {t("group.scope.git.repo.credentials.ssh")}
                      </option>
                      <option value={"USER"}>
                        {t("group.scope.git.repo.credentials.userHttps")}
                      </option>
                      <option value={"TOKEN"}>
                        {t("group.scope.git.repo.credentials.azureToken")}
                      </option>
                      {disabledCredsEdit ? (
                        <option value={"OAUTH"}>
                          {t("group.scope.git.repo.credentials.oauth")}
                        </option>
                      ) : undefined}
                    </Select>
                  </Col>
                  <Col lg={50} md={50} sm={50}>
                    <Input
                      disabled={disabledCredsEdit}
                      fw={"bold"}
                      label={t("group.scope.git.repo.credentials.name.text")}
                      name={"credentials.name"}
                      required={!isEditing}
                      tooltip={t(
                        "group.scope.git.repo.credentials.name.toolTip"
                      )}
                    />
                  </Col>
                  {_.isEmpty(repoUrl) &&
                  _.isEmpty(values.credentials.typeCredential) ? undefined : (
                    <CredentialsType credExists={credExists} values={values} />
                  )}
                  {!showGitAlert && validateGitMsg.message !== "" ? (
                    <Alert
                      onTimeOut={setShowGitAlert}
                      variant={validateGitMsg.type as IAlertProps["variant"]}
                    >
                      {validateGitMsg.message}
                    </Alert>
                  ) : undefined}
                  {submittableCredentials(values) ? undefined : (
                    <Col lg={100} md={100} sm={100}>
                      <Button
                        disp={"inline"}
                        id={"checkAccessBtn"}
                        onClick={handleCheckAccessClick}
                        variant={"secondary"}
                      >
                        {t("group.scope.git.repo.credentials.checkAccess.text")}
                      </Button>
                    </Col>
                  )}
                  <Input
                    fw={"bold"}
                    id={"git-root-add-env"}
                    label={t("group.scope.git.repo.environment.text")}
                    name={"environment"}
                    placeholder={t("group.scope.git.repo.environment.hint")}
                    required={true}
                    tooltip={t("group.scope.git.repo.environment.toolTip")}
                  />
                </Row>
              </fieldset>
              <HealthCheck
                initValues={initialValues}
                isEditing={isEditing}
                isHealthChecked={isCheckedHealthCheck}
                setHasSquad={setHasSquad}
                setIsHealthChecked={setIsCheckedHealthCheck}
                values={values}
              />
              {manyRows ? undefined : (
                <Can do={"update_git_root_filter"}>
                  <fieldset className={"bn"}>
                    <Label
                      htmlFor={"group.scope.git.filter"}
                      tooltip={
                        runTour
                          ? undefined
                          : t("group.scope.git.filter.tooltip")
                      }
                    >
                      <QuestionButton onClick={goToDocumentation}>
                        <FontAwesomeIcon icon={faQuestionCircle} />
                      </QuestionButton>
                      &nbsp;
                      {t("group.scope.git.filter.exclude")}
                    </Label>
                    <GitIgnoreAlert gitignore={values.gitignore} />
                    <InputArray initValue={""} name={"gitignore"}>
                      {(fieldName: string): JSX.Element => (
                        <Input
                          name={fieldName}
                          placeholder={t("group.scope.git.filter.placeholder")}
                        />
                      )}
                    </InputArray>
                  </fieldset>
                </Can>
              )}
              {!showSubmitAlert && modalMessages.message !== "" ? (
                <Alert
                  onTimeOut={setShowSubmitAlert}
                  variant={modalMessages.type as IAlertProps["variant"]}
                >
                  {modalMessages.message}
                </Alert>
              ) : undefined}
              <ModalConfirm
                disabled={
                  (!isGitAccessible && !values.useVpn) || !dirty || isSubmitting
                }
                id={"git-root-add-confirm"}
                onCancel={onClose}
              />
            </Form>
            {runTour ? (
              <RepositoryTour
                dirty={dirty}
                errors={errors}
                finishTour={finishTour}
                isGitAccessible={isGitAccessible}
                runTour={runTour}
                values={values}
              />
            ) : undefined}
          </Fragment>
        );
      }}
    </Formik>
  );
};

export { Repository };
