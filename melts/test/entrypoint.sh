# shellcheck shell=bash

function main {
  : \
    && aws_login "dev" "3600" \
    && use_git_repo_services \
    && melts drills --pull-repos absecon \
    && services_path=$(pwd) \
    && popd \
    && cp -r melts/ "${services_path}" \
    && pushd "${services_path}/melts" \
    && pytest \
      --verbose \
      --exitfirst \
      --color=yes \
      --capture=fd \
      --durations=0 \
      --failed-first \
      --disable-warnings \
      --cov=toolbox \
      --cov-branch \
      --cov-report term \
      --cov-report html:.coverage-html \
      --no-cov-on-fail \
      --numprocesses=auto \
      --random-order \
      --reruns 10 \
      --reruns-delay 1 \
    && popd || return 1 \
    && return 0
}

main "${@}"
