from lib_root.f153.c_sharp import (
    c_sharp_accepts_any_mime_type,
)
from lib_root.f153.go import (
    go_accepts_any_mime_type,
)
from lib_root.f153.java import (
    java_accepts_any_mime_type_chain,
    java_http_accepts_any_mime_type,
)
from lib_root.f153.javascript import (
    javascript_accepts_any_mime_default,
    javascript_accepts_any_mime_method,
)
from lib_root.f153.python import (
    python_accepts_any_mime_instance,
    python_accepts_any_mime_type,
)
from lib_root.f153.typescript import (
    typescript_accepts_any_mime_default,
    typescript_accepts_any_mime_method,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F153
QUERIES: graph_model.Queries = (
    (FINDING, java_accepts_any_mime_type_chain),
    (FINDING, java_http_accepts_any_mime_type),
    (FINDING, c_sharp_accepts_any_mime_type),
    (FINDING, javascript_accepts_any_mime_method),
    (FINDING, typescript_accepts_any_mime_method),
    (FINDING, javascript_accepts_any_mime_default),
    (FINDING, typescript_accepts_any_mime_default),
    (FINDING, python_accepts_any_mime_type),
    (FINDING, python_accepts_any_mime_instance),
    (FINDING, go_accepts_any_mime_type),
)
