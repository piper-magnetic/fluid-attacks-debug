from lib_root.f426.kubernetes import (
    k8s_image_has_digest,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F426
QUERIES: graph_model.Queries = ((FINDING, k8s_image_has_digest),)
