from lib_root.f332.cloudformation import (
    cfn_elb2_uses_insecure_protocol,
)
from lib_root.f332.kubernetes import (
    k8s_insecure_port,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F332
QUERIES: graph_model.Queries = (
    (FINDING, cfn_elb2_uses_insecure_protocol),
    (FINDING, k8s_insecure_port),
)
