from lib_root.f380.docker_compose import (
    docker_compose_image_has_digest,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F380
QUERIES: graph_model.Queries = ((FINDING, docker_compose_image_has_digest),)
