from symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def value_is_hardcoded(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    node_attrs = args.graph.nodes[args.n_id]
    if (
        node_attrs["label_type"] == "Literal"
        and node_attrs["value_type"] == "string"
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
