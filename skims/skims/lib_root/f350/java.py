from collections.abc import (
    Iterator,
)
from lib_root.utilities.java import (
    yield_method_invocation_syntax_graph,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)
from symbolic_eval.evaluate import (
    get_node_evaluation_results,
)


def use_insecure_trust_manager(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.JAVA_INSECURE_TRUST_MANAGER

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.JAVA):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for m_id, m_name in yield_method_invocation_syntax_graph(graph):
                m_split = m_name.lower().split(".")
                if (
                    m_split[0] == "sslcontextbuilder"
                    and m_split[-1] == "trustmanager"
                    and get_node_evaluation_results(method, graph, m_id, set())
                ):
                    yield shard, m_id

    return get_vulnerabilities_from_n_ids(
        desc_key="lib_root.f350.insecure_trust_manager",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
