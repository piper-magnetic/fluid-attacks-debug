from app.views.auth import (
    log_stakeholder_in,
)
from back.test.unit.src.utils import (  # pylint: disable=import-error
    get_module_at_test,
)
from dataloaders import (
    Dataloaders,
    get_new_context,
)
import pytest
from sessions.types import (
    UserAccessInfo,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = get_module_at_test(file_path=__file__)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["user_info"],
    [
        [
            UserAccessInfo(
                first_name="First_Name",
                last_name="Last_Name",
                user_email="integratesuser2@fluidattacks.com",
            )
        ]
    ],
)
@patch(
    MODULE_AT_TEST + "stakeholders_domain.update_last_login",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "utils.send_autoenroll_mixpanel_event",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "Dataloaders.stakeholder", new_callable=AsyncMock)
async def test_log_stakeholder_in(
    mock_dataloaders_stakeholder: AsyncMock,
    mock_utils_send_autoenroll_mixpanel_event: AsyncMock,
    mock_stakeholders_domain_update_last_login: AsyncMock,
    mocked_data_for_module: Any,
    user_info: UserAccessInfo,
) -> None:
    mocks_setup_list: list[tuple[AsyncMock, str, list[Any]]] = [
        (
            mock_dataloaders_stakeholder.load,
            "Dataloaders.stakeholder",
            [user_info.user_email],
        ),
        (
            mock_utils_send_autoenroll_mixpanel_event,
            "utils.send_autoenroll_mixpanel_event",
            [user_info.user_email],
        ),
        (
            mock_stakeholders_domain_update_last_login,
            "stakeholders_domain.update_last_login",
            [user_info.user_email],
        ),
    ]
    # Set up mocks' results using mocked_data_for_module fixture
    for mock_item in mocks_setup_list:
        mock, path, arguments = mock_item
        mock.return_value = mocked_data_for_module(
            mock_path=path,
            mock_args=arguments,
            module_at_test=MODULE_AT_TEST,
        )
    loaders: Dataloaders = get_new_context()

    await log_stakeholder_in(
        loaders=loaders,
        user_info=user_info,
    )
    assert mock_dataloaders_stakeholder.load.called is True
    assert mock_utils_send_autoenroll_mixpanel_event.called is True
    assert mock_stakeholders_domain_update_last_login.called is True
