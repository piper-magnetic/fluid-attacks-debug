from fa_purity import (
    Cmd,
    Maybe,
    Stream,
)
from fa_purity.cmd.core import (
    unsafe_unwrap,
)
from fa_purity.stream.factory import (
    unsafe_from_cmd,
)
from typing import (
    Callable,
    Iterable,
    TypeVar,
)

_T = TypeVar("_T")


def append_to_stream(
    stream: Stream[_T], calc_item: Callable[[Maybe[_T]], Maybe[Cmd[_T]]]
) -> Stream[_T]:
    def _iter(prev: Iterable[_T]) -> Iterable[_T]:
        last: Maybe[_T] = Maybe.empty()
        for i in prev:
            last = Maybe.from_value(i)
            yield i
        result = calc_item(last)
        if result.map(lambda _: True).value_or(False):
            yield unsafe_unwrap(result.unwrap())

    return unsafe_from_cmd(stream.unsafe_to_iter().map(_iter))


def until_condition(
    stream: Stream[_T], condition: Callable[[_T], bool]
) -> Stream[_T]:
    def _iter(prev: Iterable[_T]) -> Iterable[_T]:
        for i in prev:
            yield i
            if condition(i):
                break

    return unsafe_from_cmd(stream.unsafe_to_iter().map(_iter))
