from back.test.unit.src.utils import (  # pylint: disable=import-error
    get_module_at_test,
)
from collections.abc import (
    Callable,
)
from custom_exceptions import (
    InvalidParameter,
)
from datetime import (
    datetime,
)
from db_model.subscriptions.add import (
    add,
)
from db_model.subscriptions.enums import (
    SubscriptionEntity,
    SubscriptionFrequency,
)
from db_model.subscriptions.types import (
    Subscription,
    SubscriptionState,
)
import pytest
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = get_module_at_test(file_path=__file__)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["subscription", "subscription_to_raise_exception"],
    [
        [
            Subscription(
                email="test_user_email@test.com",
                entity=SubscriptionEntity.ORGANIZATION,
                frequency=SubscriptionFrequency.WEEKLY,
                subject="test_report_subject",
                state=SubscriptionState(
                    modified_date=datetime.fromisoformat(
                        "2022-10-27T20:07:57+00:00"
                    )
                ),
            ),
            Subscription(
                email="test_user_email@test.com",
                entity=SubscriptionEntity.ORGANIZATION,
                frequency=SubscriptionFrequency.WEEKLY,
                subject="test_report_subject",
                state=SubscriptionState(modified_date=None),
            ),
        ],
        [
            Subscription(
                email="test_user_email2@test.com",
                entity=SubscriptionEntity.GROUP,
                frequency=SubscriptionFrequency.MONTHLY,
                subject="test_report_subject2",
                state=SubscriptionState(
                    modified_date=datetime.fromisoformat(
                        "2022-10-27T20:07:57+00:00"
                    )
                ),
            ),
            Subscription(
                email="test_user_email2@test.com",
                entity=SubscriptionEntity.GROUP,
                frequency=SubscriptionFrequency.MONTHLY,
                subject="test_report_subject2",
                state=SubscriptionState(modified_date=None),
            ),
        ],
    ],
)
@patch(MODULE_AT_TEST + "operations.batch_put_item", new_callable=AsyncMock)
async def test_add(
    mock_operations_batch_put_item: AsyncMock,
    mocked_data_for_module: Callable,
    subscription: Subscription,
    subscription_to_raise_exception: Subscription,
) -> None:
    # Set up mock's result using mocked_data_for_module fixture
    mock_operations_batch_put_item.return_value = mocked_data_for_module(
        mock_path="operations.batch_put_item",
        mock_args=[subscription],
        module_at_test=MODULE_AT_TEST,
    )
    await add(subscription=subscription)
    assert mock_operations_batch_put_item.called is True

    with pytest.raises(InvalidParameter) as invalid_parameter:
        await add(subscription=subscription_to_raise_exception)
    assert (
        str(invalid_parameter.value)
        == "Exception - Field modified_date is invalid"
    )
    assert mock_operations_batch_put_item.call_count == 1
