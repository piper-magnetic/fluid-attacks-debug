from collections.abc import (
    Iterable,
)
from custom_exceptions import (
    InvalidCvss3VectorString,
    InvalidSeverityCweIds,
)
from cvss import (
    CVSS3,
    CVSS3Error,
)
from db_model.findings.enums import (
    AttackComplexity,
    AttackVector,
    AvailabilityImpact,
    ConfidentialityImpact,
    Exploitability,
    IntegrityImpact,
    PrivilegesRequired,
    RemediationLevel,
    ReportConfidence,
    SeverityScope,
    UserInteraction,
)
from db_model.findings.types import (
    Cvss31Severity,
    Cvss31SeverityParameters,
    SeverityScore,
)
from decimal import (
    Decimal,
)
import math
import re

DEFAULT_CVSS_31_PARAMETERS = Cvss31SeverityParameters(
    base_score_factor=Decimal("1.08"),
    exploitability_factor_1=Decimal("8.22"),
    impact_factor_1=Decimal("6.42"),
    impact_factor_2=Decimal("7.52"),
    impact_factor_3=Decimal("0.029"),
    impact_factor_4=Decimal("3.25"),
    impact_factor_5=Decimal("0.02"),
    impact_factor_6=Decimal("15"),
    mod_impact_factor_1=Decimal("0.915"),
    mod_impact_factor_2=Decimal("6.42"),
    mod_impact_factor_3=Decimal("7.52"),
    mod_impact_factor_4=Decimal("0.029"),
    mod_impact_factor_5=Decimal("3.25"),
    mod_impact_factor_6=Decimal("0.02"),
    mod_impact_factor_7=Decimal("13"),
    mod_impact_factor_8=Decimal("0.9731"),
)


def get_f_impact(impact: Decimal) -> Decimal:
    if impact:
        f_impact_factor = Decimal("1.176")
    else:
        f_impact_factor = Decimal("0.0")

    return f_impact_factor


def get_cvss31_base_score(
    severity: Cvss31Severity,
    parameters: Cvss31SeverityParameters = DEFAULT_CVSS_31_PARAMETERS,
) -> Decimal:
    """
    Calculate cvss 3.1 base score attribute. Adjustment for Privileges Required
    metric is done according to the CVSS 3.1 spec.
    """
    severity = adjust_privileges_required(severity)
    iss = 1 - (
        (1 - severity.confidentiality_impact)
        * (1 - severity.integrity_impact)
        * (1 - severity.availability_impact)
    )
    if severity.severity_scope:
        impact = (
            parameters.impact_factor_2 * (iss - parameters.impact_factor_3)
        ) - (
            parameters.impact_factor_4
            * (iss - parameters.impact_factor_5) ** parameters.impact_factor_6
        )
    else:
        impact = parameters.impact_factor_1 * iss
    exploitability = (
        parameters.exploitability_factor_1
        * severity.attack_vector
        * severity.attack_complexity
        * severity.privileges_required
        * severity.user_interaction
    )
    if impact <= 0:
        base_score = Decimal(0)
    else:
        if severity.severity_scope:
            base_score = Decimal(
                math.ceil(
                    min(
                        float(
                            parameters.base_score_factor
                            * (impact + exploitability)
                        ),
                        10,
                    )
                    * 10
                )
                / 10
            )
        else:
            base_score = Decimal(
                math.ceil(min(float(impact + exploitability), 10) * 10) / 10
            )

    return base_score.quantize(Decimal("0.1"))


def get_cvss31_temporal(
    severity: Cvss31Severity, base_score: Decimal
) -> Decimal:
    """Calculate cvss 3.1 temporal attribute."""
    temporal = Decimal(
        math.ceil(
            base_score
            * severity.exploitability
            * severity.remediation_level
            * severity.report_confidence
            * 10
        )
        / 10
    )

    return temporal.quantize(Decimal("0.1"))


def _calculate_privileges(privileges: Decimal, scope: Decimal) -> Decimal:
    """
    Calculate Privileges Required metric according to
    https://www.first.org/cvss/specification-document#7-4-Metric-Values.
    """
    if scope:  # Changed
        if privileges == Decimal("0.62"):
            privileges = Decimal("0.68")
        elif privileges == Decimal("0.27"):
            privileges = Decimal("0.5")
    else:  # Unchanged
        if privileges == Decimal("0.68"):
            privileges = Decimal("0.62")
        elif privileges == Decimal("0.5"):
            privileges = Decimal("0.27")

    return Decimal(privileges).quantize(Decimal("0.01"))


def adjust_privileges_required(severity: Cvss31Severity) -> Cvss31Severity:
    """
    Adjusment made to the metric in accordance to official documentation
    https://www.first.org/cvss/specification-document#7-4-Metric-Values.
    """
    return severity._replace(
        privileges_required=_calculate_privileges(
            severity.privileges_required,
            severity.severity_scope,
        ),
        modified_privileges_required=_calculate_privileges(
            severity.modified_privileges_required,
            severity.modified_severity_scope,
        ),
    )


def get_severity_score(severity: Cvss31Severity) -> Decimal:
    base_score = get_cvss31_base_score(severity)
    return get_cvss31_temporal(severity, base_score)


def get_severity_level(severity: Decimal) -> str:
    """
    Qualitative severity rating scale as defined in
    https://www.first.org/cvss/v3.1/specification-document section 5.
    """
    if severity < 4:
        return "low"
    if 4 <= severity < 7:
        return "medium"
    if 7 <= severity < 9:
        return "high"

    return "critical"


def get_cvssf_score(temporal_score: Decimal) -> Decimal:
    return Decimal(
        pow(Decimal("4.0"), temporal_score - Decimal("4.0"))
    ).quantize(Decimal("0.001"))


def parse_cvss31_vector_string(vector_string: str) -> Cvss31Severity:
    pattern = (
        r"^CVSS:3[.]1/((AV:[NALP]|AC:[LH]|PR:[NLH]|UI:[NR]|S:[UC]|"
        r"[CIA]:[NLH]|E:[XUPFH]|RL:[XOTWU]|RC:[XURC]|[CIA]R:[XLMH]|"
        r"MAV:[XNALP]|MAC:[XLH]|MPR:[XNLH]|MUI:[XNR]|MS:[XUC]|M[CIA]:[XNLH])/)"
        r"*(AV:[NALP]|AC:[LH]|PR:[NLH]|UI:[NR]|S:[UC]|[CIA]:[NLH]|E:[XUPFH]|"
        r"RL:[XOTWU]|RC:[XURC]|[CIA]R:[XLMH]|MAV:[XNALP]|MAC:[XLH]|MPR:[XNLH]|"
        r"MUI:[XNR]|MS:[XUC]|M[CIA]:[XNLH])$"
    )
    if not re.match(pattern, vector_string):
        raise InvalidCvss3VectorString()

    cvss_vector = {
        metric.split(":")[0]: metric.split(":")[1]
        for metric in vector_string.split("/")
        if metric not in {"CVSS:3.1"}
    }

    try:
        return Cvss31Severity(
            # Base. Mandatory: YES
            attack_vector=AttackVector[cvss_vector["AV"]].value,
            attack_complexity=AttackComplexity[cvss_vector["AC"]].value,
            privileges_required=PrivilegesRequired[cvss_vector["PR"]].value,
            user_interaction=UserInteraction[cvss_vector["UI"]].value,
            severity_scope=SeverityScope[cvss_vector["S"]].value,
            confidentiality_impact=ConfidentialityImpact[
                cvss_vector["C"]
            ].value,
            integrity_impact=IntegrityImpact[cvss_vector["I"]].value,
            availability_impact=AvailabilityImpact[cvss_vector["A"]].value,
            # Temporal. Mandatory: NO
            exploitability=Exploitability[cvss_vector["E"]].value
            if cvss_vector.get("E")
            else Decimal("0.0"),
            remediation_level=RemediationLevel[cvss_vector["RL"]].value
            if cvss_vector.get("RL")
            else Decimal("0.0"),
            report_confidence=ReportConfidence[cvss_vector["RC"]].value
            if cvss_vector.get("RC")
            else Decimal("0.0"),
        )
    except KeyError as ex:
        raise InvalidCvss3VectorString() from ex


def parse_cwe_ids(raw_list: Iterable[str] | None) -> list[str] | None:
    if not raw_list:
        return None

    pattern = r"^CWE-\d{1,4}$"
    if not all(re.match(pattern, id) for id in raw_list):
        raise InvalidSeverityCweIds()

    return list(raw_list)


def get_severity_score_summary(
    severity: Cvss31Severity,
    cwe_ids: list[str] | None = None,
) -> SeverityScore:
    base_score = get_cvss31_base_score(severity)
    temporal_score = get_cvss31_temporal(severity, base_score)

    return SeverityScore(
        base_score=base_score,
        temporal_score=temporal_score,
        cvssf=get_cvssf_score(temporal_score),
        cwe_ids=cwe_ids,
    )


def get_severity_score_from_cvss3_vector(
    vector: str,
    cwe_ids: list[str] | None,
) -> SeverityScore:
    try:
        cvss3 = CVSS3(vector)
        base_score = cvss3.base_score
        temporal_score = cvss3.temporal_score

        return SeverityScore(
            base_score=base_score,
            temporal_score=temporal_score,
            cvss_v3=cvss3.clean_vector(),
            cvssf=get_cvssf_score(temporal_score),
            cwe_ids=cwe_ids,
        )
    except CVSS3Error as ex:
        raise InvalidCvss3VectorString() from ex


def validate_cvss3_vector(vector: str | None) -> None:
    if not vector:
        return

    try:
        CVSS3(vector)
    except CVSS3Error as ex:
        raise InvalidCvss3VectorString() from ex
