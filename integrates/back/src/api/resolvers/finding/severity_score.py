from .schema import (
    FINDING,
)
from db_model.findings.types import (
    Finding,
)
from decimal import (
    Decimal,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from newutils import (
    cvss as cvss_utils,
)


@FINDING.field("severityScore")
def resolve(
    parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None
) -> Decimal:
    return cvss_utils.get_severity_score(parent.severity)
