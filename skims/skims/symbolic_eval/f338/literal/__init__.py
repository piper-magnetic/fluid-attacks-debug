from model.core_model import (
    MethodsEnum,
)
from symbolic_eval.f338.literal.common import (
    value_is_hardcoded,
)
from symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.TS_SALT_IS_HARDCODED: value_is_hardcoded,
    MethodsEnum.JS_SALT_IS_HARDCODED: value_is_hardcoded,
    MethodsEnum.JAVA_SALT_IS_HARDCODED: value_is_hardcoded,
    MethodsEnum.KOTLIN_SALT_IS_HARDCODED: value_is_hardcoded,
    MethodsEnum.DART_SALT_IS_HARDCODED: value_is_hardcoded,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
