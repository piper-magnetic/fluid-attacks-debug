from collections.abc import (
    Iterator,
)
from lib_root.f297.common import (
    get_vuln_nodes,
)
from model.core_model import (
    MethodsEnum,
    Vulnerabilities,
)
from model.graph_model import (
    GraphDB,
    GraphShardMetadataLanguage as GraphLanguage,
    GraphShardNode,
)
from sast.query import (
    get_vulnerabilities_from_n_ids,
)


def sql_injection(graph_db: GraphDB) -> Vulnerabilities:
    method = MethodsEnum.TS_SQL_INJECTION

    def n_ids() -> Iterator[GraphShardNode]:
        for shard in graph_db.shards_by_language(GraphLanguage.TYPESCRIPT):
            if shard.syntax_graph is None:
                continue
            graph = shard.syntax_graph

            for n_id in get_vuln_nodes(graph, method):
                yield shard, n_id

    return get_vulnerabilities_from_n_ids(
        desc_key="src.lib_root.f297.sql_injection",
        desc_params={},
        graph_shard_nodes=n_ids(),
        method=method,
    )
