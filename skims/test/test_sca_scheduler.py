from aioextensions import (
    run,
)
from db_model.advisories.types import (
    Advisory,
)
from git.exc import (
    GitError,
)
from importlib import (
    import_module,
)
import pytest
from pytest_mock import (
    MockerFixture,
)
from schedulers.invoker import (
    main as invoker_main,
)
from schedulers.update_sca_table import (
    clone_repo,
    fix_advisory,
    main as sca_table_main,
    update_sca,
)
from schedulers.update_sca_table.repositories.advisories_community import (
    _format_ranges,
    fix_pip_composer_range,
    format_range,
    format_ranges,
    get_advisories_community,
    get_platform_advisories,
    PLATFORMS,
    RE_RANGES,
    URL_ADVISORIES_COMMUNITY,
)
from schedulers.update_sca_table.repositories.advisory_database import (
    append_advisories,
    get_advisory_database,
    get_final_range,
    get_limit,
    get_vulnerabilities_ranges,
    URL_ADVISORY_DATABASE,
)
from types import (
    ModuleType,
)
import yaml  # type: ignore

UPD_SCA_TABLE_STR: str = "schedulers.update_sca_table"
ADVS_COMM_MOD_STR: str = (
    f"{UPD_SCA_TABLE_STR}.repositories.advisories_community"
)
ADVS_DB_MOD_STR: str = f"{UPD_SCA_TABLE_STR}.repositories.advisory_database"


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "ranges,expected",
    [
        ("[5.0.0,6.0.0)[6.0.0,6.0.4]", ["[5.0.0,6.0.0)", "[6.0.0,6.0.4]"]),
        ("(,0.3.13]", ["(,0.3.13]"]),
        ("[1.1.4]", ["[1.1.4]"]),
        ("12345]", []),
    ],
)
def test_re_ranges_pattern(ranges: str, expected: list[str]) -> None:
    match_ranges: list[str] = RE_RANGES.findall(ranges)
    assert match_ranges == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "range_str,expected",
    [
        ("[5.0.0,6.0.0)", ">=5.0.0 <6.0.0"),
        ("(,0.3.13]", ">=0 <=0.3.13"),
        ("[1.1.4]", "=1.1.4"),
    ],
)
def test_advs_format_range(range_str: str, expected: str) -> None:
    formated_range: str = format_range(range_str)
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "range_str,expected",
    [
        (">=4.0,<4.3||>=5.0,<5.2", ">=4.0 <4.3||>=5.0 <5.2"),
        ("==3.1||>=4.0.0,<=4.0.2", "=3.1||>=4.0.0 <=4.0.2"),
        (">=1.0,<=1.0.1", ">=1.0 <=1.0.1"),
    ],
)
def test_fix_pip_composer_range(range_str: str, expected: str) -> None:
    formated_range: str = fix_pip_composer_range(range_str)
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "platform,range_str,format_func,expected",
    [
        ("pypi", ">=1.0,<=1.0.1", "fix_pip_composer_range", ">=1.0 <=1.0.1"),
        (
            "npm",
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
            None,
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
        ),
        ("maven", "[1.1.4]", "format_range", "=1.1.4"),
    ],
)
def test_format_ranges_internal(
    mocker: MockerFixture,
    platform: str,
    range_str: str,
    format_func: str | None,
    expected: str,
) -> None:
    format_func_mock = None
    if format_func:
        format_func_mock = mocker.patch(
            f"{ADVS_COMM_MOD_STR}.{format_func}", return_value=expected
        )
    formated_range: str = _format_ranges(platform, range_str)
    if format_func_mock:
        assert format_func_mock.call_count == 1
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "platform,range_str,internal_return,expected",
    [
        ("pypi", "1.0", "<1.0||>=2.4.1 <3.2.1", "<1.0 || >=2.4.1 <3.2.1"),
        (
            "gem",
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
            "<5.2.4.4||>=6.0.0.0 <6.0.3.3",
            "<5.2.4.4 || >=6.0.0.0 <6.0.3.3",
        ),
        ("nuget", "[12.3.5]", "=12.3.5", "=12.3.5"),
    ],
)
def test_format_ranges(
    mocker: MockerFixture,
    platform: str,
    range_str: str,
    internal_return: str,
    expected: str,
) -> None:
    format_internal_mock = mocker.patch(
        f"{ADVS_COMM_MOD_STR}._format_ranges", return_value=internal_return
    )
    formated_range: str = format_ranges(platform, range_str)
    assert format_internal_mock.call_args.args == (platform, range_str)
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "parameters,expected",
    [
        (("=3.11", ">=3.11 <3.11.1"), "=3.11 || >=3.11 <3.11.1"),
        (
            (">=3.9.0 <3.9.3", ">=3.8.0 <3.8.6"),
            ">=3.9.0 <3.9.3 || >=3.8.0 <3.8.6",
        ),
        (("=3.4.2", "=3.5.6"), "=3.4.2 || =3.5.6"),
    ],
)
def test_get_final_range(parameters: tuple[str, str], expected: str) -> None:
    formated_range: str = get_final_range(*parameters)
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "parameters,expected",
    [
        ([{"introduced": "1.0.2"}, {"last_affected": "1.2.4"}], " <=1.2.4"),
        (
            [{"introduced": "1.3.5"}, {"fixed": "2.3.6"}],
            " <2.3.6",
        ),
        ([{"introduced": "1.0.2"}], ""),
    ],
)
def test_get_limit(parameters: list[dict[str, str]], expected: str) -> None:
    formated_range: str = get_limit(parameters)
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "parameter,expected",
    [
        (
            Advisory(
                associated_advisory="CVE-ADVISORY-1",
                package_manager="gem",
                package_name="package_name_1",
                severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="https://github.com/github/advisory-database.git",
                vulnerable_version=">=0 || >=2.4.0 <2.12.2",
            ),
            Advisory(
                associated_advisory="CVE-ADVISORY-1",
                package_manager="gem",
                package_name="package_name_1",
                severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="https://github.com/github/advisory-database.git",
                vulnerable_version=">=2.4.0 <2.12.2",
            ),
        ),
        (
            Advisory(
                associated_advisory="CVE-ADVISORY-2",
                package_manager="go",
                package_name="package_name_2",
                severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="MANUAL",
                vulnerable_version=">=0 <2.4.0 || >2.5.4 <=2.6.3",
            ),
            Advisory(
                associated_advisory="CVE-ADVISORY-2",
                package_manager="go",
                package_name="package_name_2",
                severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                source="MANUAL",
                vulnerable_version=">=0 <2.4.0 || >2.5.4 <=2.6.3",
            ),
        ),
    ],
)
def test_fix_advisory(parameter: Advisory, expected: Advisory) -> None:
    formated_range: Advisory = fix_advisory(parameter)
    assert formated_range == expected


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "current_advisories,expected",
    [
        (
            {"http": {"version": ">=0 <0.13.3", "platform": "pub"}},
            [
                Advisory(
                    associated_advisory="CVE-EXAMPLE-1",
                    package_manager="pub",
                    package_name="http",
                    severity=None,
                    source=URL_ADVISORY_DATABASE,
                    vulnerable_version=">=0 <0.13.3",
                )
            ],
        ),
        ({}, []),
    ],
)
def test_append_advisories(
    current_advisories: dict[str, dict[str, str]], expected: list[Advisory]
) -> None:
    advisories: list[Advisory] = []
    vuln_id: str = "CVE-EXAMPLE-1"
    severity: None = None
    append_advisories(advisories, current_advisories, vuln_id, severity)
    assert advisories == expected


@pytest.mark.skims_test_group("unittesting")
def test_get_advisories_community(mocker: MockerFixture) -> None:
    mocker.patch("builtins.print")
    get_advs_mock = mocker.patch(
        f"{ADVS_COMM_MOD_STR}.get_platform_advisories"
    )
    get_advisories_community([], "sample-dir_name")
    assert get_advs_mock.call_count == len(PLATFORMS)


@pytest.mark.skims_test_group("unittesting")
def test_get_platform_advisories(mocker: MockerFixture) -> None:
    advisories: list[Advisory] = []
    tmp_dirname: str = "skims/test/data/sca_scheduler/test_sca_adv"
    platform: str = "npm"
    affected_range: str = ">=1.0.0 <=1.6.3"
    expected: list[Advisory] = [
        Advisory(
            associated_advisory="CVE-2021-25943",
            package_manager=platform,
            package_name="101",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            source=URL_ADVISORIES_COMMUNITY,
            vulnerable_version=affected_range,
        )
    ]
    format_ranges_mock = mocker.patch(
        f"{ADVS_COMM_MOD_STR}.format_ranges",
        return_value=affected_range,
    )
    get_platform_advisories(advisories, tmp_dirname, platform)
    assert format_ranges_mock.call_args.args == (platform, affected_range)
    assert advisories == expected


@pytest.mark.skims_test_group("unittesting")
def test_get_platform_advisories_error(mocker: MockerFixture) -> None:
    advisories: list[Advisory] = []
    tmp_dirname: str = "skims/test/data/sca_scheduler/test_sca_adv"
    platform: str = "npm"
    glob_mock = mocker.patch(
        f"{ADVS_COMM_MOD_STR}.yaml.safe_load",
        side_effect=yaml.YAMLError(),
    )
    print_mock = mocker.patch("builtins.print")
    get_platform_advisories(advisories, tmp_dirname, platform)
    assert glob_mock.call_count == 1
    assert isinstance(print_mock.call_args.args[0], yaml.YAMLError)


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize(
    "affected",
    [
        [
            {
                "package": {"ecosystem": "npm", "name": "vega"},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "0"},
                            {"fixed": "5.23.0"},
                        ],
                    }
                ],
            }
        ],
        [
            {
                "package": {"ecosystem": "Packagist", "name": "php_pkg"},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "1.0.1"},
                            {"last_affected": "2.24.5"},
                        ],
                    }
                ],
            },
            {
                "package": {"ecosystem": "Packagist", "name": "php_pkg"},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "3.0.5"},
                            {"fixed": "4.0.8"},
                        ],
                    }
                ],
            },
        ],
    ],
)
def test_get_vulnerabilities_ranges(
    mocker: MockerFixture, affected: list[dict]
) -> None:
    advisories: list[Advisory] = []
    vuln_id: str = "CVE-EXAMPLE"
    severity: None = None
    len_affected: int = len(affected)
    pkg_info: dict[str, str] = affected[0]["package"]
    final_range_output: str = "final_range_output"
    adv_db_mod_str: str = f"{UPD_SCA_TABLE_STR}.repositories.advisory_database"
    get_limit_mock = mocker.patch(
        f"{adv_db_mod_str}.get_limit", return_value="test_output"
    )
    get_final_range_mock = mocker.patch(
        f"{adv_db_mod_str}.get_final_range", return_value=final_range_output
    )
    append_advs_mock = mocker.patch(f"{adv_db_mod_str}.append_advisories")
    get_vulnerabilities_ranges(affected, vuln_id, advisories, severity)
    assert get_limit_mock.call_count == len_affected
    assert get_final_range_mock.call_count == len_affected
    assert append_advs_mock.call_args.args == (
        advisories,
        {
            pkg_info["name"]: {
                "version": " || ".join([final_range_output] * len_affected),
                "platform": pkg_info["ecosystem"].lower(),
            }
        },
        vuln_id,
        severity,
    )


@pytest.mark.skims_test_group("unittesting")
def test_get_advisory_database(mocker: MockerFixture) -> None:
    advisories: list[Advisory] = []
    tmp_dirname: str = "skims/test/data/sca_scheduler"
    expected: list[dict] = [
        {
            "advisory": "GHSA-2qh6-hhvv-m2ww",
            "affected": [
                {
                    "package": {
                        "ecosystem": "Maven",
                        "name": "org.jenkins-ci.plugins:http_request",
                    },
                    "ranges": [
                        {
                            "type": "ECOSYSTEM",
                            "events": [{"introduced": "0"}, {"fixed": "1.16"}],
                        }
                    ],
                }
            ],
            "severity": "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N",
        },
        {
            "advisory": "GHSA-h79p-32mx-fjj9",
            "affected": [
                {
                    "package": {
                        "ecosystem": "Maven",
                        "name": "org.apache.camel:camel-netty",
                    },
                    "ranges": [
                        {
                            "type": "ECOSYSTEM",
                            "events": [
                                {"introduced": "3.0.0"},
                                {"fixed": "3.2.0"},
                            ],
                        }
                    ],
                }
            ],
            "severity": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
        },
    ]
    mocker.patch("builtins.print")
    get_vulns_mock = mocker.patch(
        (
            f"{UPD_SCA_TABLE_STR}.repositories"
            ".advisory_database.get_vulnerabilities_ranges"
        )
    )
    get_advisory_database(advisories, tmp_dirname)
    assert get_vulns_mock.call_args_list == [
        ((pkg["affected"], pkg["advisory"], advisories, pkg["severity"]),)
        for pkg in expected
    ]


@pytest.mark.skims_test_group("unittesting")
def test_clone_repo(mocker: MockerFixture) -> None:
    mocker.patch("builtins.print")
    clone_mock = mocker.patch(f"{UPD_SCA_TABLE_STR}.Repo.clone_from")
    result_clone = clone_repo(URL_ADVISORY_DATABASE)
    assert clone_mock.call_count == 1
    assert isinstance(result_clone, str)


@pytest.mark.skims_test_group("unittesting")
def test_clone_repo_error(mocker: MockerFixture) -> None:
    mocker.patch("builtins.print")
    mocker.patch(f"{UPD_SCA_TABLE_STR}.log_blocking")
    mocked = mocker.patch(
        f"{UPD_SCA_TABLE_STR}.Repo.clone_from",
        side_effect=GitError(),
    )
    result_clone = clone_repo("invalid_repo_url/")
    assert mocked.call_count == 1
    assert result_clone is None


@pytest.mark.skims_test_group("unittesting")
def test_sca_table_main(mocker: MockerFixture) -> None:
    mocked = mocker.patch(f"{UPD_SCA_TABLE_STR}.update_sca")
    run(sca_table_main())
    assert mocked.await_count == 1


@pytest.mark.skims_test_group("unittesting")
def test_update_sca(mocker: MockerFixture) -> None:
    mocker.patch(f"{UPD_SCA_TABLE_STR}.s3_shutdown")
    mocker.patch(f"{UPD_SCA_TABLE_STR}.s3_start_resource")
    mocker.patch(f"{UPD_SCA_TABLE_STR}.fix_advisory")
    log_mock = mocker.patch(f"{UPD_SCA_TABLE_STR}.log_blocking")
    module_test: ModuleType = import_module(UPD_SCA_TABLE_STR)
    get_repo_mock = mocker.Mock(
        side_effect=lambda advs, pat: advs.append("test_advisory")
    )
    repos_mock = mocker.patch.object(
        module_test, "REPOSITORIES", [(get_repo_mock, "test")]
    )
    clone_repo_mock = mocker.patch(
        f"{UPD_SCA_TABLE_STR}.clone_repo", return_value="test_dir/"
    )
    model_add_mock = mocker.patch(f"{UPD_SCA_TABLE_STR}.advisories_model.add")
    upload_advs_mock = mocker.patch(f"{UPD_SCA_TABLE_STR}.upload_advisories")
    run(update_sca())
    advisories_list = get_repo_mock.call_args.args[0]
    assert clone_repo_mock.call_count == len(repos_mock)
    assert get_repo_mock.call_count == len(repos_mock)
    assert model_add_mock.call_count == len(advisories_list)
    assert upload_advs_mock.call_count == 1
    assert log_mock.call_count == 4


@pytest.mark.skims_test_group("unittesting")
@pytest.mark.parametrize("is_coroutine", [True, False])
def test_invoker_main(mocker: MockerFixture, is_coroutine: bool) -> None:
    table_main_func_str: str = f"{UPD_SCA_TABLE_STR}.main"
    invoker_mod_str: str = "schedulers.invoker"
    dyn_start_mock = mocker.patch(f"{invoker_mod_str}.dynamo_startup")
    dyn_down_mock = mocker.patch(f"{invoker_mod_str}.dynamo_shutdown")
    in_thread_mock = mocker.patch(f"{invoker_mod_str}.in_thread")
    table_main_mock = mocker.patch(table_main_func_str)
    mocker.patch(f"{invoker_mod_str}.sys", argv=["test", table_main_func_str])
    mocker.patch(
        f"{invoker_mod_str}.asyncio.iscoroutinefunction",
        return_value=is_coroutine,
    )
    run(invoker_main())
    assert dyn_start_mock.await_count == 1
    assert dyn_down_mock.await_count == 1
    if is_coroutine:
        assert table_main_mock.await_count == 1
    else:
        assert in_thread_mock.await_count == 1
