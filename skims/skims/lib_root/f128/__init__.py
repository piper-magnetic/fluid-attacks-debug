from lib_root.f128.java import (
    java_cookie_http_only,
)
from lib_root.f128.javascript import (
    javascript_insecure_cookies,
)
from lib_root.f128.kotlin import (
    kt_cookie_http_only,
)
from lib_root.f128.python import (
    python_http_only_cookie,
)
from lib_root.f128.typescript import (
    typescript_insecure_cookies,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F128
QUERIES: graph_model.Queries = (
    (FINDING, javascript_insecure_cookies),
    (FINDING, typescript_insecure_cookies),
    (FINDING, java_cookie_http_only),
    (FINDING, kt_cookie_http_only),
    (FINDING, python_http_only_cookie),
)
