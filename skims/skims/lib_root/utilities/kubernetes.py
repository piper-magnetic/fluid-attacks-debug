from collections.abc import (
    Iterator,
)
from model.graph_model import (
    Graph,
    NId,
)
from utils import (
    graph as g,
)
from utils.graph import (
    adj_ast,
)


def get_list_from_node(graph: Graph, nid: NId | None) -> list:
    if nid:
        value_id = graph.nodes[nid]["value_id"]
        if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
            child_ids = adj_ast(graph, value_id)
            result: list = []
            for c_id in child_ids:
                result.append(graph.nodes[c_id].get("value"))
            return result
        return [graph.nodes[value_id].get("value")]
    return []


def get_attr_inside_attrs(
    graph: Graph, nid: NId, attrs: list
) -> tuple[str | None, str, NId]:
    curr_nid = nid
    final_key, final_val, final_id = None, "", ""
    attr, attr_val, attr_id = None, "", ""
    for key in attrs:
        if not curr_nid:
            break
        attr, attr_val, attr_id = get_attribute(graph, curr_nid, key)
        if not attr:
            break
        curr_nid = graph.nodes[attr_id].get("value_id")
    else:
        final_key, final_val, final_id = attr, attr_val, attr_id

    return final_key, final_val, final_id


def get_key_value(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id]["value"]
    value_id = graph.nodes[nid]["value_id"]
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_id = adj_ast(graph, value_id, label_type="Literal")
        if len(child_id) > 0:
            value = graph.nodes[child_id[0]].get("value", "")
    else:
        value = graph.nodes[value_id].get("value", "")
    return key, value


def get_attribute(
    graph: Graph, object_id: NId, expected_attr: str
) -> tuple[str | None, str, NId]:
    if object_id != "":
        for attr_id in adj_ast(graph, object_id, label_type="Pair"):
            key, value = get_key_value(graph, attr_id)
            if key == expected_attr:
                return key, value, attr_id
    return None, "", ""


def get_pod_spec(graph: Graph, nid: NId) -> NId | None:
    if (
        check_template_integrity(graph, nid)
        and (kind := get_attribute(graph, nid, "kind"))
        and kind[1] == "Pod"
    ):
        spec, _, spec_id = get_attribute(graph, nid, "spec")
        if spec:
            return graph.nodes[spec_id]["value_id"]
    return None


def iterate_template_spec(graph: Graph) -> Iterator[NId]:
    for nid in g.matching_nodes(graph, label_type="Object"):
        if check_template_integrity(graph, nid):
            spec, _, spec_id = get_attribute(graph, nid, "spec")
            if spec:
                yield graph.nodes[spec_id]["value_id"]


def iterate_images(graph: Graph) -> Iterator[tuple[str, NId]]:
    for nid in g.matching_nodes(graph, label_type="Object"):
        if check_template_integrity(graph, nid):
            for p_id in g.matching_nodes(graph, label_type="Pair"):
                key, value = get_key_value(graph, p_id)
                if key == "image":
                    yield value, p_id


def get_containers_capabilities(
    graph: Graph, sec_ctx: NId, type_cap: str
) -> list:
    cap = get_attribute(graph, sec_ctx, "capabilities")
    if cap[0]:
        cap_attrs = graph.nodes[cap[2]]["value_id"]
        specific_cap = get_attribute(graph, cap_attrs, type_cap)
        if specific_cap[0]:
            return get_list_from_node(graph, specific_cap[2])
    return []


def check_template_integrity(graph: Graph, nid: NId) -> bool:
    valid_template = get_attribute(graph, nid, "apiVersion")
    if valid_template[0]:
        return True
    return False


def _aux_iterate_containers(graph: Graph, nid: NId) -> Iterator[NId]:
    container_attrs = graph.nodes[nid]["value_id"]
    for c_id in adj_ast(graph, container_attrs):
        yield c_id


def iterate_containers(graph: Graph) -> Iterator[NId]:
    containers_type = {
        "containers",
        "ephemeralContainers",
        "initContainers",
    }
    for nid in g.matching_nodes(graph, label_type="Object"):
        if check_template_integrity(graph, nid):
            spec, _, spec_id = get_attribute(graph, nid, "spec")
            if spec:
                spec_attrs = graph.nodes[spec_id]["value_id"]
                for cont in containers_type:
                    container = get_attribute(graph, spec_attrs, cont)
                    if container[0]:
                        yield from _aux_iterate_containers(graph, container[2])


def _iterate_sec_ctx_from_spec(graph: Graph, nid: NId) -> Iterator[NId]:
    spec, _, spec_id = get_attribute(graph, nid, "spec")
    if spec:
        spec_attrs = graph.nodes[spec_id]["value_id"]
        sec_ctx = get_attribute(graph, spec_attrs, "securityContext")
        if sec_ctx[0]:
            sec_ctx_attrs = graph.nodes[sec_ctx[2]]["value_id"]
            yield sec_ctx_attrs
        else:
            yield spec_attrs


def iterate_security_context(
    graph: Graph, container_only: bool
) -> Iterator[NId]:
    for nid in g.matching_nodes(graph, label_type="Object"):
        if check_template_integrity(graph, nid):
            if (
                not container_only
                and (kind := get_attribute(graph, nid, "kind"))
                and kind[1] == "Pod"
            ):
                yield from _iterate_sec_ctx_from_spec(graph, nid)
            for container in iterate_containers(graph):
                sec_ctx = get_attribute(graph, container, "securityContext")
                if sec_ctx[0]:
                    sec_ctx_attrs = graph.nodes[sec_ctx[2]]["value_id"]
                    yield sec_ctx_attrs
