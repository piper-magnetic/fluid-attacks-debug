from lib_root.f418.docker_compose import (
    docker_compose_read_only,
)
from model import (
    core_model,
    graph_model,
)

FINDING: core_model.FindingEnum = core_model.FindingEnum.F418
QUERIES: graph_model.Queries = ((FINDING, docker_compose_read_only),)
