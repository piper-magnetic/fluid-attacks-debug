from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
    field,
)
from fa_purity import (
    Result,
    ResultE,
)
from tap_gitlab.intervals.patch import (
    Patch,
)
from typing import (
    Any,
    Callable,
    Generic,
    TypeVar,
    Union,
)


@dataclass(frozen=True)
class _Private:
    pass


@dataclass(frozen=True)
class MIN:
    def __str__(self) -> str:
        return "MIN"


@dataclass(frozen=True)
class MAX:
    def __str__(self) -> str:
        return "MAX"


class InvalidInterval(Exception):
    pass


_P = TypeVar("_P")
IntervalPoint = Union[_P, MIN, MAX]
Comparison = Callable[[_P, _P], bool]


@dataclass(frozen=True)
class ClosedInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Patch[Comparison[IntervalPoint[_P]]]
    lower: _P
    upper: _P

    @staticmethod
    def new(
        greater_than: Comparison[IntervalPoint[_P]],
        lower: _P,
        upper: _P,
    ) -> ResultE[ClosedInterval[_P]]:
        if not greater_than(upper, lower):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = ClosedInterval(_Private(), Patch(greater_than), lower, upper)
        return Result.success(obj)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        _greater = self.greater.unwrap
        return (_greater(point, self.lower) or point == self.lower) and (
            _greater(self.upper, point) or point == self.upper
        )


@dataclass(frozen=True)
class OpenInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Patch[Comparison[IntervalPoint[_P]]]
    lower: Union[_P, MIN]
    upper: Union[_P, MAX]

    @staticmethod
    def new(
        greater_than: Comparison[IntervalPoint[_P]],
        lower: Union[_P, MIN],
        upper: Union[_P, MAX],
    ) -> ResultE[OpenInterval[_P]]:
        if not greater_than(upper, lower):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = OpenInterval(_Private(), Patch(greater_than), lower, upper)
        return Result.success(obj)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        _greater = self.greater.unwrap
        return _greater(point, self.lower) and _greater(self.upper, point)


@dataclass(frozen=True)
class OpenLeftInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Patch[Comparison[IntervalPoint[_P]]]
    lower: Union[_P, MIN]
    upper: _P

    @staticmethod
    def new(
        greater_than: Comparison[IntervalPoint[_P]],
        lower: Union[_P, MIN],
        upper: _P,
    ) -> ResultE[OpenLeftInterval[_P]]:
        if not greater_than(upper, lower):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = OpenLeftInterval(_Private(), Patch(greater_than), lower, upper)
        return Result.success(obj)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        _greater = self.greater.unwrap
        return _greater(point, self.lower) and (
            _greater(self.upper, point) or point == self.upper
        )


@dataclass(frozen=True)
class OpenRightInterval(Generic[_P]):
    _private: _Private = field(repr=False, hash=False, compare=False)
    greater: Patch[Comparison[IntervalPoint[_P]]]
    lower: _P
    upper: Union[_P, MAX]

    @staticmethod
    def new(
        greater_than: Comparison[IntervalPoint[_P]],
        lower: _P,
        upper: Union[_P, MAX],
    ) -> ResultE[OpenRightInterval]:
        if not greater_than(upper, lower):
            err = InvalidInterval(f"{upper} <= {lower}")
            return Result.failure(Exception(err))
        obj = OpenRightInterval(_Private(), Patch(greater_than), lower, upper)
        return Result.success(obj)

    def __contains__(self, point: IntervalPoint[_P]) -> bool:
        _greater = self.greater.unwrap
        return (
            _greater(point, self.lower) or point == self.lower
        ) and _greater(self.upper, point)


Interval = Union[
    ClosedInterval[_P],
    OpenInterval[_P],
    OpenLeftInterval[_P],
    OpenRightInterval[_P],
]
